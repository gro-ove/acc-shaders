cbuffer cbData : register(b1) {
  float opacity;
  float2 screenSize;
  float resolutionMult;

  float2 resolutionMultStep;
  float2 padding;
}

SamplerState samPoint : register(s10) {
  Filter = POINT;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  return float4(saturate(txDiffuse.SampleLevel(samPoint, pin.Tex, 0).rgb) * opacity, 1);
}