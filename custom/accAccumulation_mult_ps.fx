cbuffer cbData : register(b1) {
  float opacity;
  float2 screenSize;
  float resolutionMult;

  float2 resolutionMultStep;
  float2 padding;
}

SamplerState samPoint : register(s11) {
  Filter = POINT;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float2 uv = pin.Tex;

  float2 pos = uv * screenSize;
  uint pixel_x = uint(resolutionMult - 1) - uint(pos.x) % uint(resolutionMult);
  uint pixel_y = uint(pos.y) % uint(resolutionMult);
  if (pixel_x != uint(resolutionMultStep.x) || pixel_y != uint(resolutionMultStep.y)) clip(-1);
  
  return float4(saturate(txDiffuse.Load(float3(uv * screenSize / resolutionMult, 0), int2(0, 0)).rgb) * opacity, 1);
}