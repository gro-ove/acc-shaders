#define PI 3.141592
#define iSteps 12
#define jSteps 4

#define iStepGrowth 1.15
#define jStepGrowth 1.15
#define mieExtinctionMul 1.11 // 1.11 in frostbite

float2 rsi(float3 r0, float3 rd, float sr) {
  // Ray-sphere intersection that assumes
  // the sphere is centered at the origin.
  // No intersection when result.x > result.y
  float a = dot(rd, rd);
  float b = 2.0 * dot(rd, r0);
  float c = dot(r0, r0) - (sr * sr);
  float d = (b * b) - 4.0 * a * c;
  if (d < 0.0) return float2(1e5,-1e5);
  return float2(
    -b - sqrt(d),
    -b + sqrt(d)
  ) / (2.0 * a);
}

float geometricSeries(float commonRatio, float numTerms) {
  return (1.0 - pow(commonRatio, numTerms)) / (1.0 - commonRatio);
}

cbuffer cbData : register(b0) {
  float3 sunDir;
  float sunIntensity; // 14

  float3 cameraPos;
  float planetRadius; // 6371e3

  float3 rayleighScatteringCoeff; // float3(5.5e-6, 13.0e-6, 22.4e-6), Rayleigh scattering coefficient
  float mieScatteringCoeff; // 21e-6, Mie scattering coefficient

  float atmosphereRadius; // 6471e3
  float rayleighScaleHeight; // 8e3, Rayleigh scale height
  float mieScaleHeight; // 1.2e3, Mie scale height
  float mieScatteringDir; // 0.988, Mie preferred scattering direction
}

float3 atmosphere(float3 r, float3 r0, float3 pSun) {
  // Calculate the step size of the primary ray.
  float2 p = rsi(r0, r, atmosphereRadius);
  if (p.x > p.y) return 0.0;

  p.y = min(p.y, rsi(r0, r, planetRadius).x);

  float iDist = p.y - p.x;
  float iStepSize = iDist / geometricSeries(iStepGrowth, float(iSteps));

  // Initialize the primary ray time.
  float iTime = iStepSize * 0.5;

  // Initialize accumulators for Rayleigh and Mie scattering.
  float3 totalRlh = 0.0;
  float3 totalMie = 0.0;

  // Initialize optical depth accumulators for the primary ray.
  float iOdRlh = 0.0;
  float iOdMie = 0.0;

  // Calculate the Rayleigh and Mie phases.
  float mu = dot(r, pSun);
  float mumu = mu * mu;
  float gg = mieScatteringDir * mieScatteringDir;
  float pRlh = 3.0 / (16.0 * PI) * (1.0 + mumu);
  float pMie = 3.0 / (8.0 * PI) * ((1.0 - gg) * (mumu + 1.0)) / (pow(abs(1.0 + gg - 2.0 * mu * mieScatteringDir), 1.5) * (2.0 + gg));

  // Sample the primary ray.
  for (int i = 0; i < iSteps; i++) {

    // Calculate the primary ray sample position.
    float3 iPos = r0 + r * iTime;

    // Calculate the height of the sample.
    float iHeight = length(iPos) - planetRadius;

    // Calculate the optical depth of the Rayleigh and Mie scattering for this step.
    float odStepRlh = exp(-iHeight / rayleighScaleHeight) * iStepSize;
    float odStepMie = exp(-iHeight / mieScaleHeight) * iStepSize;

    // Accumulate optical depth.
    iOdRlh += odStepRlh;
    iOdMie += odStepMie;

    // Calculate the step size of the secondary ray.
    float jStepSize = rsi(iPos, pSun, atmosphereRadius).y / geometricSeries(jStepGrowth, float(jSteps));

    // Initialize the secondary ray time.
    float jTime = jStepSize * 0.5;

    // Initialize optical depth accumulators for the secondary ray.
    float jOdRlh = 0.0;
    float jOdMie = 0.0;

    // Sample the secondary ray.
    [unroll]
    for (int j = 0; j < jSteps; j++) {

      // Calculate the secondary ray sample position.
      float3 jPos = iPos + pSun * jTime;

      // Calculate the height of the sample.
      float jHeight = length(jPos) - planetRadius;

      // Accumulate the optical depth.
      jOdRlh += exp(-jHeight / rayleighScaleHeight) * jStepSize;
      jOdMie += exp(-jHeight / mieScaleHeight) * jStepSize;

      // Increment the secondary ray time.
      jTime += jStepSize;
      jStepSize *= jStepGrowth;
    }

    // Calculate attenuation.
    float3 attn = exp(-(
      mieScatteringCoeff * (iOdMie + jOdMie) 
      + rayleighScatteringCoeff * (iOdRlh + jOdRlh)
      ));

    // Accumulate scattering.
    totalRlh += odStepRlh * attn;
    totalMie += odStepMie * attn;

    // Increment the primary ray time.
    iTime += iStepSize;
    iStepSize *= iStepGrowth;
  }

  // Calculate and return the final color.
  return sunIntensity * (pRlh * rayleighScatteringCoeff * totalRlh + pMie * mieScatteringCoeff * totalMie);
}

float3 uvToFisheyeCoord(float2 uv) {
  float2 n = uv * 2.0 - 1.0;  
  float R = length(n);
  float phiCos = cos(saturate(R) * PI * 0.6);
  float3 dir = float3(n.x / R, sign(phiCos) * pow(abs(phiCos), 1.3), n.y / R);  
  dir.xz *= sqrt(1.0 - dir.y * dir.y);
  return dir;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float3 cameraDir = uvToFisheyeCoord(pin.Tex);
  float3 color = atmosphere(cameraDir, cameraPos, sunDir);
  return float4(color, 1);
}