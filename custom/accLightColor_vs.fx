struct VS_IN {
  float4 PosL : POSITION;
  float3 NormalL : NORMAL;
  float2 Tex : TEXCOORD;
  float3 TangentL : TANGENT;
};

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

VS_Copy main(VS_IN vin, uint id: SV_VertexID) {
  VS_Copy vout;
  float2 fixed = ((vin.Tex % 1 + 1) % 1) * 2;
  vout.PosH = float4(fixed.x - 1, 1 - fixed.y, 1, 1);
  vout.Tex = vin.Tex;
  return vout;
}
