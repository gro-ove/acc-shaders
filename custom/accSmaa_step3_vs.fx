#include "accSmaa_common.hlsl"

VS_Smaa_step3 main(uint id: SV_VertexID) {
  VS_Smaa_step3 vout;
  vout.Tex = float2((float)(id / 2) * 2, 1 - (float)(id % 2) * 2);
  vout.PosH = float4(
    (float)(id / 2) * 4 - 1,
    (float)(id % 2) * 4 - 1, 0, 1);
  vout.Offset = mad(SMAA_RT_METRICS.xyxy, float4(1.0, 0.0, 0.0, 1.0), vout.Tex.xyxy);
  return vout;
}
