cbuffer cbData : register(b7) {
  float2 resolutionInv;
  float2 direction;
}

Texture2D txDiffuse : register(t0);

SamplerState samLinearSimple : register(s15) {
  Filter = LINEAR;
  AddressU = CLAMP;
  AddressV = CLAMP;
};

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 color = 0;
  float2 off1 = (float2)1.411764705882353 * direction;
  float2 off2 = (float2)3.2941176470588234 * direction;
  float2 off3 = (float2)5.176470588235294 * direction;
  color += sqrt(txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0)) * 0.1964825501511404;
  color += sqrt(txDiffuse.SampleLevel(samLinearSimple, pin.Tex + (off1 * resolutionInv), 0)) * 0.2969069646728344;
  color += sqrt(txDiffuse.SampleLevel(samLinearSimple, pin.Tex - (off1 * resolutionInv), 0)) * 0.2969069646728344;
  color += sqrt(txDiffuse.SampleLevel(samLinearSimple, pin.Tex + (off2 * resolutionInv), 0)) * 0.09447039785044732;
  color += sqrt(txDiffuse.SampleLevel(samLinearSimple, pin.Tex - (off2 * resolutionInv), 0)) * 0.09447039785044732;
  color += sqrt(txDiffuse.SampleLevel(samLinearSimple, pin.Tex + (off3 * resolutionInv), 0)) * 0.010381362401148057;
  color += sqrt(txDiffuse.SampleLevel(samLinearSimple, pin.Tex - (off3 * resolutionInv), 0)) * 0.010381362401148057;
  return color.rgba * color.rgba;
}
