#define SMAA_PRESET_HIGH
#include "accSmaa_common.hlsl"

float4 main(VS_Smaa_step3 pin) : SV_TARGET {
  return SMAANeighborhoodBlendingPS(pin.Tex, pin.Offset, txDiffuse, txBlendMap);
}