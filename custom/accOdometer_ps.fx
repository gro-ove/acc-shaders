SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  return float4(1, 1, 1, txDiffuse.Sample(samLinearSimple, pin.Tex).x);
}