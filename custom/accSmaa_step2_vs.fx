#include "accSmaa_common.hlsl"

VS_Smaa_step2 main(uint id: SV_VertexID) {
  VS_Smaa_step2 vout;
  vout.Tex = float2((float)(id / 2) * 2, 1 - (float)(id % 2) * 2);
  vout.PosH = float4(
    (float)(id / 2) * 4 - 1,
    (float)(id % 2) * 4 - 1, 0, 1);
  vout.Pix = vout.Tex * SMAA_RT_METRICS.zw;
  vout.Offset[0] = mad(SMAA_RT_METRICS.xyxy, float4(-0.25, -0.125, 1.25, -0.125), vout.Tex.xyxy);
  vout.Offset[1] = mad(SMAA_RT_METRICS.xyxy, float4(-0.125, -0.25, -0.125, 1.25), vout.Tex.xyxy);
  vout.Offset[2] = mad(SMAA_RT_METRICS.xxyy,
    float4(-2.0, 2.0, -2.0, 2.0) * float(SMAA_MAX_SEARCH_STEPS),
    float4(vout.Offset[0].xz, vout.Offset[1].yw));
  return vout;
}
