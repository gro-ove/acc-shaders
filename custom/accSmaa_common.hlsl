cbuffer cbData : register(b1) {
  float4 screenMetrics;
}

#define SMAA_HLSL_4_1
#define SMAA_RT_METRICS screenMetrics
#include "SMAA.hlsl"

SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txDiffuse : register(t0);
Texture2D txAreaTexMap : register(t1);
Texture2D txSearchTexMap : register(t2);
Texture2D txBlendMap : register(t3);

struct VS_Smaa_step1 {
  float4 PosH : SV_POSITION;
  float4 Offset[3] : OFFSET;
  float2 Tex : TEXCOORD0;
};

struct VS_Smaa_step2 {
  float4 PosH : SV_POSITION;
  float2 Pix : PIXCOORD;
  float4 Offset[3] : OFFSET;
  float2 Tex : TEXCOORD0;
};

struct VS_Smaa_step3 {
  float4 PosH : SV_POSITION;
  float4 Offset : OFFSET;
  float2 Tex : TEXCOORD0;
};