#!/bin/zsh
zmodload zsh/datetime
setopt BRACE_CCL BSD_ECHO EXTENDED_GLOB MULTIBYTE NO_CASE_GLOB NO_CSH_JUNKIE_LOOPS NO_MATCH \
    NUMERIC_GLOB_SORT RC_EXPAND_PARAM RC_QUOTES RCS RE_MATCH_PCRE SHORT_LOOPS

function fxc(){
  '/cygdrive/C/Program Files (x86)/Microsoft DirectX SDK (June 2010)/Utilities/bin/x64/fxc.exe' $@
}

function clean-text-data(){
  echo ${1##*// Input signature:} | sed -r 's/^[ \t]+([0-9]+: )?|\r//'
}

function prepare-original-text(){
  for n in original/binary/*.fxo; do 
    data=$( fxc '/dumpbin' $n )
    clean-text-data $data > original/text/${${n:t}%%.fxo}.txt
  done
}

function test-shader(){
  if [[ ! -e original/text/${1%%.fx}.txt ]]; then return 0; fi
  if ! diff -w original/text/${1%%.fx}.txt compiled/details/${1%%.fx}.txt > data/failed-diff.txt; then
    echo 'not the same: '${1%%.fx}' (difference:'\
        $(grep -e '>' data/failed-diff.txt | wc -l)')'
    
    mkdir -p data/failed
    cp data/failed-diff.txt data/failed/${1%%.fx}.txt
    # echo 'tar: 'original/text/${1%%.fx}.txt
    # echo 'src: 'compiled/details/${1%%.fx}.txt
    # < data/failed-diff.txt
    return 1
  fi
  return 0
}

function compile-shader(){
  name=${1:t}

  if [[ $name =~ ksSky ]]; then
    target=ps_4_1
    [[ $name =~ _vs.fx ]] && target=vs_4_1
  else
    target=ps_5_0
    [[ $name =~ _vs.fx ]] && target=vs_5_0
  fi


  if ! fxc /T $target /Ni /nologo /O3 /E main $1 \
      /Fodata/compiled.fxo /Fcdata/compiled-details.txt \
      $3 $4 $5 $6 $7 $8 $9 $10 >/dev/null; then
    return 1
  fi

  data=$( clean-text-data "$( < data/compiled-details.txt )" )
  als=( $( echo ${name%.fx}; grep -oPe '[aA]lias: \K.+' $1 ) )

  for a in $als; do
    cp data/compiled.fxo compiled/original/$a.fxo
    echo $data > compiled/details/$a.txt

    if [[ $2 == '--test' ]] && ! test-shader $a; then
      return 1
    fi
  done

  return 0
}

function convert-to-compiled-with-tests(){
  mkdir -p compiled/original
  mkdir -p compiled/details
  rm compiled/*.fxo(N) compiled/*/*.fxo(N) 2>/dev/null

  for n in recreated/*.fx; do
    name=${n:t}

    # echo 'compiling '$name
    if ! compile-shader $n --copy; then
      echo 'failed: '$name
      read _
    fi
  done
}

# To generate text files from binary files:
# prepare-original-text

# To convert all shaders from ./recreated  to ./compiled testing them for 
# full match with original shaders as well:
# convert-to-compiled-with-tests

# Custom stuff:
mkdir -p '/D/Games/Assetto Corsa/extension/shaders'

shader_sets=( )
shader_sets+=( MAIN_FX ) # Lighting FX, Tyres FX, SH ambient
# shader_sets+=( MAIN_NOFX ) # basic shaders with a bit of small optimizations
# shader_sets+=( SIMPLIFIED_FX ) # Lighting FX, Tyres FX, SH ambient; no reflections, no shadows
# shader_sets+=( SIMPLIFIED_NOFX ) # no reflections, no shadows
shader_sets+=( CUSTOM ) # custom non-AC shaders for internal usage

to_compile=()

# to_compile+=( custom/*.fx )
# to_compile+=( custom/accOdometer*.fx )
# to_compile+=( custom/accAccumulation*.fx )
# to_compile+=( custom/accBlur*.fx )
# to_compile+=( custom/accSmaa*.fx )
# to_compile+=( custom/accColorGrading*.fx )
to_compile+=( custom/accSkyHda*.fx )
# to_compile+=( recreated/*_ps.fx )
to_compile+=( recreated/ksSkyFX_[pv]s.fx )
# to_compile+=( recreated/ksPerPixelReflection_ps.fx )
# to_compile+=( recreated/*MultiMap_ps.fx )
# to_compile+=( recreated/*_vs.fx )
# to_compile+=( recreated/*UVMult_ps.fx )
# to_compile+=( recreated/ksPerPixel(|AT)_[pv]s.fx )
# to_compile+=( recreated/*Cloud*_ps.fx )
# to_compile+=( recreated/*Tyres*_*.fx )
# to_compile+=( recreated/ksPerPixel_ps.fx )
# to_compile+=( recreated/*Tree_[p]s.fx )
# to_compile+=( recreated/*Grass*_ps.fx )
# to_compile+=( recreated/ksWindscreen*.fx )
# to_compile+=( recreated/*Flag*_*.fx )
# to_compile+=( recreated/*BrakeDiscFX_[p]s.fx )
# to_compile+=( recreated/*BrakeDisc_[p]s.fx )
# to_compile+=( recreated/*Sky*_*.fx )
# to_compile+=( recreated/ksFakeCarShadows_*.fx )
# to_compile+=( recreated/ksPerPixelNM_ps.fx )
# to_compile+=( recreated/*SkidMark*_*.fx )
# to_compile+=( recreated/*(Particle)*_*.fx )
# to_compile+=( recreated/*(objsp)*_*.fx )
# to_compile+=( recreated/*Tyres*_[p]s.fx )
# to_compile+=( recreated/ks(Tyres(|FX)|BrakeDisc|PerPixel(|NM|MultiMap*|Reflection))_ps.fx )
# to_compile+=( recreated/ksTyresFX_[p]s.fx )
# to_compile+=( recreated/*emissive*_ps.fx )
# to_compile+=( recreated/ks[TW]*_ps.fx )
# to_compile+=( recreated/ksPerPixel(|AT|Reflection)_?s.fx )
# to_compile+=( recreated/ksMultilayer_fresnel_nm_ps.fx )
# to_compile+=( recreated/ksPerPixelMultiMap(|_AT)_NMDetail_ps.fx )
# to_compile+=( recreated/ksMultilayer*_ps.fx )
# to_compile+=( recreated/(*Skinned*)_[pv]s.fx )
# to_compile+=( recreated/(*Particle*)_ps.fx )
# to_compile+=( recreated/(*damage_dirt*)_ps.fx )
# to_compile+=( recreated/ksPerPixel_vs.fx )
# to_compile+=( recreated/ksPerPixelNM_[p]s.fx )
# to_compile+=( recreated/(*Reflection*)_ps.fx )
# to_compile+=( recreated/ksPerPixel(Alpha|AT|)_ps.fx )
# to_compile+=( recreated/(*Windscreen*)_ps.fx )
# to_compile+=( recreated/(*WindscreenFX*)_ps.fx )
# to_compile+=( recreated/(*MultiMap*)_ps.fx )
# to_compile+=( recreated/ksPerPixelMultiMap_ps.fx )
# to_compile+=( recreated/(*NM*)_ps.fx )
# to_compile+=( recreated/(*_damage_dirt*)_ps.fx )
# to_compile+=( recreated/(ksPerPixelMultiMap|ksPerPixelMultiMap_damage_dirt|ksPerPixelMultiMap_NMDetail|_ksMultilayer|_ksMultilayer_fresnel_nm|ksPerPixelNM|ksPerPixelAT_NM|_ksTree|ksPerPixelAT|ksPerPixel|_ksGrass)_ps.fx )

for SET in $shader_sets; do

  set_dest='/D/Games/Assetto Corsa/extension/shaders/'${SET:l}
  mkdir -p $set_dest
  # continue

  for n in $to_compile; do
    # as for broken glass, gotta check first, isn’t it a performance hit?
    if [[ $n = */acc* ]]; then 
      if [[ $SET != CUSTOM ]]; then continue; fi
    else
      if [[ $SET == CUSTOM ]]; then continue; fi
    fi

    if ! compile-shader $n -- '/DMODE_'$SET'=1' 2>data/error.txt; then
      echo 'failed: '${n:t}
      < data/error.txt
      read _
    fi

    grep warning data/error.txt

    test-shader ${n:t} && echo 'in order: '${${n:t}%%.fx}
    a=${${n:t}%%.fx}
    cp data/compiled.fxo $set_dest/$a.fxo

    als=( $( grep -oPe '[aA]lias: \K.+' $n ) )
    for a in $als; do
      echo 'save as '$a
      cp data/compiled.fxo $set_dest/$a.fxo
    done

    # if [[ $a = *_ps && -e '/D/Games/Assetto Corsa/system/shaders/win/'${a%%_ps}_meta.ini ]]; then
    #   if [[ ! -e $set_dest/${a%%_ps}_vs.fxo ]]; then
    #     cp '/D/Games/Assetto Corsa/system/shaders/win/'${a%%_ps}_vs.fxo -t $set_dest
    #   fi
    #   cp '/D/Games/Assetto Corsa/system/shaders/win/'${a%%_ps}_meta.ini -t $set_dest
    # fi
  done

done
exit 0

# Old stuff:
# for n in $1*.fx; do
#   echo 'compiling '$n
#   fxc /T vs_4_1 /Ni /nologo /O3 /E main /Fo.temporary.fxo /Fc.temporary.txt $n || read _
#   echo ${(L)$( grep -E '// [aA]pprox' .temporary.txt )%%//}
#   als=( $( grep -oPe '[aA]lias: \K.+' $n ) )
#   als+=${n%.fx}
#   for a in $als; do
#     echo 'save as '$a
#     cp .temporary.fxo compiled/$a.fxo
#     cp .temporary.fxo '/D/Games/Assetto Corsa/system/shaders/win/'$a.fxo
#   done
# done
# rm .temporary.fxo
# rm .temporary.txt

