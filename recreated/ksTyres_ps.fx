#define INCLUDE_TYRE_CB
#define CARPAINT_NM
#define USE_BLURRED_NM
#define SPECULAR_DIFFUSE_FIX
#define AMBIENT_TYRES_FIX
#define FORCE_BLURREST_REFLECTIONS
#define EXTRA_SHADOW_DARKER
// #define LIGHTING_MODEL_COOKTORRANCE
#define CALCULATELIGHTING_USESPECULARMULT_AS_VALUE
#define LIGHTING_SPECULAR_POWER_MULT specMult

#ifdef MICROOPTIMIZATIONS
  #define GETSHADOW_BIAS_MULTIPLIER 4
#else
  #define GETSHADOW_BIAS_MULTIPLIER 10
#endif

#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_Nm pin) : SV_TARGET {
  READ_VECTORS_NM

  float shadow = getShadow(normalW, SHADOWS_COORDS);
  APPLY_EXTRA_SHADOW_INC_DIFFUSE

  float4 txBlurValue = txBlur.Sample(samLinear, pin.Tex);
  float4 txDirtyValue = txDirty.Sample(samLinear, pin.Tex);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  txDiffuseValue = lerp(txDiffuseValue, txBlurValue, blurLevel);

  float dirtyLevelAdj = dirtyLevel * txDirtyValue.a;
  txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, txDirtyValue.rgb, dirtyLevelAdj);

  float alpha;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);

  float dirtyInv = 1 - dirtyLevel;
  float specMult = dirtyInv * ksSpecular * txDiffuseValue.a;

  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb, shadow, specMult, 1.0, extraShadow.y * VAO);
  LIGHTINGFX(lighting);

  float3 withReflection = calculateReflection(lighting, toCamera, normalW, (float3)1, 
    /* extraMultiplier= */ saturate(dirtyInv) * extraShadow.x, /* useBias= */ false, /* isCarPaint= */ false, /* useSkyColor= */ false);
  return withFog(withReflection, pin.Fog, 1);
}
