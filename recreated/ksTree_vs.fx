// #define FOG_NMUV2_VERSION
#define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"

PS_IN_PerPixel main(VS_IN vin)
{
  GENERIC_PIECE_NOSHADOWS(PS_IN_PerPixel);

  #ifdef ALLOW_TREE_SHADOWS
    [branch]
    if (ksEmissive.z < 0){
      shadows(posW - ksLightDirection * 4, SHADOWS_COORDS);
    } else {
      noShadows(SHADOWS_COORDS);
    }
  #endif

  return vout;
}
