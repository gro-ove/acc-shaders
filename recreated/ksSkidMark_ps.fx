#define NO_CARPAINT
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_SkidMark pin) : SV_TARGET {
  float3 normalW = normalize(pin.NormalW);
  float shadow = getShadow(normalW, SHADOWS_COORDS);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 lighting = calculateLighting(pin.PosC, normalW, txDiffuseValue.rgb, shadow, 1, 1, VAO);
  LIGHTINGFX(lighting);
  float4 result = float4(lighting, txDiffuseValue.a) * float4(1, 1, 1, pin.SkidMarkThing);
  return withFog(result.rgb, pin.Fog, result.a);
}
