#include "include_new/base/_include_vs.fx"
// alias: GLTextured_vs
// alias: ksCameraDirt_vs

bool is_zero(float3 v){
  return v.y == 0 && v.z == 0;
}

PS_IN_GL main(VS_IN vin) {
  PS_IN_GL vout;
  float4 posW, posV;

  // If you’d want to fix wobbling for GL shader, keep in mind that it’s being used for UI as well
  // vout.PosH = is_zero(vin.TangentL) ? toScreenSpace_original(vin.PosL, posW, posV) : toScreenSpace(vin.PosL, posW, posV);

  vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.Color = float4(vin.NormalL, vin.TangentL.x);
  vout.Tex = vin.Tex;
  vout.TangentH = (float3)0;
  return vout;
}
