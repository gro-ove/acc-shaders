#define SHADOWS_FILTER_SIZE 2

#define NO_CARPAINT
// #define NO_SHADOWS
#define NO_EXTAMBIENT
#define NO_EXTSPECULAR
#define LIGHTINGFX_TREE
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_PerPixel pin) : SV_TARGET {
  float3 normalW = normalize(pin.NormalW);

  float shadow = 1;
  #ifdef ALLOW_TREE_SHADOWS
    [branch]
    if (ksEmissive.z < 0){
      float4 shadowOffset = float4(txNoise.SampleLevel(samPoint, pin.Tex * 1721, 0).xy, 0, 0);
      shadow = getShadow(normalW, 
        pin.ShadowTex0 + shadowOffset * 0.12, 
        pin.ShadowTex1 + shadowOffset * 0.005, 
        pin.ShadowTex2 + shadowOffset * 0.001);
    }
  #endif

  float3 toCamera = normalize(pin.PosC);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb, shadow, 1, 1, VAO);
  LIGHTINGFX(lighting);
  return withFog(lighting, pin.Fog, txDiffuseValue.a);
}
