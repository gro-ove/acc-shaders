// #define SUPPORTS_NORMALS_AO
#define SET_AO_TO_ONE
#include "include_new/base/_include_vs.fx"

#ifdef ALLOW_TYRESFX
  #include "include_new/ext_tyresfx/_include_vs.fx"
#endif

PS_IN_Nm main(VS_IN vin) {
  PS_IN_Nm vout;
  float4 posW, posV;
  posW = mul(vin.PosL, ksWorld);
  vout.NormalW = normals(vin.NormalL);

  #ifdef ALLOW_TYRESFX
    ADJUST_WORLD_POS(posW, vout.NormalW);
    posV = mul(posW, ksView);
    vout.PosH = mul(posV, ksProjection);
  #else
    vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  #endif

  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);

  #ifndef NO_NORMALMAPS
    vout.TangentW = normals(vin.TangentL);
    vout.BitangentW = bitangent(vin.NormalL, vin.TangentL);
  #endif
  
  PREPARE_AO(vout.Ao);
  return vout;
}
