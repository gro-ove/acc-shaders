//#define DEBUG

#define NO_CARPAINT
#define SUPPORTS_BLUR_SHADOWS
#include "include_new/base/_include_ps.fx"

#ifdef BLUR_SHADOWS
#include "include_new/ext_windscreenfx/_include_ps.fx"
#endif

#ifdef WINDSCREEN_RAIN
#define PI 3.1415926

#define S(a, b, t) smoothstep(a, b, t)
#define SAW(b, t) S(0.0, b, t)*S(1.0, b, t)

#include "include_new/base/random.fx"

float2 DropLayer(float2 uv, float time, float dir)
{
	uv *= float2(0.08, 0.09);
	float y = uv.y*20.;

	uv.y += time * 0.75;
	float2 a = float2(6., 1.);
	float2 grid = a * 2.;
	float2 id = floor(uv*grid);

	float colShift = hash11(id.x);
	uv.y += colShift;

	id = floor(uv*grid);
	float3 n = hash31(id.x*35.2 + id.y*2376.1);
	float2 st = frac(uv*grid) - float2(.5, 0);

	float x = n.x - .5;

	float wiggle = sin(y + sin(y));
	x += wiggle * (.5 - abs(x))*(n.z - .5);
	x *= .7;
	float ti = frac(time + n.z);
	y = step(0.0, dir) * ((SAW(.85, ti) - .5)*.9 + .5) + 0.95*step(dir, 0.0);
	float2 p = float2(x, y);

	float d = length((st - p)*a.yx);

	float mainDrop = S(.4, .0, d);

	float r = sqrt(S(1., y, st.y));
	float cd = abs(st.x - x);
	float trail = S(.23*r, .15*r*r, cd);
	float trailFront = S(-.02, .02, sign(dir)*(st.y - y));
	trail *= trailFront * r*r;

	return S(0.0, 1.0, abs(dir)) * float2(mainDrop, trail);
}

float StaticDrops(float2 uv, float time)
{
	float2 id = floor(uv);
	uv = frac(uv) - 0.5;
	float nt = hash11(id.x*107.45 + id.y*3543.654);
	float2 n = hash22(float2(nt*234.1771, 43.7917*floor(time + nt)));

	float2 p = (n - 0.5)*0.7;
	float d = length(uv - p);
	float fade = 1.0-frac(time + nt);

	float c = S(0.3, 0.0, d)*frac(nt*8.0)*fade;
	return c;
}

float2 Drops(float4x4 trafo, float3 pos, float2 e, float4 time, float dir, float4 layermult, float4 layerscale)
{
	float2 uv = mul(float4(pos, 0.0), trafo).xy + e.xy;
	float2 uv2 = uv;
	float s = 0.0;
	s = StaticDrops(uv2*layerscale.x, time.x)*layermult.x;
	uv2 += 0.131*time.yz;
	s += StaticDrops(uv2*layerscale.y, 0.613*time.x)*layermult.y;
	uv2 += 0.117*time.yz;
	s += StaticDrops(uv2*layerscale.z, 0.571*time.x)*layermult.z;
	float2 m1 = 0.0;
	float2 m2 = 0.0;

	if (dir < 0.0) time.z *= 2.0;
	m1 = DropLayer(uv*layerscale.w, 8.0*time.z, dir)*layermult.w;
//	m2 = DropLayer(uv*layerscale.w*0.9, time.x*1.71, dir)*layermult.w;

	float c = s + m1.x + m2.x;
	c = S(0.3, 1.0, c);

	return float2(c, max(m1.y*layermult.w, m2.y*layermult.w));
}

float wiper_i(float3 pos, float4 time, float f, inout float3 n)
{
	float2 wpuv = mul(float4(pos, 1.0), rainWiperMatrix).xy;
	uint4 wti = txWiper.Load(int3(wpuv.x*1024.0, wpuv.y*512.0, 0));
	uint4 woutside = (wti > 0 ? 0 : 256);
	uint4 wedge = wti & 1;
	float4 wt = frac(time.w - (wti / 256.0)) + woutside + rainWiperOffset;
	if (wt.x > wt.y) { wt.x = wt.y; wedge.x = wedge.y; }
	if (wt.z > wt.w) { wt.z = wt.w; wedge.z = wedge.w; }
	if (wt.x > wt.z) { wt.x = wt.z; wedge.x = wedge.z; }
	float m = saturate(wt.x);
	if (wedge.x)
	{
		float r = saturate(m / (f*0.5));
		n.z += rainAmount * (1.0 - r) * (hash11n((frac(1.711*time.x) + wpuv.x + wpuv.y)) - 0.5);
		r = clamp(1.0 - 2.0 * r, r, 1.0);
		return r;
	}
	return m < 1.0 ? saturate(m / (f*0.5)) : 1.0;
}

float4 overlay(in float3 x, in float xa, in float3 y, float ya)
{
	float4 r;
	r.a = xa + ya * (1.0 - xa);
	r.rgb = r.a > 0.0 ? (x * xa + y * ya * (1.0 - xa)) / r.a : 0.0;
	return r;
}

void rain(inout float3 color, inout float alpha, in float3 posc, in float2 tex, in float3 normalW, float3 toCamera, 
	float surfaceAlpha, inout float fog)
{
	[branch]
	if (rainAmount > 0.0) {
		float4 time = rainTime;
		float speed = clamp(length(rainVelocity) * 0.01 - 0.01, 0.0, 1.0);

		float3 normalR = mul(normalW, (float3x3)rainInvModel);
		float4x4 trafo;
		bool front = false;
		float dir = pow(speed - 1.2, 2.0) - 1.0;
		if (abs(normalize(normalR.xz).x) > 0.95) {
			dir = saturate(dir);
			float s = sign(normalR.x);
			trafo = float4x4(float4(0.0, 0.0, s, 0.0), float4(0.0, 1.0, 0.0, 0.0), float4(s, 0.0, 0.0, 0.0), float4(0.0, 0.0, 0.0, 0.0));
			time.yz = float2(0.2 * s, 0.02) * time.yx;
		}
		else {
			front = true;
			float s = sign(normalR.z);
//			trafo = float4x4(float4(-s, 0.0, 0.0, 0.0), float4(0.0, 1.0, 0.0, 0.0), float4(0.0, 0.0, s, 0.0), float4(0.0, 0.0, 0.0, 0.0));
			trafo = rainWindscreenMatrix;
			time.yz = float2(0.0, 0.4 * time.z);
		}

		float3 pos = mul(float4(posc, 1.0) + ksCameraPosition, rainInvModel).xyz;

		float3 n = 0.0;
		float w = 1.0;
		if (front && rainWiperSpeed > 0.0) {
			w = wiper_i(pos, time, rainWiperSpeed, n);
		}

		float4 layerscale = 1.0 / float4(0.007, 0.0073, 0.0077, 0.01);
		float4 layermult = saturate(float4(S(-0.5, 1.0, rainAmount), S(-0.4, 1.0, rainAmount), S(-0.3, 1.0, rainAmount), S(0.25, 1.0, rainAmount)));
		layermult *= float4(2.0 - speed, 1.5 - speed, 1.5 - speed, 1.5 + speed);

		float2 e = float2(0.0003, 0.0);
		float2 c = Drops(trafo, pos, e.yy, time, dir, layermult, layerscale);
		float cx = Drops(trafo, pos, e.xy, time, dir, layermult, layerscale).x;
		float cy = Drops(trafo, pos, e.yx, time, dir, layermult, layerscale).x;
		n += mul(float4(cx - c.x, cy - c.x, 0.0 , 0.0), transpose(trafo)).xyz;
//		c.x = S(0.3, 1.0, c.x);
		float maxBlur = lerp(1.0, 4.0, rainAmount);
		float minBlur = 0.0;
		float blur = lerp(maxBlur - c.y, minBlur, S(0.1, 0.2, c.x));
		toCamera = float3(-toCamera.x, toCamera.y, toCamera.z);
		float3 rainDir = normalize(toCamera + n);
		float3 rainColor = getReflectionAt(rainDir, blur, true);
		fog += 0.2 * w * rainAmount * saturate(1.0-c.y);
		float rainAlpha = saturate(w * (0.3 * rainAmount + 6.0*length(n)));
		float4 final = overlay(rainColor, saturate(9 - surfaceAlpha * 10) * rainAlpha, color, alpha);
		color = final.rgb;
		alpha = final.a;
#ifdef DEBUG
		color = w*float3(c.x, 0.25*blur, 0.0);
		alpha = 1.0;
#endif
	}
}
#endif

float4 main(PS_IN_PerPixel pin) : SV_TARGET{
	READ_VECTORS
	float shadow = getShadow(normalW, SHADOWS_COORDS);
	float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
	float lightValue = saturate(dot(normalize(pin.PosC), -ksLightDirection.xyz) * shadow);
	float3 diffuse = ksLightColor.rgb * ksDiffuse * lightValue;
	float3 resultLight = calculateAmbient(normalW, (float3)1, (float3)1) + diffuse;
	float resultAlpha = txDiffuseValue.a < 0.5 ? txDiffuseValue.a * lightValue : txDiffuseValue.a;
	float3 resultColor = txDiffuseValue.rgb * resultLight;

	#ifdef WINDSCREEN_RAIN
		rain(resultColor, resultAlpha, pin.PosC, pin.Tex, normalW, toCamera, txDiffuseValue.a, pin.Fog);
	#endif
	
	return float4(saturate(pin.Fog) * (ksFogColor - resultColor) + resultColor, resultAlpha);
}
