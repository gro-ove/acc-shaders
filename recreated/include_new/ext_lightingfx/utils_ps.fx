float sqr(float a){
  return a * a;
}

bool testBs(float3 posW, float3 bsPosW, float bsRadiusSqr){
  float3 bsVec = posW - bsPosW;
  float bsDist = dot(bsVec, bsVec);
  return bsDist < bsRadiusSqr;
}

float luminance(float3 rgb){
  return dot(rgb, float3(0.2125, 0.7154, 0.0721));
}

float normalize2Colors(float3 rgb1, float3 rgb2){
  // return dot(rgb1, rgb2) / (max(rgb1.x, max(rgb1.y, rgb1.z)) * max(rgb2.x, max(rgb2.y, rgb2.z)) + 0.0001);
  return dot(rgb1, rgb2) / (dot(rgb1, 1) * dot(rgb2, 1) + 0.0001);
}

float4 normalizeWithDistance(float3 vec){
  float dist = length(vec);
  return float4(vec / dist, dist);
}

float getDiffuseMultiplier(float3 normal, float3 lightDir) {
  #ifdef LIGHTINGFX_TREE
    return 0.75; // saturate(lerp(1, dot(normal, lightDir), concentration * 0.25));
  #else
    return saturate(dot(normal, lightDir) * 0.88 + 0.12);
  #endif
}

float getDiffuseMultiplier(float3 normal, float3 lightDir, float concentration) {
  return saturate(lerp(1, dot(normal, lightDir), concentration));
}

float3 closestPointOnSegment(float3 a, float3 ab, float distInv, float3 c, out float abValue) {
  abValue = dot(c - a, ab) * distInv;
  return a + saturate(abValue) * ab;
}

float lerpInv10(float val, float start, float lengthInv){
  // return saturate(1.0 - (val - start) * lengthInv);
  return saturate(start - val * lengthInv);
}

float lerpInv10(float val, float start){
  return saturate(start - val);
}

float lerpInv01(float val, float start, float lengthInv){
  return saturate(val * lengthInv - start);
}

/*float getTrimmedAttenuation(float rangeStart, float rangeLengthInv, float trimStart, float trimLengthInv, float d) {
  float df = max(lerpInv01(d, trimStart, trimLengthInv) - lerpInv10(d, rangeStart, rangeLengthInv), 0.0);
  return df * df;
}*/

float getTrimmedAttenuation(float rangeLengthInv, float trimStart, float trimLengthInv, float d) {
  float df = max(lerpInv01(d, trimStart, trimLengthInv) - saturate(d * rangeLengthInv), 0.0);
  return df * df;
}

float3 getDoubleSpotCone(float3 lightDirW, float3 toLight, 
    float spotCosStart, float3 attenuation,
    float secondSpotCosStart, float secondSpotCosLengthInv, float3 secondAttenuation){  
  float cosAngle = dot(lightDirW, -toLight);
  return lerpInv10(cosAngle, spotCosStart) * attenuation 
    + lerpInv10(cosAngle, secondSpotCosStart, secondSpotCosLengthInv) * secondAttenuation;
}

float getSpotCone(float3 lightDirW, float3 toLight, float spotCosStart){  
  float cosAngle = dot(lightDirW, -toLight);
  return lerpInv10(cosAngle, spotCosStart);
}

float3 getEdge(float3 lightUpW, float3 toLight, float3 spotEdgeOffset){  
  float up = dot(lightUpW, -toLight);
  return saturate(spotEdgeOffset - up);
}

float getAttenuation(float rangeStart, float rangeLengthInv, float d) {
  float df = lerpInv10(d, rangeStart, rangeLengthInv);
  return df * df;
}

float GetNDotH(float3 normalW, float3 posW, float3 lightDir) {
  float3 toEye = normalize(posW);
  float3 halfway = normalize(toEye - lightDir);
  return saturate(dot(-halfway, normalW));
}

float GetNDotHL(float3 normalW, float3 posW, float3 lightDir) {
  float3 toEye = normalize(posW);
  float3 halfway = normalize(toEye + lightDir);
  return saturate(dot(halfway, normalW));
}

float calculateSpecularLight(float nDotH, float exp) {
  return pow(nDotH, max(exp, 0.1));
}

float calculateSpecularLight_byValues(float3 normal, float3 position, float3 lightDir, float exp) {
  float nDotH = GetNDotH(normal, position, lightDir);
  return calculateSpecularLight(nDotH, exp);
}
