cbuffer cbExtTyres : register(b6) {
  float3 wheelPos;
  float rimRadiusSqr;

  float3 dirUp;
  float tyreSquashDistance;

  float3 dirForward;
  float tyreSkewX;

  float3 dirSide;
  float tyreSkewY;

  float3 contactNormal;
  float profileSizeInv;

  float3 contactPos;
  float tyreWidthInv;

  float damageWidthK;
  float damageOffsetK;
  float skewMultInv;
  float damageSpecExpMult;

  float damageSpecMult;
  float damageReflMult;
  float damageOcclusionMult;
  float damageNormalsMult;

  float shadowBiasMult;
  float3 dirtGrassColorA;

  float dirtGrassNormalsMult;
  float3 dirtGrassColorB;
};