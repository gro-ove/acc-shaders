#define M_PI 3.141592653589793
#define DIRT_BLUR_LAYERS 3

float2 getTyreCoords(float3 posW){
  float3 dir = normalize(posW - wheelPos);
  float sideDot = dot(dirSide, dir);
  dir = normalize(dir - dirSide * sideDot);

  float upDot = dot(dirUp, dir);
  float forwardDot = dot(dirForward, dir);
  float angle = acos(upDot) / (M_PI * 2.0);
  float onCircle = forwardDot > 0.0 ? angle : 1 - angle;
  return float2(onCircle, sideDot);
}

void getTyreColor(float3 posW, inout float3 normalW, float3 targentW, float3 bitangentW, 
    inout float lightingMult, inout float specMult, inout float specExpMult, inout float reflMult){
  float2 tyreCoords = getTyreCoords(posW);

  float toUp = damageWidthK - abs(tyreCoords.y + damageOffsetK) * 2.0;
  toUp = saturate((toUp * 0.7 - 0.5) * 3.0 + 0.5);

  #if DIRT_BLUR_LAYERS == 1
    float blurLayer = 0.5;
  #else
    float blurLevelAdj = blurLevel * blurLevel;
    float blurLayer = (0.5 / DIRT_BLUR_LAYERS) + blurLevelAdj * (1.0 - 1.0 / DIRT_BLUR_LAYERS);
  #endif

  float2 dmgBoth = txTyresDamage.Sample(samLinearSimple, float2(tyreCoords.x, blurLayer)) * toUp;
  dmgBoth = dmgBoth * dmgBoth;

  float dmg = max(dmgBoth.x, dmgBoth.y);

  [branch]
  if (dmg < 0.001) return;

  float2 uv = float2(tyreCoords.x, tyreCoords.y + 0.5);
  float4 txTyresDamageNmValue = txTyresDamageNm.SampleLevel(samLinear, uv, blurLevel);
  float4 txTyresMarblesNmValue = txTyresMarblesNm.SampleLevel(samLinear, uv, blurLevel);

  // blending two together
  txTyresDamageNmValue = (txTyresDamageNmValue * dmgBoth.x + txTyresMarblesNmValue * dmgBoth.y) / (0.0001 + dmgBoth.x + dmgBoth.y);

  float3 txTyresDamageNmAligned = txTyresDamageNmValue.xyz * 2 - (float3)1;
  txTyresDamageNmAligned.y *= -1;

  float dmgMat = dmg * saturate(abs(txTyresDamageNmAligned.y) * 5);
  specMult *= lerp(1.0, damageSpecMult, dmgMat);
  specExpMult *= lerp(1.0, damageSpecExpMult, dmgMat);
  reflMult *= lerp(1.0, damageReflMult, dmgMat);
  lightingMult = 1.0 - (dmgBoth.x * txTyresDamageNmValue.a + dmgBoth.y * txTyresMarblesNmValue.a) * damageOcclusionMult;

  float3 T = cross(dirSide, normalW);
  float3 B = cross(T, normalW);
  float3x3 m = float3x3(B, normalW, T);
  float3 normalDmg = normalize(mul(transpose(m), txTyresDamageNmAligned.xzy));
  normalW = lerp(normalW, normalDmg, dmg * damageNormalsMult);
}

void getTyreColorDirt(float3 posW, inout float3 normalW, float3 targentW, float3 bitangentW, 
    inout float3 color, inout float lightingMult, inout float specMult, inout float specExpMult, inout float reflMult){
  float2 tyreCoords = getTyreCoords(posW);

  float toUp = damageWidthK - abs(tyreCoords.y + damageOffsetK) * 2.0;
  float toUp1 = saturate(toUp * 2.1 - 1.0);
  float toUp2 = saturate(toUp * 2.1 - 0.23);

  #if DIRT_BLUR_LAYERS == 1
    float blurLayer = 0.5;
  #else
    float blurLevelAdj = blurLevel * blurLevel;
    float blurLayer = (0.5 / DIRT_BLUR_LAYERS) + blurLevelAdj * (1.0 - 1.0 / DIRT_BLUR_LAYERS);
  #endif

  float2 dmgBoth = txTyresDamage.SampleLevel(samLinearSimple, float2(tyreCoords.x, blurLayer), 0) * toUp1;
  float2 dirtBoth = txTyresDirt.SampleLevel(samLinearSimple, float2(tyreCoords.x, blurLayer), 0) * toUp2;
  dmgBoth = dmgBoth * dmgBoth;

  float dmg = max(dmgBoth.x, dmgBoth.y);

  // [branch]
  // if (dmg < 0.001) return;

  float2 uv = float2(tyreCoords.x, tyreCoords.y + 0.5);
  float4 txTyresDamageNmValue = txTyresDamageNm.SampleLevel(samLinear, uv, blurLevel);
  float4 txTyresMarblesNmValue = txTyresMarblesNm.SampleLevel(samLinear, uv, blurLevel);

  float level = txTyresGrassComb.CalculateLevelOfDetail(samLinearSimple, uv);
  float4 txTyresGrassCombValue = lerp(
    txTyresGrassComb.SampleLevel(samLinearSimple, float2(uv.x, ((uv.y + 2) % 1.0) * 0.5), level),
    txTyresGrassComb.SampleLevel(samLinearSimple, float2(uv.x, ((uv.y + 2) % 1.0) * 0.5 + 0.5), level), blurLevel);

  float grassK = saturate((dirtBoth.x - txTyresGrassCombValue.w) * 12);
  color = lerp(color, dirtGrassColorA + dirtGrassColorB * txTyresGrassCombValue.z, grassK);

  // blending two together
  txTyresDamageNmValue = (txTyresDamageNmValue * dmgBoth.x + txTyresMarblesNmValue * dmgBoth.y) / (0.001 + dmgBoth.x + dmgBoth.y);

  float3 txTyresDamageNmUnaligned = lerp(txTyresDamageNmValue.xyz, float3(txTyresGrassCombValue.xy, 1), grassK);
  float3 txTyresDamageNmAligned = txTyresDamageNmUnaligned * 2 - (float3)1;
  txTyresDamageNmAligned.y *= -1;

  float dmgMat = dmg * saturate(abs(txTyresDamageNmAligned.y) * 5);
  specMult *= lerp(1.0, damageSpecMult, dmgMat);
  specExpMult *= lerp(1.0, damageSpecExpMult, dmgMat);
  reflMult *= lerp(1.0, damageReflMult, dmgMat);
  lightingMult = 1.0 - (dmgBoth.x * txTyresDamageNmValue.a + dmgBoth.y * txTyresMarblesNmValue.a) 
    * damageOcclusionMult * (1 - grassK);

  specMult = lerp(specMult, 0.1 / (ksSpecular + 0.001), grassK);
  specExpMult = lerp(specExpMult, 75 / (ksSpecularEXP + 0.001), grassK);
  reflMult = lerp(reflMult, 0.1 / (fresnelMaxLevel + 0.001), grassK);

  float3 T = cross(dirSide, normalW);
  float3 B = cross(T, normalW);
  float3x3 m = float3x3(B, normalW, T);
  float3 normalDmg = normalize(mul(transpose(m), txTyresDamageNmAligned.xzy));
  normalW = lerp(normalW, normalDmg, lerp(dmg * damageNormalsMult, dirtGrassNormalsMult, grassK));
}

#define TYRESFX(x, y, z, w) getTyreColor(pin.PosC, normalW, tangentW, bitangentW, x, y, z, w);
#define TYRESFX_DIRT(c, x, y, z, w) getTyreColorDirt(pin.PosC, normalW, tangentW, bitangentW, c, x, y, z, w);