Texture2D<float2> txTyresDamage : register(t15);
Texture2D<float2> txTyresDirt : register(t16);
Texture2D txTyresDamageNm : register(t9);
Texture2D txTyresMarblesNm : register(t11);
Texture2D txTyresGrassComb : register(t13);