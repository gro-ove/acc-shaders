#define M_PI 3.141592653589793

float3 calculateNewPosition(float3 inputPosition, float3 dirRoadForward, float sideDot, float squashedSkewMult){
  float3 posD = inputPosition - contactPos;
  float squashedBase = 1 - dot(posD, contactNormal) * profileSizeInv;
  float squashed = saturate(squashedBase);

  // if (squashed > 0.0) inputPosition += contactNormal;

  float squashedUp = squashed * squashed;
  float squashedHalf = sin(squashed * M_PI);

  float squashedSkew = squashedBase * skewMultInv + (1 - skewMultInv);
  squashedSkew = saturate(squashedSkew);
  squashedSkew = pow(squashedSkew, 1.2) * squashedSkewMult;

  inputPosition.xyz += contactNormal * squashedUp * tyreSquashDistance;
  inputPosition.xyz += dirSide * (sideDot * squashedHalf * tyreSquashDistance + squashedSkew * tyreSkewX);
  inputPosition.xyz += dirRoadForward * squashedSkew * tyreSkewY;
  return inputPosition;
}

void squashTyre(inout float3 posW, inout float3 normalW){
  float3 posD = posW - contactPos;
  float squashedBase = 1 - dot(posD, contactNormal) * profileSizeInv;
  // if (profileSizeInv == 0 || squashedBase < -2) return;

  float3 dirRoadForward = -normalize(cross(contactNormal, dirSide));
  float sideDot = dot(posD, dirSide) * tyreWidthInv;

  float3 posE = posW - (wheelPos + ksCameraPosition.xyz);
  float lenE = dot(posE, posE);
  float squashedSkewMult = saturate((lenE - rimRadiusSqr) * profileSizeInv * 2.0);

  // float3 u = calculateNewPosition(posW + contactNormal * 0.001, dirRoadForward, sideDot, squashedSkewMult);
  posW = calculateNewPosition(posW, dirRoadForward, sideDot, squashedSkewMult);

  // float3 n = normalize(u - posW);
  // float3 nj = cross(n, sideDot < 0 ? -dirRoadForward : dirRoadForward);
  // normalW = lerp(normalW, nj, saturate(abs(sideDot) * 3.0 - 1.0));
  // normalW = lerp(normalW, nj, saturate(dot(normalW, sideDot < 0 ? -dirSide : dirSide) * 4.0 - 2.0));
}

#define ADJUST_WORLD_POS(x, y) squashTyre(x.xyz, y.xyz);
