#if defined ALLOW_PERVERTEX_AO && defined SUPPORTS_COLORFUL_AO
  #define PREPARE_AO(x) x = vin.TangentL / 1e5 + 1;
#elif defined ALLOW_PERVERTEX_AO && defined SUPPORTS_OBJSP_AO
  #define PREPARE_AO(x) x = length(vin.NormalL) * 2.0 - 1.0;
#elif defined ALLOW_PERVERTEX_AO && defined SUPPORTS_NORMALS_AO
  #define PREPARE_AO(x) x = length(vin.TangentL) * 2.0 - 1.0;
#elif defined ALLOW_PERVERTEX_AO && defined SET_AO_TO_ONE
  #define PREPARE_AO(x) x = 1.0;
#else
  #define PREPARE_AO(x) ;
#endif
