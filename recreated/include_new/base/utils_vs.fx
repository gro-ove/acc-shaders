// Utils

#ifndef EXCLUDE_PEROBJECT_CB

  float3 normals(float3 input){
    return mul(input, (float3x3)ksWorld);
  }

  float3 bitangent(float3 normalL, float3 tangentL){
    #ifndef NO_NORMALMAPS
      return mul(cross(tangentL, normalL), (float3x3)ksWorld);
    #else
      return 0;
    #endif
  }

#endif

#ifdef NO_SHADOWS
  #define SHADOWS_COORDS 0
  void shadows(float4 posW, float v){ }
  void noShadows(float v){ }
#else
  #define SHADOWS_COORDS vout.ShadowTex0, vout.ShadowTex1, vout.ShadowTex2
  void shadows(float4 posW, out float4 tex0, out float4 tex1, out float4 tex2){
    tex0 = mul(posW, ksShadowMatrix0);
    tex1 = mul(posW, ksShadowMatrix1);
    tex2 = mul(posW, ksShadowMatrix2);
  }

  void noShadows(out float4 tex0, out float4 tex1, out float4 tex2){
    tex0 = 0;
    tex1 = 0;
    tex2 = 0;
  }
#endif

#ifdef NO_REFLECTIONS
  #define isAdditive 1
#endif

// Fog

float calculateFog(float4 posV){
  #if defined OPTIMIZE_FOG

    float c = pow(2, posV.z / ksFogLinear * (5.770780 * 2));
    return saturate((1 - c) / (1 + c)) * ksFogBlend;

  #elif defined FOG_NMUV2_VERSION

    float a = -posV.z / ksFogLinear * 5.770780;
    float b = pow(2, a);
    float c = pow(2, -a);
    float d = 1 / (b + c);
    return saturate((b - c) * d) * ksFogBlend;

  #elif defined FOG_PARTICLE_VERSION

    float a = -posV.z / ksFogLinear * 5.770780;
    float b = pow(2, a);
    float c = pow(2, -a);
    float d = b - c;
    float c0 = c + b;
    float c1 = 1 / c0;
    return saturate(d * c1) * ksFogBlend;

  #else

    float a = -posV.z / ksFogLinear * 5.770780;
    float b = pow(2, a);
    float c = pow(2, -a);
    return saturate((b - c) / (b + c)) * ksFogBlend;

  #endif
}

// Local-to-sceen conversion

#ifndef EXCLUDE_PEROBJECT_CB

float4 toScreenSpace(float4 posL, out float4 posW, out float4 posV){

#if defined FIX_SCREENSPACE

  float4x4 ksWorldFixed = ksWorld;
  ksWorldFixed[3].xyz -= ksCameraPosition.xyz;

  float4x4 ksViewFixed = ksView;
  ksViewFixed[3].xyz = (float3)0;
  
  float4 posWfixed = mul(posL, ksWorldFixed);
  ADJUST_WORLD_POS(posWfixed);

  posW = posWfixed + float4(ksCameraPosition.xyz, 0);
  posV = mul(posWfixed, ksViewFixed);
  return mul(posV, ksProjection);

#else

  posW = mul(posL, ksWorld);
  posV = mul(posW, ksView);
  return mul(posV, ksProjection);

#endif

}

// Skinned

#ifndef SKINNED_WEIGHT_CHECK
#define SKINNED_WEIGHT_CHECK(a) (a != 0)
#endif

float4 toScreenSpaceSkinned(float4 posL, float3 normalL, float3 tangentL, float4 boneWeights, float4 boneIndices, 
    out float4 posW, out float3 normalW, out float3 tangentW, out float4 posV){
  #if ! defined FIX_SCREENSPACE

    posW = 0;
    normalW = 0;
    tangentW = 0;

    for (int i = 0; i < 4; i++){
      float weight = boneWeights[i];
      if (SKINNED_WEIGHT_CHECK(weight)){
        uint index = (uint)boneIndices[i];
        float4x4 bone = bones[index];
        posW += mul(bone, posL) * weight;
        normalW += mul((float3x3)bone, normalL) * weight;
        tangentW += mul((float3x3)bone, tangentL) * weight;
      }
    }

    posV = mul(posW, ksView);
    return mul(posV, ksProjection);

  #else

    float4x4 ksViewFixed = ksView;
    ksViewFixed[3].xyz = (float3)0;

    float4 posWfixed = 0;
    normalW = 0;
    tangentW = 0;

    for (int i = 0; i < 4; i++){
      float weight = boneWeights[i];
      if (SKINNED_WEIGHT_CHECK(weight)){
        uint index = (uint)boneIndices[i];
        float4x4 bone = bones[index];
        bone[0].w -= ksCameraPosition.x;
        bone[1].w -= ksCameraPosition.y;
        bone[2].w -= ksCameraPosition.z;
        posWfixed += mul(bone, posL) * weight;
        normalW += mul((float3x3)bone, normalL) * weight;
        tangentW += mul((float3x3)bone, tangentL) * weight;
      }
    }

    posW = posWfixed + float4(ksCameraPosition.xyz, 0);
    posV = mul(posWfixed, ksViewFixed);
    return mul(posV, ksProjection);

  #endif
}

#endif
