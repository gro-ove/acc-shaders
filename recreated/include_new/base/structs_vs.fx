struct VS_IN {
  float4 PosL : POSITION;
  float3 NormalL : NORMAL;
  float2 Tex : TEXCOORD;
  float3 TangentL : TANGENT;
};

struct VS_IN_objsp {
  float4 PosL : POSITION;
  float3 NormalL : NORMAL;
  float2 Tex : TEXCOORD;

  #ifdef SUPPORTS_COLORFUL_AO
    float3 TangentL : TANGENT;
  #endif
};

struct VS_IN_Particle {
  float4 PosL : POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD;

  #ifdef SUPPORTS_COLORFUL_AO
    // float3 TangentL : TANGENT;
  #endif
};

struct VS_IN_Skinned {
  float4 PosL : POSITION;
  float3 NormalL : NORMAL;
  float2 Tex : TEXCOORD;
  float3 TangentL : TANGENT;
  float4 BoneWeights : TEXCOORD1;
  float4 BoneIndices : TEXCOORD2;
};