#include "cbuffers_common.fx"

#ifdef INCLUDE_MULTILAYER_CB
cbuffer cbMultiLayer : register(b5) {
  float multR;                       // Offset:    0 Size:     4
  float multG;                       // Offset:    4 Size:     4
  float multB;                       // Offset:    8 Size:     4
  float2 multA;                      // Offset:   16 Size:     8
  float magicMult;                   // Offset:   24 Size:     4
  float2 boh;                        // Offset:   32 Size:     8 [unused]
}
#endif

#ifdef INCLUDE_MULTILAYER2_CB
cbuffer cbMultiLayer : register(b5) {
  float2 multR;                      // Offset:    0 Size:     8
  float2 multG;                      // Offset:    8 Size:     8
  float2 multB;                      // Offset:   16 Size:     8
  float2 multA;                      // Offset:   24 Size:     8
  float magicMult;                   // Offset:   32 Size:     4
}
#endif

#ifdef INCLUDE_MULTILAYER3_CB
cbuffer cbMultiLayer : register(b5) {
  float multR;                       // Offset:    0 Size:     4
  float multG;                       // Offset:    4 Size:     4
  float multB;                       // Offset:    8 Size:     4
  float2 multA;                      // Offset:   16 Size:     8
  float magicMult;                   // Offset:   24 Size:     4
  float fresnelC;                    // Offset:   28 Size:     4
  float fresnelEXP;                  // Offset:   32 Size:     4
  float fresnelMaxLevel;             // Offset:   36 Size:     4
  float tarmacSpecularMultiplier;    // Offset:   40 Size:     4
  float2 detailNMMult;               // Offset:   48 Size:     8
}
#endif

#ifdef INCLUDE_GRASS_CB
cbuffer cbGrassBuffer : register(b10) {  
  float2 scale;                      // Offset:    0 Size:     8
  float gain;                        // Offset:    8 Size:     4
  float boh;                         // Offset:   12 Size:     4 [unused]
}
#endif

#ifdef INCLUDE_ALPHA_CB
cbuffer cbAlphaLevel : register(b12) {  
  float alpha;                       // Offset:    0 Size:     4
}
#endif

#ifdef CARPAINT_NM_UVMULT
cbuffer cbTextureMult : register(b11) {  
  float diffuseMult;                 // Offset:    0 Size:     4
  float normalMult;                  // Offset:    4 Size:     4
  float2 bo;                         // Offset:    8 Size:     8 [unused]
}
#endif

#ifdef INCLUDE_TYRE_CB

cbuffer cbTyre : register(b10) {
  float blurLevel;                   // Offset:    0 Size:     4
  #ifdef USE_BRAKE_DISC_MODE
  float glowLevel;                  // Offset:    4 Size:     4
  #else
  float dirtyLevel;                  // Offset:    4 Size:     4
  #endif
  float fresnelC;                    // Offset:    8 Size:     4
  float fresnelEXP;                  // Offset:   12 Size:     4
  float isAdditive;                  // Offset:   16 Size:     4
  float fresnelMaxLevel;             // Offset:   20 Size:     4
}

static bool nmObjectSpace = false;

#elif !defined NO_CARPAINT
cbuffer cbCarPaint : register(b5) {
#ifdef CARPAINT_SIMPLE
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float isAdditive;                  // Offset:    8 Size:     4
  float fresnelMaxLevel;             // Offset:   12 Size:     4
#elif defined CARPAINT_NM
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float nmObjectSpace;               // Offset:    8 Size:     4
  float isAdditive;                  // Offset:   12 Size:     4
  float fresnelMaxLevel;             // Offset:   16 Size:     4
  float3 boh;                        // Offset:   20 Size:    12 [unused]
#elif defined CARPAINT_NM_UVMULT
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float boh;                         // Offset:   12 Size:     4 [unused]
#elif defined CARPAINT_SKINNED_NM
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float isAdditive;                  // Offset:   12 Size:     4
  float useDetail;                   // Offset:   16 Size:     4
  float detailUVMultiplier;          // Offset:   20 Size:     4
  float detailNormalBlend;           // Offset:   24 Size:     4
  float boh;                         // Offset:   28 Size:     4 [unused]
#elif defined CARPAINT_SKINNED
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float nmObjectSpace;               // Offset:   12 Size:     4
  float isAdditive;                  // Offset:   16 Size:     4 [unused]
  float useDetail;                   // Offset:   20 Size:     4
  float detailUVMultiplier;          // Offset:   24 Size:     4
  float boh;                         // Offset:   28 Size:     4 [unused]
#elif defined CARPAINT_SIMPLE_REFL
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float nmObjectSpace;               // Offset:   12 Size:     4
  float isAdditive;                  // Offset:   16 Size:     4 [unused]
  float useDetail;                   // Offset:   20 Size:     4
  float detailUVMultiplier;          // Offset:   24 Size:     4
  float shadowBiasMult;              // Offset:   28 Size:     4
#elif defined CARPAINT_AT_NMDETAILS
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float isAdditive;                  // Offset:   12 Size:     4
  float useDetail;                   // Offset:   16 Size:     4
  float detailUVMultiplier;          // Offset:   20 Size:     4
  float shadowBiasMult;              // Offset:   24 Size:     4
  float normalUVMultiplier;          // Offset:   28 Size:     4 [unused]
  float detailNormalBlend;           // Offset:   32 Size:     4
  float3 boh;                        // Offset:   36 Size:    12 [unused]
#elif defined CARPAINT_NMDETAILS
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float isAdditive;                  // Offset:   12 Size:     4
  float useDetail;                   // Offset:   16 Size:     4
  float detailUVMultiplier;          // Offset:   20 Size:     4
  float shadowBiasMult;              // Offset:   24 Size:     4
  float detailNormalBlend;           // Offset:   28 Size:     4
#elif defined CARPAINT_AT
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float nmObjectSpace;               // Offset:   12 Size:     4
  float isAdditive;                  // Offset:   16 Size:     4
  float useDetail;                   // Offset:   20 Size:     4
  float detailUVMultiplier;          // Offset:   24 Size:     4
  float shadowBiasMult;              // Offset:   28 Size:     4
  float normalUVMultiplier;          // Offset:   32 Size:     4
  float3 boh;                        // Offset:   36 Size:    12 [unused]
#elif defined CARPAINT_GLASS
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float nmObjectSpace;               // Offset:    8 Size:     4 [unused]
  float isAdditive;                  // Offset:   12 Size:     4 [unused]
  float fresnelMaxLevel;             // Offset:   16 Size:     4
  float glassDamage;                 // Offset:   20 Size:     4
  float2 boh;                        // Offset:   24 Size:     8 [unused]
#else
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float nmObjectSpace;               // Offset:   12 Size:     4
  float isAdditive;                  // Offset:   16 Size:     4
  float useDetail;                   // Offset:   20 Size:     4
  float detailUVMultiplier;          // Offset:   24 Size:     4
  float shadowBiasMult;              // Offset:   28 Size:     4
  float4 damageZones;                // Offset:   32 Size:    16 [unused]
  float dirt;                        // Offset:   48 Size:     4 [unused]
  float sunSpecular;                 // Offset:   52 Size:     4
  float sunSpecularEXP;              // Offset:   56 Size:     4

  #ifndef LIGHTING_SPECULAR_EXP
    #define LIGHTING_SPECULAR_EXP sunSpecularEXP
  #endif

  #ifndef LIGHTING_SPECULAR_EXP
    #define LIGHTING_SPECULAR_POWER sunSpecular
  #endif

#endif
}
#endif
