// For larger (integer stepped) ranges
#define HASHSCALE1 .1031
#define HASHSCALE3 float3(.1031, .1030, .0973)
#define HASHSCALE4 float4(.1031, .1030, .0973, .1099)

// For smaller normalized input ranges like 0-1 UVs
#define HASHSCALE1N 443.8975
#define HASHSCALE3N float3(443.897, 441.423, 437.195)
#define HASHSCALE4N float4(443.897, 441.423, 437.195, 444.129)

float hash11(float p)
{
	float3 p3 = frac(p * HASHSCALE1);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.x + p3.y) * p3.z);
}

float hash12(float2 p)
{
	float3 p3 = frac(float3(p.xyx) * HASHSCALE1);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.x + p3.y) * p3.z);
}

float hash13(float3 p3)
{
	p3 = frac(p3 * HASHSCALE1);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.x + p3.y) * p3.z);
}

float2 hash21(float p)
{
	float3 p3 = frac(p * HASHSCALE3);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.xx + p3.yz)*p3.zy);

}

float2 hash22(float2 p)
{
	float3 p3 = frac(float3(p.xyx) * HASHSCALE3);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.xx + p3.yz)*p3.zy);

}

float2 hash23(float3 p3)
{
	p3 = frac(p3 * HASHSCALE3);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.xx + p3.yz)*p3.zy);
}

float3 hash31(float p)
{
	float3 p3 = frac(p * HASHSCALE3);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.xxy + p3.yzz)*p3.zyx);
}

float3 hash32(float2 p)
{
	float3 p3 = frac(float3(p.xyx) * HASHSCALE3);
	p3 += dot(p3, p3.yxz + 19.19);
	return frac((p3.xxy + p3.yzz)*p3.zyx);
}

float3 hash33(float3 p3)
{
	p3 = frac(p3 * HASHSCALE3);
	p3 += dot(p3, p3.yxz + 19.19);
	return frac((p3.xxy + p3.yxx)*p3.zyx);

}

float4 hash41(float p)
{
	float4 p4 = frac(p * HASHSCALE4);
	p4 += dot(p4, p4.wzxy + 19.19);
	return frac((p4.xxyz + p4.yzzw)*p4.zywx);

}

float4 hash42(float2 p)
{
	float4 p4 = frac(float4(p.xyxy) * HASHSCALE4);
	p4 += dot(p4, p4.wzxy + 19.19);
	return frac((p4.xxyz + p4.yzzw)*p4.zywx);

}

float4 hash43(float3 p)
{
	float4 p4 = frac(float4(p.xyzx)  * HASHSCALE4);
	p4 += dot(p4, p4.wzxy + 19.19);
	return frac((p4.xxyz + p4.yzzw)*p4.zywx);
}

float4 hash44(float4 p4)
{
	p4 = frac(p4  * HASHSCALE4);
	p4 += dot(p4, p4.wzxy + 19.19);
	return frac((p4.xxyz + p4.yzzw)*p4.zywx);
}

float hash11n(float p)
{
	float3 p3 = frac(p * HASHSCALE1N);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.x + p3.y) * p3.z);
}

float hash12n(float2 p)
{
	float3 p3 = frac(float3(p.xyx) * HASHSCALE1N);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.x + p3.y) * p3.z);
}

float hash13n(float3 p3)
{
	p3 = frac(p3 * HASHSCALE1N);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.x + p3.y) * p3.z);
}

float2 hash21n(float p)
{
	float3 p3 = frac(p * HASHSCALE3N);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.xx + p3.yz)*p3.zy);

}

float2 hash22n(float2 p)
{
	float3 p3 = frac(float3(p.xyx) * HASHSCALE3N);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.xx + p3.yz)*p3.zy);

}

float2 hash23n(float3 p3)
{
	p3 = frac(p3 * HASHSCALE3N);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.xx + p3.yz)*p3.zy);
}

float3 hash31n(float p)
{
	float3 p3 = frac(p * HASHSCALE3N);
	p3 += dot(p3, p3.yzx + 19.19);
	return frac((p3.xxy + p3.yzz)*p3.zyx);
}

float3 hash32n(float2 p)
{
	float3 p3 = frac(float3(p.xyx) * HASHSCALE3N);
	p3 += dot(p3, p3.yxz + 19.19);
	return frac((p3.xxy + p3.yzz)*p3.zyx);
}

float3 hash33n(float3 p3)
{
	p3 = frac(p3 * HASHSCALE3N);
	p3 += dot(p3, p3.yxz + 19.19);
	return frac((p3.xxy + p3.yxx)*p3.zyx);

}

float4 hash41n(float p)
{
	float4 p4 = frac(p * HASHSCALE4N);
	p4 += dot(p4, p4.wzxy + 19.19);
	return frac((p4.xxyz + p4.yzzw)*p4.zywx);

}

float4 hash42n(float2 p)
{
	float4 p4 = frac(float4(p.xyxy) * HASHSCALE4N);
	p4 += dot(p4, p4.wzxy + 19.19);
	return frac((p4.xxyz + p4.yzzw)*p4.zywx);

}

float4 hash43n(float3 p)
{
	float4 p4 = frac(float4(p.xyzx)  * HASHSCALE4N);
	p4 += dot(p4, p4.wzxy + 19.19);
	return frac((p4.xxyz + p4.yzzw)*p4.zywx);
}

float4 hash44n(float4 p4)
{
	p4 = frac(p4  * HASHSCALE4N);
	p4 += dot(p4, p4.wzxy + 19.19);
	return frac((p4.xxyz + p4.yzzw)*p4.zywx);
}
