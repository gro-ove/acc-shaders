#ifdef ALLOW_PERVERTEX_AO
  #define AO_NORMALS centroid float Ao : COLOR;
  #define AO_COLORFUL centroid float3 Ao : COLOR1;
#else
  #define AO_NORMALS 
  #define AO_COLORFUL 
#endif

#ifdef NO_SHADOWS
  #define SHADOWS_COORDS_ITEMS
#else
  #define SHADOWS_COORDS_ITEMS \
    float4 ShadowTex0 : TEXCOORD3;\
    float4 ShadowTex1 : TEXCOORD4;\
    float4 ShadowTex2 : TEXCOORD5;
#endif

struct PS_IN_GL {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD;
  float3 TangentH : TANGENT;
};

struct PS_IN_Font {
  float4 PosH : SV_POSITION;
  float3 Color : COLOR;
  float2 Tex : TEXCOORD;
  float3 TangentH : TANGENT;
};

struct PS_IN_ShadowGetAt {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD1;
};

struct PS_IN_PerPixel {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float Fog : TEXCOORD6;
  SHADOWS_COORDS_ITEMS
  AO_COLORFUL
};

struct PS_IN_Multilayer {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  SHADOWS_COORDS_ITEMS
  float3 PosW : TEXCOORD6;
  float Fog : TEXCOORD7;
  AO_COLORFUL
};

struct PS_IN_MultilayerFresnel {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  SHADOWS_COORDS_ITEMS
  float3 PosW : TEXCOORD6;
  float Fog : TEXCOORD7;
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD8;
    float3 BitangentW : TEXCOORD9;
  #endif
  AO_NORMALS
};

struct PS_IN_Grass {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  SHADOWS_COORDS_ITEMS
  float Fog : TEXCOORD6;
  float2 GrassThing : TEXCOORD7;
  AO_COLORFUL
};

struct PS_IN_AtNs {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float Fog : TEXCOORD6;
  AO_COLORFUL
};

struct PS_IN_Nm {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  SHADOWS_COORDS_ITEMS
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  float Fog : TEXCOORD8;
  AO_NORMALS
};

struct PS_IN_Uv2 {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  SHADOWS_COORDS_ITEMS
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  float2 Tex2 : TEXCOORD8;
  float Fog : TEXCOORD9;
  AO_NORMALS
};

struct PS_IN_Sky {
  float4 PosH : SV_POSITION;
  float3 PosW : TEXCOORD2;
  float3 NormalL : TEXCOORD3;
  float2 Tex : TEXCOORD0;
  float3 LightH : TEXCOORD4;
};

struct PS_IN_Particle {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD0;
  float Extra : TEXCOORD1;
  float Fog : TEXCOORD2;
  SHADOWS_COORDS_ITEMS
  float3 NormalW : TEXCOORD6;
  float3 PosC : TEXCOORD7;
  // AO_COLORFUL
};

struct PS_IN_SkidMark {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  SHADOWS_COORDS_ITEMS
  centroid float SkidMarkThing : TEXCOORD6;
  float Fog : TEXCOORD7;
};

struct PS_IN_Tree {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float Fog : TEXCOORD6;
  AO_COLORFUL
};