#include "_flags.fx"

#include "structs_vs.fx"
#include "structs_ps.fx"
#include "cbuffers_vs.fx"
#include "utils_vs.fx"
#include "..\ext_vao\_include_vs.fx"

#ifdef ALLOW_TREE_SHADOWS
  #define PS_TREE_INPUT PS_IN_PerPixel
#else
  #define PS_TREE_INPUT PS_IN_Tree
#endif

#define GENERIC_PIECE_NOSHADOWS(_OUTPUTTYPE)\
  _OUTPUTTYPE vout;\
  float4 posW, posV;\
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);\
  vout.NormalW = normals(vin.NormalL);\
  vout.PosC = posW.xyz - ksCameraPosition.xyz;\
  vout.Tex = vin.Tex;\
  vout.Fog = calculateFog(posV);\
  PREPARE_AO(vout.Ao);

#define GENERIC_PIECE(_OUTPUTTYPE)\
  GENERIC_PIECE_NOSHADOWS(_OUTPUTTYPE);\
  shadows(posW, SHADOWS_COORDS);

#ifdef NO_NORMALMAPS
  #define GENERIC_PIECE_NM(_OUTPUTTYPE)\
    GENERIC_PIECE(_OUTPUTTYPE);
#else
  #define GENERIC_PIECE_NM(_OUTPUTTYPE)\
    GENERIC_PIECE(_OUTPUTTYPE);\
    vout.TangentW = normals(vin.TangentL);\
    vout.BitangentW = bitangent(vin.NormalL, vin.TangentL);
#endif