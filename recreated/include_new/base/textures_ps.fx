Texture2D txDiffuse : register(t0);
Texture2D txVariation : register(t1);
Texture2D txNormal : register(t1);
Texture2D txDirty : register(t2);
Texture2D txGlow : register(t2);
Texture2D txBlur : register(t3);
Texture2D txNormalBlur : register(t4);
Texture2D txMask : register(t1);
Texture2D txDetailR : register(t2);
Texture2D txDetailG : register(t3);
Texture2D txDetailB : register(t4);
Texture2D txDetailA : register(t5);
Texture2D txMaps : register(t2);
Texture2D txDetail : register(t3);
Texture2D txNoise : register(t20);
Texture2D txDetailNM : register(t21);
Texture2D txNormalDetail : register(t4);
Texture2D txDamage : register(t4);
Texture2D txDust : register(t5);

#if defined OPTIMIZED_SHADOWS && defined SHADOWS_ARRAY
Texture2DArray<float> txShadow : register(t6);
#else
Texture2D<float> txShadow0 : register(t6);
Texture2D<float> txShadow1 : register(t7);
Texture2D<float> txShadow2 : register(t8);
#endif

TextureCube txCube : register(t10);
Texture2D txDamageMask : register(t21);