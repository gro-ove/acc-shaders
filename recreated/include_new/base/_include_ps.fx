#include "_flags.fx"

#include "samplers_ps.fx"
#include "textures_ps.fx"
#include "cbuffers_ps.fx"
#include "structs_ps.fx"

#include "../ext_lighting_models/_include_ps.fx"

#if ! defined NO_SHADOWS && defined OPTIMIZE_SHADOWS && !( defined SUPPORTS_BLUR_SHADOWS && defined BLUR_SHADOWS )
  #ifndef SHADOWS_COORDS
    #define SHADOWS_COORDS pin.ShadowTex0, pin.ShadowTex1, pin.ShadowTex2
  #endif
  #include "../ext_shadows/_include_ps.fx"
#endif

#ifdef ALLOW_LIGHTINGFX

  #include "../ext_lightingfx/_include_ps.fx"

#else

  #define LIGHTINGFX(x) ;
  #define TYRESFX(x, y, z, w) ;
  #define getSHValue(x) 1

  #if defined ALLOW_PERVERTEX_AO && defined SUPPORTS_AO
    #define VAO pin.Ao
  #else
    #define VAO 1
  #endif

#endif

// #ifdef ALLOW_TYRESFX
//   #include "../ext_tyresfx/_include_ps.fx"
// #endif

#ifdef ALLOW_TREE_SHADOWS
  #define PS_TREE_INPUT PS_IN_PerPixel
#else
  #define PS_TREE_INPUT PS_IN_Tree
#endif

#include "utils_ps.fx"