cbuffer cbMaterial : register(b4) {
  float ksAmbient;
  float ksDiffuse;
  float ksSpecular;
  float ksSpecularEXP;
  float3 ksEmissive;
  float ksAlphaRef;
}

cbuffer cbCamera : register(b0) {
  float4x4 ksView;
  float4x4 ksProjection;
  float4x4 ksMVPInverse;
  float4 ksCameraPosition;
  float ksNearPlane;
  float ksFarPlane;
  float ksFOV;
  float ksDofFactor;
}

#ifndef EXCLUDE_PEROBJECT_CB
cbuffer cbPerObject : register(b1) {
  float4x4 ksWorld;
  float shadowR1;
  float3 shadowPos;
  float shadowR2;
  float shadowOpacity;
  float shadowOpacityDarker;
  float extraAmbientBrightness;
}
#endif

cbuffer cbLighting : register(b2) {  
  float4 ksLightDirection;
  float4 ksAmbientColor;
  float4 ksLightColor;
  float4 ksHorizonColor;
  float4 ksZenithColor;
  float ksExposure;
  float ksScreenWidth;
  float ksScreenHeight;
  float ksFogLinear;
  float ksFogBlend;
  float3 ksFogColor;
  float ksCloudCover;
  float ksCloudCutoff;
  float ksCloudColor;
  float ksCloudOffset;
  float ksMinExposure;
  float ksMaxExposure;
  float ksDofFocus;
  float ksDofRange;
  float ksSaturation;
  float ksGameTime;
  float2 boh44;
}

#ifndef NO_SHADOWS
cbuffer cbShadowMaps : register(b3) {
  float4x4 ksShadowMatrix0;
  float4x4 ksShadowMatrix1;
  float4x4 ksShadowMatrix2;
  float3 bias;
  float textureSize;
}
#endif

#ifdef INCLUDE_PARTICLE_CB 
cbuffer cbParticle : register(b5) {
  float emissiveBlend;
  float minDistance;
  float shaded;
  float boh;
}
#endif