SamplerState samLinear : register(s0) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerState samPoint : register(s2) {
  Filter = POINT;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerComparisonState samShadow : register(s1) {
  Filter = COMPARISON_MIN_MAG_MIP_LINEAR;
  AddressU = BORDER;
  AddressV = BORDER;
  AddressW = BORDER;
  BorderColor = 1;
  ComparisonFunc = LESS;
};

SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};
