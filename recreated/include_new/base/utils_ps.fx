// Shadows

#define READ_VECTORS\
  float3 toCamera = normalize(pin.PosC);\
  float3 normalW = normalize(pin.NormalW);

#ifdef NO_NORMALMAPS
  #define READ_VECTORS_NM\
    READ_VECTORS\
    float3 tangentW = float3(1, 0, 0);
    float3 bitangentW = float3(1, 0, 0);
#else
  #define READ_VECTORS_NM\
    READ_VECTORS\
    float3 tangentW = normalize(pin.TangentW);\
    float3 bitangentW = normalize(pin.BitangentW);
#endif


#ifdef NO_SHADOWS

  #ifndef SHADOWS_COORDS
    #define SHADOWS_COORDS 0, 0, 0
  #endif

  float getShadow(float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2, float biasMultiplier){
    return 1;
  }

  float getShadow(float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2){
    return 1;
  }

#elif ! defined OPTIMIZE_SHADOWS && !( defined SUPPORTS_BLUR_SHADOWS && defined BLUR_SHADOWS )

  #ifndef SHADOWS_COORDS
    #define SHADOWS_COORDS pin.ShadowTex0, pin.ShadowTex1, pin.ShadowTex2
  #endif

  bool checkCascade(float4 uv){
    return -1 < uv.x && uv.x < 1 && -1 < uv.y && uv.y < 1;
  }

  float cascadeShadowStep(float deltaZ, float4 uv, Texture2D<float> tx){
    float result = 0;
    float comparisonValue = uv.z - deltaZ;
    uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

    [unroll]
    for (int x = -1; x <= 1; x++){
      [unroll]
      for (int y = -1; y <= 1; y++){
        #ifdef MICROOPTIMIZATIONS
        result += tx.SampleCmpLevelZero(samShadow, uv.xy + textureSize * float2(x, y), comparisonValue);
        #else
        result += tx.SampleCmpLevelZero(samShadow, uv.xy + float2(y * textureSize, x * textureSize), comparisonValue);
        #endif
      }
    }

    return result;
  }

  float getShadow(float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2, float biasMultiplier){
    float deltaZBase = max(1 - abs(dot(normalW, ksLightDirection.xyz)), 0.1) * biasMultiplier;

    if (checkCascade(shadowTex0)){
      return cascadeShadowStep(deltaZBase * bias.x, shadowTex0, txShadow0) / 9;
    }

    if (checkCascade(shadowTex1)){
      return cascadeShadowStep(deltaZBase * bias.y, shadowTex1, txShadow1) / 9;
    }

    if (checkCascade(shadowTex2)){
      return min(cascadeShadowStep(deltaZBase * bias.z, shadowTex2, txShadow2) / 9 
          + max(-(shadowTex2.y + 0.5), 0) * 2, 1);
    }

    return 1;
  }

  float getShadow(float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2){
    #ifdef USE_SHADOW_BIAS_MULT
      float biasMultiplier = shadowBiasMult + 1;
      return getShadow(normalW, shadowTex0, shadowTex1, shadowTex2, biasMultiplier);
    #elif defined GETSHADOW_BIAS_MULTIPLIER
      return getShadow(normalW, shadowTex0, shadowTex1, shadowTex2, GETSHADOW_BIAS_MULTIPLIER);
    #else
      return getShadow(normalW, shadowTex0, shadowTex1, shadowTex2, 1);
    #endif
  }

#endif

// Fix for tiling

float4 textureSampleVariation(Texture2D tx, SamplerState sam, float2 uv){
  float k = txNoise.Sample(samLinear, 0.05 * uv).x;

  float2 duvdx = ddx(uv);
  float2 duvdy = ddy(uv);
    
  float l = k * 8.0;
  float i = floor(l);
  float f = frac(l);

  float2 offa = sin(float2(3.0, 7.0) * (i + 0.0));
  float2 offb = sin(float2(3.0, 7.0) * (i + 1.0));
  float4 cola = tx.SampleGrad(sam, uv + offa, duvdx, duvdy);
  float4 colb = tx.SampleGrad(sam, uv + offb, duvdx, duvdy);

  return lerp(cola, colb, smoothstep(0.2, 0.8, f - 0.1 * dot(cola - colb, 1)));
}

// Normals

#if !defined NO_CARPAINT || defined USE_GETNORMALW

#ifdef USE_GETNORMALW
#define nmObjectSpace false
#endif

#ifndef GETNORMALW_SAMPLER
#define GETNORMALW_SAMPLER samLinear
#endif

#ifndef GETNORMALW_OS_SAMPLER
#define GETNORMALW_OS_SAMPLER samLinearSimple
#endif

#ifdef CARPAINT_DAMAGES

float3 getDamageNormalW(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW, float damage, 
    out float normalAlpha){
  #ifdef NO_NORMALMAPS
    normalAlpha = 0.0;
    return normalW;
  #else

    #ifndef GETNORMALW_NORMALIZED_INPUT
      normalW = normalize(normalW);
    #endif

    #ifdef GETNORMALW_XYZ_TX
      float4 txNormalValue = txNormal.Sample(samLinearSimple, uv).xzyw;
    #else
      float4 txNormalValue = txNormal.Sample(samLinearSimple, uv).xyzw;
    #endif

    normalAlpha = txNormalValue.a;

    #ifdef CARPAINT_GLASS
      normalAlpha = damage * txNormalValue.a;
    #endif

    [flatten]
    if (nmObjectSpace){
      float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
      txNormalAligned.z *= -1;
      float3x3 m = (float3x3)ksWorld;
      return normalize(mul(txNormalAligned, m));
    } else {
      float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
    txNormalAligned = normalize(txNormalAligned);
      #ifdef CARPAINT_GLASS
        float3x3 m = float3x3(tangentW, bitangentW, normalW);
    #else
        float3x3 m = float3x3(tangentW, normalW, bitangentW);
    #endif
      return lerp(normalW, normalize(mul(transpose(m), txNormalAligned)), damage);
    }

  #endif
}

#endif

#if defined CARPAINT_NM || defined USE_GETNORMALW || defined CARPAINT_AT

float3 getNormalW(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW, out float alpha){
  #ifdef NO_NORMALMAPS
    #ifdef USE_BLURRED_NM
      alpha = 1.0;
    #else 
      #ifdef GETNORMALW_UV_MULT
        alpha = txNormal.Sample(GETNORMALW_OS_SAMPLER, uv * GETNORMALW_UV_MULT).a;
      #else
        alpha = txNormal.Sample(GETNORMALW_OS_SAMPLER, uv).a;
      #endif
    #endif

    return normalW;
  #else

    #ifdef USE_BLURRED_NM
      alpha = 1;

      float3 T = normalize(tangentW);
      float3 B = normalize(bitangentW);
      float4 txNormalValue = txNormal.Sample(samLinearSimple, uv);
      float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
      float4 txNormalBlurValue = txNormalBlur.Sample(samLinearSimple, uv);
      float3 txNormalBlurAligned = txNormalBlurValue.xyz * 2 - (float3)1;
      float3x3 m = float3x3(T, normalW, B);
      float3 normalStatic = normalize(mul(transpose(m), txNormalAligned.xzy));
      float3 normalBlur = normalize(mul(transpose(m), txNormalBlurAligned.xzy));
      return lerp(normalStatic, normalBlur, blurLevel);
    #else 
      if (nmObjectSpace){
        #ifdef GETNORMALW_UV_MULT
          float4 txNormalValue = txNormal.Sample(GETNORMALW_OS_SAMPLER, uv * GETNORMALW_UV_MULT);
        #else
          float4 txNormalValue = txNormal.Sample(GETNORMALW_OS_SAMPLER, uv);
        #endif
        alpha = txNormalValue.a;
        float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
        txNormalAligned.z *= -1;
        float3x3 m = (float3x3)ksWorld;
        return normalize(mul(txNormalAligned, m));
      } else {
        float3 T = normalize(tangentW);
        float3 B = normalize(bitangentW);
        float4 txNormalValue = txNormal.Sample(GETNORMALW_SAMPLER, uv);
        alpha = txNormalValue.a;
        float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
        float3x3 m = float3x3(T, normalW, B);
        return normalize(mul(transpose(m), txNormalAligned.xzy));
      }
    #endif

  #endif
}

#elif defined CARPAINT_NM_UVMULT
#else

float3 getNormalW(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW, bool objectSpace){
  #ifndef GETNORMALW_NORMALIZED_INPUT
    normalW = normalize(normalW);
  #endif

  #ifdef NO_NORMALMAPS
    return normalW;
  #else
    if (objectSpace){
      #ifdef GETNORMALW_XYZ_TX
        float4 txNormalValue = txNormal.Sample(GETNORMALW_SAMPLER, uv);
        float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
        txNormalAligned.z *= -1;
      #else
        float4 txNormalValue = txNormal.Sample(GETNORMALW_SAMPLER, uv);
        float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
        txNormalAligned.z *= -1;
      #endif
      float3x3 m = (float3x3)ksWorld;
      return normalize(mul(txNormalAligned, m));
    } else {
      #ifdef GETNORMALW_NORMALIZED_TB
        float3 T = tangentW;
        float3 B = bitangentW;
      #else
        float3 T = normalize(tangentW);
        float3 B = normalize(bitangentW);
      #endif

      #ifdef GETNORMALW_XYZ_TX
        float3 txNormalValue = txNormal.Sample(samLinearSimple, uv).xyz;
      #else
        float3 txNormalValue = txNormal.Sample(samLinearSimple, uv).xzy;
      #endif

      float3 txNormalAligned = txNormalValue * 2 - (float3)1;
      float3x3 m = float3x3(T, normalW, B);
      return normalize(mul(transpose(m), txNormalAligned.xzy));
    }
  #endif
}

float3 getNormalW(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW){
  #ifdef NO_NORMALMAPS
    return normalW;
  #elif defined CARPAINT_NMDETAILS || defined CARPAINT_SIMPLE
    return getNormalW(uv, normalW, tangentW, bitangentW, false);
  #else
    return getNormalW(uv, normalW, tangentW, bitangentW, nmObjectSpace);
  #endif
}

#endif

#endif

// Reflections (new)

float4 getSkyColor(float3 direction){
  float v = saturate(900 / ksFogLinear);
  float3 color;
  if (direction.y > 0) {
    color = lerp(ksHorizonColor.rgb, ksZenithColor.rgb, 1 / rsqrt(direction.y));
    color = lerp(color, ksAmbientColor.r * 0.5, v);
  } else {
    color = lerp(ksHorizonColor.rgb, float3(0.15, 0.125, 0.125), saturate(direction.y * -7));
    color = lerp(color, ksAmbientColor.r * 0.5, v);
  }
  return float4(color, v);
}

float3 getReflectionAt(float3 reflDir, float blur, bool useBias){
  #ifdef NO_REFLECTIONS
    float v = saturate(900 / ksFogLinear);
    float3 color = lerp(ksHorizonColor.rgb, ksZenithColor.rgb, saturate(reflDir.y));
    color = lerp(color, ksAmbientColor.r * 0.5, v);
    color = lerp(color, float3(0.05, 0.11, 0.08), saturate(reflDir.y * -60 / (blur + 1) + 0.1));
    return color * 1.5;
    // return lerp(ksHorizonColor, ksZenithColor, saturate(reflDir.y)).rgb * 1.5;
  #elif defined FORCE_BLURREST_REFLECTIONS
    return txCube.SampleLevel(samLinearSimple, reflDir, 100).rgb;
  #else
    if (useBias){
      float level = txCube.CalculateLevelOfDetail(samLinearSimple, reflDir);
      return txCube.SampleLevel(samLinearSimple, reflDir, blur + level * saturate(3.5 - blur)).rgb;
    } else {
      return txCube.SampleLevel(samLinearSimple, reflDir, blur).rgb;
    }
  #endif
}

#ifndef NO_CARPAINT

  #ifdef CARPAINT_NM_UVMULT
    #define IS_ADDITIVE_VAR 0
  #elif defined CARPAINT_GLASS
    #define IS_ADDITIVE_VAR 1
  #else
    #define IS_ADDITIVE_VAR isAdditive
  #endif

  float4 calculateReflection(float4 lighting, float3 toCamera, float3 normalW, float3 txMapsValue,
      float extraMultiplier, bool useBias, bool isCarPaint, bool useSkyColor = false){
    float fresnelMaxLevelAdj = fresnelMaxLevel * extraMultiplier;
    float facingToCamera = dot(normalW, -toCamera);
    float fresnel = pow(saturate(1 - facingToCamera), max(fresnelEXP, 1));
    float reflPower = min(fresnel + fresnelC * extraMultiplier, fresnelMaxLevelAdj);

    float3 reflDir = normalize(reflect(toCamera, normalW));
    reflDir.x *= -1;

    float reflBlur = saturate(1 - txMapsValue.y * ksSpecularEXP / (isCarPaint && IS_ADDITIVE_VAR == 2 ? 8 : 255)) * 6;
    float3 reflColor = (useSkyColor ? getSkyColor(reflDir).rgb : getReflectionAt(reflDir, reflBlur, useBias)) * txMapsValue.z;
    return float4(
      lighting.rgb + (useSkyColor || IS_ADDITIVE_VAR == 1 ? reflColor : reflColor - lighting.rgb) * reflPower,
      IS_ADDITIVE_VAR == 1 ? lighting.w : lerp(lighting.w, 1, reflPower));
  }

  float3 calculateReflection(float3 lighting, float3 toCamera, float3 normalW, float3 txMapsValue,
      float extraMultiplier, bool useBias, bool isCarPaint, bool useSkyColor = false){
    return calculateReflection(float4(lighting, 1), toCamera, normalW, txMapsValue, extraMultiplier, 
      useBias, isCarPaint, useSkyColor).rgb;
  }

#endif

// Lighting

float3 calculateDiffuse(float3 normalW){
  float lightValue = saturate(dot(normalW, -ksLightDirection.xyz));
  return ksLightColor.rgb * ksDiffuse * lightValue;
}

float3 calculateAmbient(float3 normalW, float3 ambientAoMult, float3 txEmissiveValue/* = float3(1, 1, 1)*/){
  #if defined ALLOW_SH_AMBIENT && ! defined NO_EXTAMBIENT
    float4 s = getSHValue(normalW);
    return max(0, s.rgb * ambientAoMult * ksAmbient + ksEmissive * txEmissiveValue);
  #else
    #ifdef MICROOPTIMIZATIONS
      float ambientMultiplier = saturate(normalW.y * 0.25 + 0.75);
    #else
      float upMultiplier = normalW.y * 0.5 + 0.5;
      float ambientMultiplier = saturate(upMultiplier * 0.5 + 0.5);
    #endif
    return max(0, ksAmbientColor.rgb * ambientAoMult * ksAmbient * ambientMultiplier + ksEmissive * txEmissiveValue);
  #endif
}

float3 calculateSpecular(float3 normalW, float3 toCameraW, float shadow){
  float specularBase = saturate(dot(normalize(toCameraW - ksLightDirection.xyz), normalW));
  return pow(specularBase, max(ksSpecularEXP, 1)) * ksSpecular * shadow * ksLightColor.rgb;
}

// Basic shaders

#if defined NO_CARPAINT || defined CARPAINT_SIMPLE || defined CARPAINT_NM || defined CARPAINT_NM_UVMULT || defined CARPAINT_GLASS

float3 calculateLighting_base(float3 normalW, float shadow, float3 ambientAoMult){
  float dotValue = saturate(dot(normalW, -ksLightDirection.xyz));

  // Experimental: maybe it’s a bug? Let’s fix it
  // #ifdef CALCULATELIGHTING_SPECIAL_ALPHA
  //   float3 light = ksLightColor.rgb * dotValue;
  //   float3 base = light * ksDiffuse + calculateAmbient(normalW, ambientAoMult, (float3)1);
  // #else
    float3 light = ksLightColor.rgb * ksDiffuse * dotValue;
    float3 base = light * shadow + calculateAmbient(normalW, ambientAoMult, (float3)1);
  // #endif

  return base;
}

float3 calculateLighting_base(float3 normalW, float3 txDiffuseValue, float shadow, float3 ambientAoMult){
  #ifdef AMBIENT_TYRES_FIX
    float dotValue = saturate(dot(normalW, -ksLightDirection.xyz));
    float3 light = ksLightColor.rgb * ksDiffuse * dotValue;
    return light * shadow * txDiffuseValue.rgb 
      + calculateAmbient(normalW, ambientAoMult, (float3)1) * (saturate(normalW.y * 1.4 + 0.6) * extraAmbientBrightness + txDiffuseValue.rgb);
  #else
    return calculateLighting_base(normalW, shadow, ambientAoMult) * txDiffuseValue.rgb;
  #endif
}

float3 calculateLighting_spec(float3 toCamera, float3 normalW, float shadow,
    float ksSpecularMultiplier = 1.0, float ksSpecularExpMultiplier = 1.0){
  #ifdef CALCULATELIGHTING_USESPECULARMULT_AS_VALUE
    float ksSpecularAdj = ksSpecularMultiplier;
  #else
    float ksSpecularAdj = ksSpecular * ksSpecularMultiplier;
  #endif

  #ifdef SPECULAR_DIFFUSE_FIX
    shadow *= saturate(dot(normalW, -ksLightDirection.xyz) * 4);
  #endif

  return reflectanceModel(-toCamera, -ksLightDirection.xyz, normalW, 
    max(ksSpecularEXP * ksSpecularExpMultiplier, 1))
    * ksSpecularAdj * shadow * ksLightColor.rgb;
}

float3 calculateLighting(float3 toCamera, float3 normalW, float3 txDiffuseValue, float shadow,
    float ksSpecularMultiplier, float ksSpecularExpMultiplier, float3 ambientAoMult){
  #if defined SIMPLIFED_SPECULARS && defined NO_EXTSPECULAR
    return calculateLighting_base(normalW, txDiffuseValue, shadow, ambientAoMult);
  #else
    return calculateLighting_base(normalW, txDiffuseValue, shadow, ambientAoMult) 
      + calculateLighting_spec(toCamera, normalW, shadow, ksSpecularMultiplier, ksSpecularExpMultiplier);
  #endif
}

// MultiMap shaders

#else

void considerDetails(float2 uv, inout float4 txDiffuseValue, inout float3 txMapsValue){
  if (useDetail){
    float4 txDetailValue = txDetail.Sample(samLinear, uv * detailUVMultiplier);
    txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, 1 - txDiffuseValue.a);
    txMapsValue.x = lerp(txMapsValue.x, txMapsValue.x * txDetailValue.a, 1 - txDiffuseValue.a);
  }
}

#ifdef CARPAINT_NMDETAILS
void considerNmDetails(float2 uv, float3 tangentW, float3 bitangentW,
    inout float4 txDiffuseValue, inout float3 txMapsValue, inout float3 normalW){
  #ifdef NO_NORMALMAPS
    #ifdef CONSIDERNMDETAILS_LOAD_TX
      txDiffuseValue = txDiffuse.Sample(samLinear, uv);
      txMapsValue = txMaps.Sample(samLinear, uv).xyz;
    #endif

    if (useDetail){
      uv *= detailUVMultiplier;
      float4 txDetailValue = txDetail.Sample(samLinear, uv);

      #if !defined CARPAINT_SKINNED_NM
        txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, 1 - txDiffuseValue.a);
      #endif
    }
  #else
    if (useDetail){
      uv *= detailUVMultiplier;

      #ifdef USE_MULTIMAP_TEXTURE_VARIATION
        float3 txNormalDetailValue;
        float4 txDetailValue;
        
        [branch]
        if (ksAlphaRef == -193){
          txNormalDetailValue = textureSampleVariation(txNormalDetail, samLinearSimple, uv).xyz;
          txDetailValue = textureSampleVariation(txDetail, samLinear, uv);
        } else {
          txNormalDetailValue = txNormalDetail.Sample(samLinearSimple, uv).xyz;
          txDetailValue = txDetail.Sample(samLinear, uv);
        }
      #else
        float3 txNormalDetailValue = txNormalDetail.Sample(samLinearSimple, uv).xyz;
        float4 txDetailValue = txDetail.Sample(samLinear, uv);
      #endif

      float3 txNormalDetailAligned = txNormalDetailValue * 2 - (float3)1;
      float3x3 m = float3x3(cross(normalW, tangentW), normalW, cross(normalW, bitangentW));
      float3 normalDetailW = normalize(mul(transpose(m), txNormalDetailAligned.xzy));
      float transparency = 1 - txDiffuseValue.a;
      normalW = normalize(lerp(normalW, normalDetailW, detailNormalBlend * transparency));

      #if !defined CARPAINT_SKINNED_NM
        txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, transparency);
        txMapsValue.x = lerp(txMapsValue.x, txMapsValue.x * txDetailValue.a, transparency);
      #endif
    }
  #endif
}
#endif

#define NO_SUNSPEC_MAT (defined CARPAINT_NMDETAILS || defined CARPAINT_SIMPLE_REFL || defined CARPAINT_AT || defined CARPAINT_SKINNED)

#if !NO_SUNSPEC_MAT
float3 calculateMapsLighting_wSun(float3 toCamera, float3 normalW, 
    float3 txDiffuseValue, float3 txMapsValue, float3 txEmissiveValue, float shadow, float3 ambientAoMult){
  float r1w = txMapsValue.x * ksSpecular;
  float r3w = txMapsValue.y * ksSpecularEXP + 1;
  float r4x = (txMapsValue.z * txMapsValue.y) * sunSpecular;
  float r4y = txMapsValue.y * sunSpecularEXP + 1;
  float lightValue = saturate(dot(normalW, -ksLightDirection.xyz));
  float3 diffuse = ksLightColor.rgb * ksDiffuse * lightValue;
  float3 diffuseAmbient = diffuse * shadow + calculateAmbient(normalW, ambientAoMult, txEmissiveValue);

  float specularBase = saturate(dot(normalize(-toCamera - ksLightDirection.xyz), normalW));

  #ifdef SIMPLIFED_SPECULARS
    float sunSpecularResult = pow(specularBase, max(r4y, 1)) * shadow;
    float3 specularPart = sunSpecularResult * r4x * ksLightColor.rgb;
  #else
    float baseSpecularResult = pow(specularBase, max(r3w, 1));
    float sunSpecularResult = pow(specularBase, max(r4y, 1));
    float3 specularPart = (baseSpecularResult * r1w + sunSpecularResult * r4x) * shadow * ksLightColor.rgb;
  #endif

  return diffuseAmbient * txDiffuseValue + specularPart;
}
#endif

float3 calculateMapsLighting_woSun(float3 toCamera, float3 normalW, 
    float3 txDiffuseValue, float3 txMapsValue, float3 txEmissiveValue, float shadow, float3 ambientAoMult){
  float specularLevel = txMapsValue.x * ksSpecular;
  float specularExp = txMapsValue.y * ksSpecularEXP + 1;

  float lightValue = saturate(dot(normalW, -ksLightDirection.xyz));
  float3 diffuse = ksLightColor.rgb * ksDiffuse * lightValue;
  float3 diffuseAmbient = diffuse * shadow + calculateAmbient(normalW, ambientAoMult, txEmissiveValue);

  float specularBase = saturate(dot(normalize(-toCamera - ksLightDirection.xyz), normalW));
  float3 specularPart = pow(specularBase, max(specularExp, 1)) * shadow * specularLevel * ksLightColor.rgb;

  return diffuseAmbient * txDiffuseValue + specularPart;
}

float3 calculateMapsLighting(float3 toCamera, float3 normalW, 
    float3 txDiffuseValue, float3 txMapsValue, float3 txEmissiveValue, float shadow, float3 ambientAoMult){

  #if NO_SUNSPEC_MAT
    return calculateMapsLighting_woSun(toCamera, normalW, txDiffuseValue, txMapsValue, txEmissiveValue, shadow, ambientAoMult);
  #else
    if (sunSpecular > 0){
      return calculateMapsLighting_wSun(toCamera, normalW, txDiffuseValue, txMapsValue, txEmissiveValue, shadow, ambientAoMult);
    } else {
      return calculateMapsLighting_woSun(toCamera, normalW, txDiffuseValue, txMapsValue, txEmissiveValue, shadow, ambientAoMult);
    }
  #endif
}

#endif

float4 withFog(float3 baseResult, float fog, float alphaValue){
  return float4(lerp(baseResult.rgb, ksFogColor, saturate(fog)), alphaValue);
}

void withFog(out float4 output, float3 baseResult, float fog, float alphaValue){
  output.w = alphaValue;
  output.xyz = lerp(baseResult.rgb, ksFogColor, saturate(fog));
}

#ifdef ALLOW_EXTRA_SHADOW
  float2 getExtraShadow(float3 pos){
    float3 rel = pos - shadowPos;
    float dist = dot(rel, rel);

    // (dist - shadowR1) / (shadowR2 - shadowR1)
    // dist / (shadowR2 - shadowR1) - shadowR1 / (shadowR2 - shadowR1)

    // k = 1 / (shadowR2 - shadowR1)
    // dist * k - shadowR1 * k

    // R1 = 1 / (shadowR2 - shadowR1)
    // R2 = -R1 * shadowR1

    float result = saturate(dist * shadowR1 + shadowR2);
    #ifdef EXTRA_SHADOW_DARKER
      return float2(result, lerp(1, result, shadowOpacityDarker));
    #else
      return float2(result, lerp(1, result, shadowOpacity));
    #endif
  }

  #define APPLY_EXTRA_SHADOW \
      float2 extraShadow = getExtraShadow(pin.PosC);

  #define APPLY_EXTRA_SHADOW_INC_DIFFUSE \
      float2 extraShadow = getExtraShadow(pin.PosC);\
      shadow *= extraShadow.x;
#else
  float getExtraShadow(float3 pos){
    return 1;
  }

  #define APPLY_EXTRA_SHADOW float2 extraShadow = 1;
  #define APPLY_EXTRA_SHADOW_INC_DIFFUSE float2 extraShadow = 1;
#endif
