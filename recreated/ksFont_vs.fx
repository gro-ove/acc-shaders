#include "include_new/base/_include_vs.fx"

PS_IN_Font main(VS_IN vin) {
  PS_IN_Font vout;
  float4 posW, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.Color = vin.NormalL;
  vout.Tex = vin.Tex;
  vout.TangentH = (float3)0;
  return vout;
}
