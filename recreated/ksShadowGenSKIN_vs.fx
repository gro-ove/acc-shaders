#define SKINNED_WEIGHT_CHECK(a) (a)
#include "include_new/base/_include_vs.fx"

float4 main(VS_IN_Skinned vin) : SV_POSITION {
  float4 posW, posV;
  float3 normalW, tangentW;
  return toScreenSpaceSkinned(vin.PosL, vin.NormalL, vin.TangentL, vin.BoneWeights, vin.BoneIndices,
    posW, normalW, tangentW, posV);
}
