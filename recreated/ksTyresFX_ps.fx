#define INCLUDE_TYRE_CB
#define CARPAINT_NM
#define USE_BLURRED_NM
#define SPECULAR_DIFFUSE_FIX
#define AMBIENT_TYRES_FIX
#define FORCE_BLURREST_REFLECTIONS
#define EXTRA_SHADOW_DARKER
// #define LIGHTING_MODEL_COOKTORRANCE
#define CALCULATELIGHTING_USESPECULARMULT_AS_VALUE
#define LIGHTING_SPECULAR_POWER_MULT specMult
#define GETSHADOW_BIAS_MULTIPLIER 4
#include "include_new/base/_include_ps.fx"

#ifdef ALLOW_TYRESFX
  #include "include_new/ext_tyresfx/_include_ps.fx"
#endif

float4 main(PS_IN_Nm pin) : SV_TARGET {
  READ_VECTORS_NM

  #ifdef ALLOW_TYRESFX
    float shadow = getShadow(normalW, SHADOWS_COORDS, shadowBiasMult);
  #else
    float shadow = getShadow(normalW, SHADOWS_COORDS);
  #endif
  APPLY_EXTRA_SHADOW_INC_DIFFUSE

  #ifdef ALLOW_TYRESFX_DIRT
    float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
    float4 txBlurValue = txBlur.Sample(samLinear, pin.Tex);
    txDiffuseValue = lerp(txDiffuseValue, txBlurValue, blurLevel);

    float alpha;
    normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);

    float dirtyInv = 1;
    float specMult = dirtyInv * ksSpecular * txDiffuseValue.a;
    float specExpMult = 1.0;
    float lightingMult = 1.0;
    TYRESFX_DIRT(txDiffuseValue.xyz, lightingMult, specMult, specExpMult, dirtyInv);
  #else
    float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
    float4 txBlurValue = txBlur.Sample(samLinear, pin.Tex);
    float4 txDirtyValue = txDirty.Sample(samLinear, pin.Tex);
    txDiffuseValue = lerp(txDiffuseValue, txBlurValue, blurLevel);

    float dirtyLevelAdj = dirtyLevel * txDirtyValue.a;
    txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, txDirtyValue.rgb, dirtyLevelAdj);

    float alpha;
    normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);

    float dirtyInv = 1 - dirtyLevel;
    float specMult = dirtyInv * ksSpecular * txDiffuseValue.a;
    float specExpMult = 1.0;
    float lightingMult = 1.0;
    TYRESFX(lightingMult, specMult, specExpMult, dirtyInv);
  #endif

  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb * lightingMult, shadow, specMult, specExpMult, 
    lightingMult * extraShadow.y * VAO);
  LIGHTINGFX(lighting);

  float3 withReflection = calculateReflection(lighting, toCamera, normalW, (float3)1, 
    /* extraMultiplier= */ saturate(dirtyInv) * extraShadow.x, /* useBias= */ false, /* isCarPaint= */ false, /* useSkyColor= */ false);
  return withFog(withReflection, pin.Fog, 1);
}
