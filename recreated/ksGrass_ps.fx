#define NO_CARPAINT
#define NO_EXTAMBIENT
#define INCLUDE_GRASS_CB
#define NO_EXTAMBIENT
#define NO_EXTSPECULAR
#define LIGHTINGFX_GRASS
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_Grass pin) : SV_TARGET {
  READ_VECTORS
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);

  float2 uv = pin.GrassThing * scale;
  float3 var = txVariation.Sample(samLinear, uv).xyz - (float3)0.5;
  var *= gain;
  txDiffuseValue.rgb = txDiffuseValue.rgb * var + txDiffuseValue.rgb;

  float shadow = getShadow(normalW, SHADOWS_COORDS);
  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb, shadow, 1, 1, VAO);
  LIGHTINGFX(lighting);

  // return float4(normalW, 1);
  return withFog(lighting, pin.Fog, txDiffuseValue.a);
}
