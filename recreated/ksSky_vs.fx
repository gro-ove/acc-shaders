#include "include_new/base/_include_vs.fx"

PS_IN_Sky main(VS_IN vin) {
  PS_IN_Sky vout;
  float4 posW, posV;
  float4 PosC;
  PosC.xyz = vin.PosL.xyz + ksCameraPosition.xyz;
  PosC.w = vin.PosL.w;
  vout.PosH = toScreenSpace(PosC, posW, posV);
  vout.PosW = posW.xyz;
  vout.NormalL = vin.NormalL;
  vout.Tex = posW.xz;
  vout.LightH = mul(mul(ksLightDirection.xyz, (float3x3)ksView), (float3x3)ksProjection);
  return vout;
}
