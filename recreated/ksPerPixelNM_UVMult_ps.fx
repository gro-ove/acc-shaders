#define CARPAINT_NM_UVMULT
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_Nm pin) : SV_TARGET {
  READ_VECTORS_NM
  float shadow = getShadow(normalW, SHADOWS_COORDS);

  #ifndef NO_NORMALMAPS
    float2 uvValueNormal = pin.Tex * (normalMult + 1);
    float4 txNormalValue = txNormal.Sample(samLinearSimple, uvValueNormal);
    float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
    float3x3 normalMatrix = float3x3(tangentW, normalW, bitangentW);
    normalW = normalize(mul(transpose(normalMatrix), txNormalAligned.xzy));
  #endif

  float2 uvValueDiffuse = pin.Tex * (diffuseMult + 1);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, uvValueDiffuse);
  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb, shadow, 1, 1, VAO);
  LIGHTINGFX(lighting);

  float3 withReflection = calculateReflection(lighting, toCamera, normalW, (float3)1, 
    /* extraMultiplier= */ 1.0, /* useBias= */ true, /* isCarPaint= */ false, /* useSkyColor= */ false);
  return withFog(withReflection, pin.Fog, txDiffuseValue.a);
}
