#define USE_GETNORMALW
#define GETNORMALW_NORMALIZED_INPUT
#define GETNORMALW_XYZ_TX
#define CARPAINT_DAMAGES
#define CARPAINT_GLASS
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_Nm pin) : SV_TARGET {
  READ_VECTORS_NM
  float shadow = getShadow(normalW, SHADOWS_COORDS);

  float alpha;
  normalW = getDamageNormalW(pin.Tex, normalW, tangentW, bitangentW, glassDamage, alpha);
  clip(alpha - 0.02);

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb, shadow, 1, 1, VAO);
  LIGHTINGFX(lighting.rgb);

  float3 withReflection = calculateReflection(lighting, toCamera, normalW, (float3)1, 
    /* extraMultiplier= */ 1.0, /* useBias= */ true, /* isCarPaint= */ false, /* useSkyColor= */ false);
  return withFog(withReflection.rgb, pin.Fog, alpha);
}
