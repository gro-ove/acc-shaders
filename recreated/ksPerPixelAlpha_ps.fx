#define CARPAINT_SIMPLE
#define CALCULATELIGHTING_SPECIAL_ALPHA
#define INCLUDE_ALPHA_CB
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_PerPixel pin) : SV_TARGET {
  READ_VECTORS
  float shadow = getShadow(normalW, SHADOWS_COORDS);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb, shadow, 1, 1, VAO);
  LIGHTINGFX(lighting);
  return withFog(lighting, pin.Fog, txDiffuseValue.a * alpha);
}
