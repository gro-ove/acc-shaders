// based on https://www.shadertoy.com/view/Ml2cWG by robobo1221

#include "include_new/base/_include_ps.fx"
// #define OPTIMIZE_SKY

cbuffer cbExtSky : register(b6) {
  float zenithOffset;
  float multiScatterPhase;
  float density;
  float sunMieExp;

  float3 skyColor; // skyColor * (1.0 + anisotropicIntensity)
  float brightnessMult;

  float2 sunSize;
  float sunBrightness;
  float inputYOffset;

  float3 sunColor;
  float __1;
};

static const float pi = 3.14159265359;

// static const float zenithOffset = 0.1;
// static const float multiScatterPhase = 0.1;
// static const float density = 0.7;
// static const float anisotropicIntensity = 0.0; //Higher numbers result in more anisotropic scattering
// static const float3 skyColor = float3(0.39, 0.57, 1.0) * (1.0 + anisotropicIntensity); //Make sure one of the conponents is never 0.0

#define getZenithDensity(x) density * pow(max(x - zenithOffset, 0.35e-2), -0.75)

float3 getSkyAbsorption(float3 x, float y){  
  float3 absorption = x * -y;
  absorption = exp2(absorption) * 2.0;  
  return absorption;
}

// static const float2 sunSize = 1.0 - float2(0.03, 0.026) / 35;

float3 getRayleigMultiplier(float dotV){
  return 1.0 + dotV * dotV * (pi * 0.5) * sunColor;
}

float getSunPoint(float dotV){
  return smoothstep(sunSize.x, sunSize.y, dotV) * sunBrightness;
}

float getMie(float dotV){
  return pow(dotV, sunMieExp);
}

float getHeight(float3 v){
  return saturate(v.y + inputYOffset);
}

float3 getAtmosphericScattering(float3 dir, float3 lightPos){
  // float2 correctedLp = lp / max(iResolution.x, iResolution.y) * iResolution.xy;
    
  float lightHeightFix = getHeight(lightPos) + multiScatterPhase;
  float sunPointDistMult = saturate(lightHeightFix - zenithOffset);
  
  float dotV = saturate(dot(dir, lightPos));
  float3 rayleighMult = getRayleigMultiplier(dotV);
  
  float dirHeight = getHeight(dir);
  float zenithDensity = getZenithDensity(dirHeight);
  float3 absorption = getSkyAbsorption(skyColor, zenithDensity);
  float3 sunAbsorption = getSkyAbsorption(skyColor, getZenithDensity(lightHeightFix));
  float3 sky = skyColor * zenithDensity * rayleighMult;

  float3 sun = getSunPoint(dotV) * absorption;
  float3 mie = getMie(dotV) * sunAbsorption;
  
  float3 totalSky = lerp(sky * absorption, sky / (sky + 0.5), sunPointDistMult);
  totalSky += (sun + mie) * saturate((dirHeight - zenithOffset) * 100.0) * sunColor;
  totalSky *= sunAbsorption + length(sunAbsorption);
  
  return totalSky * brightnessMult;
}

#define PI 3.141592
Texture2D txSky : register(t9);

float2 fisheyeToUVCoord(float3 v) {
  float phiCos = pow(abs(v.y), 1/1.3) * sign(v.y);
  float k = acos(phiCos) / (0.6 * PI * sqrt(1.0 - v.y * v.y));
  return float2(v.x, v.z) * k * 0.5 + 0.5;
}

SamplerState samSky : register(s9) {
  Filter = LINEAR;
  AddressU = CLAMP;
  AddressV = CLAMP;
};

float4 main(PS_IN_Sky pin) : SV_TARGET {
  float2 uv = fisheyeToUVCoord(normalize(pin.PosW.xyz - ksCameraPosition.xyz));
  return float4(getAtmosphericScattering(normalize(pin.PosW.xyz - ksCameraPosition.xyz), -ksLightDirection.xyz), 1);
}
