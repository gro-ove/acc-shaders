#define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"

PS_IN_PerPixel main(VS_IN vin) {
  PS_IN_PerPixel vout;
  float4 posW, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.NormalW = normalize(normals(vin.NormalL));
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);
  PREPARE_AO(vout.Ao);
  return vout;
}
