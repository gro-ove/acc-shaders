// #define FOG_NMUV2_VERSION
#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"

PS_IN_MultilayerFresnel main(VS_IN vin)
{
  GENERIC_PIECE_NM(PS_IN_MultilayerFresnel);
  vout.PosW = posW.xyz;
  return vout;
}
