#define USE_BRAKE_DISC_MODE
#define USE_BLURRED_NM
#define INCLUDE_TYRE_CB
#define CARPAINT_NM
#define CALCULATELIGHTING_USESPECULARMULT_AS_VALUE
#define LIGHTING_SPECULAR_POWER ksSpecularAdj

#include "include_new/base/_include_ps.fx"

#ifdef ALLOW_BRAKEDISCFX
  #include "include_new/ext_brakediscfx/_include_ps.fx"

  float posWithinCircle(float3 delta){
    return length(delta - wheelNormal * dot(delta, wheelNormal));
  }

  float luminanceBrakeDiscFX(float3 rgb){
    return dot(rgb, float3(0.2125, 0.7154, 0.0721));
  }
#endif

float3 getHeatColor(float wavelength){
  float3 result = 0;
  if (wavelength <= 580) {
    result.r = 1.0;
    result.g = 1.0;
    result.b = saturate((580 - wavelength) / (580 - 510));
  } else if (wavelength <= 645) {
    result.r = 1.0;
    result.g = saturate((645 - wavelength) / (645 - 580));
    result.b = 0.0;
  } else {
    result.r = saturate((750 - wavelength) / (750 - 645));
    result.g = 0.0;
    result.b = 0.0;
  }
  return result * result;
}

float4 main(PS_IN_Nm pin) : SV_TARGET {
  READ_VECTORS_NM

  float shadow = getShadow(normalW, SHADOWS_COORDS);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txBlurValue = txBlur.Sample(samLinear, pin.Tex);
  float3 txGlowValue = txGlow.Sample(samLinear, pin.Tex).rgb;
  txDiffuseValue = lerp(txDiffuseValue, txBlurValue, blurLevel);

  float alpha = 1;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);

  #ifdef ALLOW_BRAKEDISCFX
    extraShadow.y *= ambientMult;
  #endif

  float ksSpecularAdj = ksSpecular * txDiffuseValue.a;
  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.xyz, shadow, ksSpecularAdj, 1, extraShadow.y * VAO);

  #ifdef ALLOW_BRAKEDISCFX
    lighting += getHeatColor(lerp(750, 550, luminanceBrakeDiscFX(txGlowValue) * glowLevel / 15.0)) * 50.0 * txDiffuseValue.rgb;
  #else
    lighting += txGlowValue * glowLevel * txDiffuseValue.rgb;
  #endif

  LIGHTINGFX(lighting);

  float reflectionMult;
  #ifdef ALLOW_BRAKEDISCFX
    float centerFix = saturate((posWithinCircle(pin.PosC - wheelPos) - discInternalRadius) * discInternalRadiusSharpness);
    float result = saturate(dot(normalW, wheelNormal) * 200 - 40);
    normalW = lerp(normalW, wheelNormal, simplifyNormalsK * centerFix);

    float3 deltaPos = pin.PosC - wheelPos;
    float3 reflDir = normalize(reflect(toCamera, normalW));
    float reflDirN = saturate(dot(reflDir, wheelNormal));
    reflDir *= rimHeight / reflDirN;

    [unroll]
    for (int i = 1; i <= 9; i++){
      float reflectedPos = posWithinCircle(deltaPos + reflDir * ((float)i / 9));
      result = min(result, reflectedPos - rimInternalRadius);
      if (i == 9){
        result = min(result, rimRadius - reflectedPos);
      }
    }

    reflectionMult = lerp(0.5, reflectionPowerMult, centerFix) * saturate(result * 150.0);
  #else
    reflectionMult = 1;
  #endif

  float4 withReflection = calculateReflection(float4(lighting, alpha), toCamera, normalW, (float3)1, 
    /* extraMultiplier= */ reflectionMult, /* useBias= */ true, /* isCarPaint= */ false, /* useSkyColor= */ false);
  return withFog(withReflection.rgb, pin.Fog, withReflection.a);
}
