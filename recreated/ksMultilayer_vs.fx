// #define FOG_NMUV2_VERSION
#define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"

PS_IN_Multilayer main(VS_IN vin)
{
  GENERIC_PIECE(PS_IN_Multilayer);
  vout.PosW = posW.xyz;
  return vout;
}
