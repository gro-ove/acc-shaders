#define NO_CARPAINT
#define NO_SHADOWS
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_Tree pin) : SV_TARGET {
  READ_VECTORS
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb, 1, 1, 1, VAO);
  LIGHTINGFX(lighting);
  return withFog(lighting, pin.Fog, txDiffuse.SampleLevel(samPoint, pin.Tex, 0).a);
}
