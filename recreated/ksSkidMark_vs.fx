#define FOG_NMUV2_VERSION
// #define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"

PS_IN_SkidMark main(VS_IN vin) {
  GENERIC_PIECE(PS_IN_SkidMark);
  vout.SkidMarkThing = vin.TangentL.x;
  return vout;
}
