#define USE_SHADOW_BIAS_MULT
#define CARPAINT_DAMAGES
#define LIGHTING_SPECULAR_POWER_MULT txMapsValue.x
#define LIGHTING_SPECULAR_EXP (txMapsValue.y * ksSpecularEXP + 1)
// #define LIGHTING_SPECULAR_EXP_MULT txMapsValue.y
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_Nm pin) : SV_TARGET {
  READ_VECTORS_NM

  float shadow = getShadow(normalW, SHADOWS_COORDS);
  APPLY_EXTRA_SHADOW

  // damage-related
  float4 txDamageMaskValue = txDamageMask.Sample(samLinear, pin.Tex);
  float4 txDamageValue = txDamage.Sample(samLinear, pin.Tex);
  float normalMultiplier = dot(txDamageMaskValue, damageZones);
  float damageInPoint = saturate(normalMultiplier * txDamageValue.a);

  // normals piece
  float normalAlpha;
  normalW = getDamageNormalW(pin.Tex, normalW, tangentW, bitangentW, normalMultiplier, normalAlpha);

  // usual stuff
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 txMapsValue = txMaps.Sample(samLinear, pin.Tex).xyz;
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue);

  // new damaged txDiffuseValue
  txDiffuseValue.xyz = damageInPoint > 0 
      ? lerp(txDiffuseValue.xyz, txDamageValue.xyz, damageInPoint) 
      : txDiffuseValue.xyz;

  // dirt
  float4 txDustValue = txDust.Sample(samLinear, pin.Tex);
  float isDusty = dirt > 0;
  float dirtLevel = txDustValue.a * dirt;
  txDiffuseValue.xyz = isDusty
      ? lerp(txDiffuseValue.xyz, txDustValue.xyz, dirtLevel) 
      : txDiffuseValue.xyz;

  // both damages and dirt affecting txMaps
  float damageInverted = 1 - damageInPoint;
  float mapsMultiplier = saturate(1 - dirtLevel * 10);
  float damageFactor = damageInPoint * (normalAlpha - 1) + 1;
  mapsMultiplier *= damageInverted;
  mapsMultiplier *= damageFactor;
  txMapsValue.x *= mapsMultiplier;

  // lighting
  float3 lighting = calculateMapsLighting(toCamera, normalW, txDiffuseValue.rgb, txMapsValue, (float3)1, shadow, extraShadow.y * VAO);
  LIGHTINGFX(lighting);

  float3 withReflection = calculateReflection(lighting, toCamera, normalW, txMapsValue, 
    /* extraMultiplier= */ isAdditive ? mapsMultiplier * extraShadow.x : extraShadow.x, 
    /* useBias= */ true, /* isCarPaint= */ true, /* useSkyColor= */ false);
  return withFog(withReflection, pin.Fog, 1);
}
