// I think, it’s safe to assume there is absolutely no need for AO for this shader
#define SET_AO_TO_ONE

#include "include_new/base/_include_vs.fx"

PS_IN_Nm main(VS_IN vin) {
  PS_IN_Nm vout;
  float4 posW, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);
  #ifndef NO_NORMALMAPS
    vout.TangentW = normals(vin.TangentL);
    vout.BitangentW = bitangent(vin.NormalL, vin.TangentL);
  #endif
  PREPARE_AO(vout.Ao);
  return vout;
}
