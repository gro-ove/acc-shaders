#define USE_BRAKE_DISC_MODE
#define USE_BLURRED_NM
#define INCLUDE_TYRE_CB
#define CARPAINT_NM
#define CALCULATELIGHTING_USESPECULARMULT_AS_VALUE
#define LIGHTING_SPECULAR_POWER ksSpecularAdj

#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_Nm pin) : SV_TARGET {
  READ_VECTORS_NM

  float shadow = getShadow(normalW, SHADOWS_COORDS);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txBlurValue = txBlur.Sample(samLinear, pin.Tex);
  float3 txGlowValue = txGlow.Sample(samLinear, pin.Tex).rgb;
  txDiffuseValue = lerp(txDiffuseValue, txBlurValue, blurLevel);

  float alpha = 1;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);

  float ksSpecularAdj = ksSpecular * txDiffuseValue.a;
  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.xyz, shadow, ksSpecularAdj, 1, extraShadow.y * VAO);
  lighting += txGlowValue * glowLevel * txDiffuseValue.rgb;
  LIGHTINGFX(lighting);

  float4 withReflection = calculateReflection(float4(lighting, alpha), toCamera, normalW, (float3)1, 
    /* extraMultiplier= */ 1, /* useBias= */ true, /* isCarPaint= */ false, /* useSkyColor= */ false);
  return withFog(withReflection.rgb, pin.Fog, withReflection.a);
}
