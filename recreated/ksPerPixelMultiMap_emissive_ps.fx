#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define LIGHTING_SPECULAR_POWER_MULT txMapsValue.x
// #define LIGHTING_SPECULAR_EXP_MULT txMapsValue.y
#define LIGHTING_SPECULAR_EXP (txMapsValue.y * ksSpecularEXP + 1)

#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

Texture2D txEmissive : register(t21);

float4 main(PS_IN_Nm pin) : SV_TARGET {
  READ_VECTORS_NM
  
  float shadow = getShadow(normalW, SHADOWS_COORDS);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  float4 txEmissiveValue = txEmissive.Sample(samLinear,pin.Tex);

  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);

  float3 lighting = calculateMapsLighting(toCamera, normalW, txDiffuseValue.rgb, txMapsValue.xyz, txEmissiveValue.xyz, shadow, VAO);
  LIGHTINGFX(lighting);

  float3 withReflection = calculateReflection(lighting, toCamera, normalW, txMapsValue.xyz, 
    /* extraMultiplier= */ 1.0, /* useBias= */ true, /* isCarPaint= */ true, /* useSkyColor= */ false);
  return withFog(withReflection, pin.Fog, txMapsValue.a);
}
