#define CARPAINT_NM
#define GETNORMALW_OS_SAMPLER samLinear
#define GETNORMALW_SAMPLER samLinear
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_Nm pin) : SV_TARGET {
  READ_VECTORS_NM
  
  float shadow = getShadow(normalW, SHADOWS_COORDS);
  APPLY_EXTRA_SHADOW

  float alpha;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb, shadow, 1, 1, extraShadow.y * VAO);
  LIGHTINGFX(lighting);

  float4 withReflection = calculateReflection(float4(lighting, alpha), toCamera, normalW, (float3)1, 
    /* extraMultiplier= */ extraShadow.x, /* useBias= */ true, /* isCarPaint= */ false, /* useSkyColor= */ false);
  return withFog(withReflection.rgb, pin.Fog, withReflection.a);
}
