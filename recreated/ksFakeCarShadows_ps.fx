#include "include_new/base/_flags.fx"
#include "include_new/base/cbuffers_common.fx"

struct PS_IN_FakeCarShadows {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
};

Texture2D txDiffuse : register(t0);
Texture2D txNoise : register(t1);

SamplerState samPoint : register(s2) {
  Filter = POINT;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

float4 withFog(float3 baseResult, float fog, float alphaValue){
  return float4(lerp(baseResult.rgb, ksFogColor, saturate(fog)), alphaValue);
}

cbuffer cbExtFakeShadows : register(b6) {
  float transparency;
  float blurBase;
  float2 blurExtra;
};

SamplerState samLinearSimpleCustom : register(s6) {
  Filter = LINEAR;
  AddressU = Border;
  AddressV = Border;
  BorderColor = (float4)0;
};

float4 main(PS_IN_FakeCarShadows pin) : SV_TARGET {
  #ifdef ADVANCED_FAKE_SHADOWS
    #define POISSON_DISK_SIZE 12
    const float2 poissonDisk[POISSON_DISK_SIZE] = {
      float2(-0.7886961f, -0.2233229f),
      float2(0.1798526f, -0.4192532f),
      float2(-0.3731767f, -0.8694668f),
      float2(-0.9565483f, 0.249105f),
      float2(-0.1117985f, 0.4849544f),
      float2(0.8123304f, 0.2958729f),
      float2(-0.2990344f, 0.02080005f),
      float2(0.2467226f, 0.9040384f),
      float2(-0.6412935f, 0.6410192f),
      float2(0.2098728f, -0.9328976f),
      float2(0.7062484f, -0.644536f),
      float2(0.3566671f, 0.051722f)
    };

    float2 random = normalize(txNoise.SampleLevel(samPoint, pin.Tex * 1000.0, 0).xy);

    float value = 0;
    float blurLevel = blurBase + dot(blurExtra * pin.Tex, 1);
    float2 blurOffset = pow(saturate(blurLevel), 0.75) * float2(0.28, 0.1);
    for (float i = 0; i < POISSON_DISK_SIZE; i++) {
      float2 uvOffset = reflect(poissonDisk[i], random);
      value += txDiffuse.Sample(samLinearSimpleCustom, pin.Tex * (1 + blurOffset * 2) + (uvOffset - 1) * blurOffset).x;
    }

    float result = value * (1.0 - transparency) * (1 - blurLevel) * (1.2 / POISSON_DISK_SIZE);
    // return float4(0, 0, 0, saturate(fog * result));
    return withFog(0, pin.Fog, result);
  #else
    float value = txDiffuse.Sample(samLinearSimple, pin.Tex).x;
    // return float4(0, 0, 0, saturate(fog * value));
    return withFog(0, pin.Fog, value * 1.2);
  #endif
}
