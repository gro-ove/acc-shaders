// Not needed (yet?)
#define SET_AO_TO_ONE
#include "include_new/base/_include_vs.fx"
// alias: ksSkinnedMesh_NMDetaill_vs

PS_IN_Nm main(VS_IN_Skinned vin) {
  PS_IN_Nm vout;
  float4 posW, posV;
  float3 normalW, tangentW;
  vout.PosH = toScreenSpaceSkinned(vin.PosL, vin.NormalL, vin.TangentL, vin.BoneWeights, vin.BoneIndices,
    posW, normalW, tangentW, posV);

  normalW = normalize(normalW);
  tangentW = normalize(tangentW);

  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  shadows(posW, SHADOWS_COORDS);
  vout.Fog = calculateFog(posV);

  vout.NormalW = normalW;
  vout.Tex = vin.Tex;
  #ifndef NO_NORMALMAPS
    vout.TangentW = tangentW;
    vout.BitangentW = cross(normalW, tangentW);
  #endif
  PREPARE_AO(vout.Ao);
  return vout;
}
