#define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"

#ifdef BLUR_SHADOWS
  #include "include_new/ext_windscreenfx/_include_vs.fx"
#endif

PS_IN_PerPixel main(VS_IN vin) {
  PS_IN_PerPixel vout;
  float4 posW, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.NormalW = normalize(normals(vin.NormalL));
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);

  #ifdef BLUR_SHADOWS
    shadowsBlur(posW, SHADOWS_COORDS);
  #else
    shadows(posW, SHADOWS_COORDS);
  #endif

  PREPARE_AO(vout.Ao);
  return vout;
}
