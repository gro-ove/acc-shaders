#include "include_new/base/_include_ps.fx"
//#define OPTIMIZE_SKY

float4 main(PS_IN_Sky pin) : SV_TARGET {
  float3 dir;
  float v;
  dir = normalize(pin.PosW.xyz - ksCameraPosition.xyz);
  
  float4 color = getSkyColor(dir);
  color.rgb *= 1.5;

#ifdef OPTIMIZE_SKY
  color.rgb = lerp(color.rgb, ksAmbientColor.r * float3(0.75, 0.75, 0.75), color.a);
#else
  // why ???
  v = (ksFogLinear * ksFogLinear) / 900;
  v = saturate(ksFogLinear / v);
  color.rgb = lerp(color.rgb, ksAmbientColor.r * float3(0.75, 0.75, 0.75), v);
#endif

  return float4(color.rgb, 1);
}
