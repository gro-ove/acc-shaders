// #define FOG_NMUV2_VERSION
#define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"
// alias: ksPerPixel_dual_layer_vs
// alias: ksPerPixel_nosdw_vs
// alias: ksPerPixel_vs
// alias: ksPerPixelAT_vs
// alias: ksPerPixelReflection_vs
// alias: ksPerPixelSimpleRefl_vs

PS_IN_PerPixel main(VS_IN vin) {
  GENERIC_PIECE(PS_IN_PerPixel);
  return vout;
}
