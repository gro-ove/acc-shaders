#include "include_new/base/_include_vs.fx"

PS_IN_Sky main(VS_IN vin) {
  PS_IN_Sky vout;
  float4 posW, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.PosW = posW.xyz;
  vout.NormalL = vin.NormalL;
  vout.Tex = vin.Tex.xy;
  vout.LightH = mul(mul(ksLightDirection.xyz, (float3x3)ksView), (float3x3)ksProjection);
  return vout;
}
