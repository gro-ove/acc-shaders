// #define FOG_NMUV2_VERSION
#define SUPPORTS_COLORFUL_AO
#define INCLUDE_FLAGS_CB
#include "include_new/base/_include_vs.fx"

float3 kunosWave(float2 uv) {
  float2 offset = sin(uv.xy * ksGameTime * frequency / 1000);
  return saturate(uv.x) * distortion * float3(offset.x, offset.xy);
}

float3 racerWave(float2 uv) {
  float3 off;
  float sinOff = uv.x + uv.y;
  float t = -ksGameTime * frequency / 2500;
  float sin1 = sin(t * 1.45 + sinOff);
  float sin2 = sin(t * 3.12 + sinOff);
  float sin3 = sin(t * 2.2 + sinOff);
  float fx = uv.x;
  float fy = uv.x * uv.y;
  off.x = sin1 * fx * 2;
  off.y = sin2 * fx * 2 - fy * 0.9;
  off.z = sin3 * fx * 2;
  return distortion * off;
}

// To implement custom wave, first thing first, there should be a way to rotate flags according to wind direction.
// The way Kunos do wave is that they use X UV coordinate to figure out how far is vertex from the pole. But, there
// is no way to figure out flag width, to cancel out existing direction and reapply a new one. And, my naive attempt 
// to find one from vertices failed: one mesh might contain many flags, with various sizes.

// Possible solutions for the future:
// • Just create a new shader with its own implementation and split flags in existing tracks;
// • Store flag width in normal length, detect one by trying to combine triangles into pieces?

static const float windDirection = 0;

void customWave(float2 uv, inout float3 posW, inout float3 normal, float flagWidth) {
  float3 fwd = cross(normal, float3(0, -1, 0));
  fwd.y = 0;
  fwd = normalize(fwd);

  if (flagWidth < 0){
    fwd = -fwd;
    flagWidth = -flagWidth;
  }

  float horPos = 0.07 + uv.x * flagWidth;
  float3 offset = -fwd * horPos;
  float2 newDir = float2(sin(ksGameTime * 0.003), cos(ksGameTime * 0.003));
  float3 newFwd = float3(newDir.x, 0, newDir.y);

  normal = normalize(cross(newFwd, float3(0, 1, 0)));
  if (flagWidth < 0){
    normal = -normal;
  }

  offset += newFwd * horPos;
  posW += offset;
  posW.y -= flagWidth;
}

PS_IN_PerPixel main(VS_IN vin) {
  PS_IN_PerPixel vout;

  vout.NormalW = normalize(normals(vin.NormalL));

  float4 posW = mul(vin.PosL, ksWorld);
  posW.xyz += kunosWave(vin.Tex);
  // posW.xyz += racerWave(vin.Tex);
  // customWave(vin.Tex, posW.xyz, vout.NormalW, length(vin.NormalL) - 1000.0f);

  float4 posV = mul(posW, ksView);
  vout.PosH = mul(posV, ksProjection);

  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);
  PREPARE_AO(vout.Ao);
  return vout;
}
