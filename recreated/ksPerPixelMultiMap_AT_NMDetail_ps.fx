#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_NORMALIZED_TB
#define CARPAINT_AT_NMDETAILS
#define CARPAINT_NMDETAILS
#define USE_GETNORMALW
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define LIGHTING_SPECULAR_POWER_MULT txMapsValue.x
#define LIGHTING_SPECULAR_EXP_MULT txMapsValue.y
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_Nm pin) : SV_TARGET {
  READ_VECTORS_NM
  
  float shadow = getShadow(normalW, SHADOWS_COORDS);
  APPLY_EXTRA_SHADOW

  float3 txMapsValue = txMaps.Sample(samLinear, pin.Tex).xyz;
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);

  float alpha;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);
  considerNmDetails(pin.Tex, tangentW, bitangentW, txDiffuseValue, txMapsValue, normalW);

  float3 lighting = calculateMapsLighting(toCamera, normalW, txDiffuseValue.rgb, txMapsValue, (float3)1, shadow, extraShadow.y * VAO);
  LIGHTINGFX(lighting);

  float3 withReflection = calculateReflection(lighting, toCamera, normalW, txMapsValue, 
    /* extraMultiplier= */ extraShadow.x, /* useBias= */ true, /* isCarPaint= */ true, /* useSkyColor= */ false);
  return withFog(withReflection, pin.Fog, alpha);
}
