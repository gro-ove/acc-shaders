#define INCLUDE_PARTICLE_CB
#define EXCLUDE_PEROBJECT_CB
// #define FOG_PARTICLE_VERSION
// #define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"

PS_IN_Particle main(VS_IN_Particle vin) {
  PS_IN_Particle vout;

  // vout.NormalW = vin.NormalL;

  float4 posW = vin.PosL;

  float4 posV = mul(posW, ksView);
  vout.PosH = mul(posV, ksProjection);

  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);

  [branch]
  if (shaded){
    vout.PosC = (posW - ksCameraPosition).xyz;
    shadows(posW, SHADOWS_COORDS);
    vout.NormalW = float3(0, 1, 0);
  } else {
    #ifndef NO_SHADOWS
      vout.ShadowTex0 = 0;
      vout.ShadowTex1 = 0;
      vout.ShadowTex2 = 0;
    #endif
    vout.NormalW = 0;
    vout.PosC = 0;
  }

  PREPARE_AO(vout.Ao);
  vout.Color = vin.Color;
  vout.Extra = -posV.z;
  return vout;

  // GENERIC_PIECE(PS_IN_Particle);
  // return vout;
}
