#define CARPAINT_SIMPLE
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_PerPixel pin) : SV_TARGET {
  READ_VECTORS
  
  float shadow = getShadow(normalW, SHADOWS_COORDS);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb, shadow, 1, 1, extraShadow.y * VAO);
  LIGHTINGFX(lighting);

  float4 withReflection = calculateReflection(float4(lighting, txDiffuseValue.a), toCamera, normalW, (float3)1, 
    /* extraMultiplier= */ extraShadow.x, /* useBias= */ true, /* isCarPaint= */ false, /* useSkyColor= */ false);
  return withFog(withReflection.rgb, pin.Fog, withReflection.a);
}
