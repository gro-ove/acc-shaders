#define NO_CARPAINT
#define USE_GETNORMALW
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_Nm pin) : SV_TARGET {
  READ_VECTORS_NM
  float shadow = getShadow(normalW, SHADOWS_COORDS);
  
  float alpha;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb, shadow, 1, 1, VAO);
  LIGHTINGFX(lighting);
  return withFog(lighting, pin.Fog, txDiffuseValue.a);
}
