#define NO_CARPAINT
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_PerPixel pin) : SV_TARGET {
  READ_VECTORS
  float shadow = getShadow(normalW, SHADOWS_COORDS);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);

  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb, shadow, 1, 1, VAO);
  LIGHTINGFX(lighting);

  #ifdef ALLOW_EXTRA_FEATURES
    if (ksAlphaRef == -193){
      lighting += ksEmissive * saturate(dot(normalize(-toCamera), normalize(normalW)) * 4 - 3) * 3;
    }
  #endif
  
  return withFog(lighting, pin.Fog, txDiffuseValue.a);
}
