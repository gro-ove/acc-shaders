#define CARPAINT_SKINNED
#define GETNORMALW_NORMALIZED_INPUT
#define GETNORMALW_NORMALIZED_TB
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
// #define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_Nm pin) : SV_TARGET {
  READ_VECTORS_NM

  float shadow = getShadow(normalW, SHADOWS_COORDS);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 txMapsValue = txMaps.Sample(samLinear, pin.Tex).xyz;

  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue);

  float3 lighting = calculateMapsLighting_woSun(toCamera, normalW, txDiffuseValue.xyz, txMapsValue, (float3)1, shadow, VAO);
  LIGHTINGFX(lighting);

  float4 withReflection = calculateReflection(float4(lighting, txDiffuseValue.a), toCamera, normalW, txMapsValue, 
    /* extraMultiplier= */ 1.0, /* useBias= */ true, /* isCarPaint= */ true, /* useSkyColor= */ false);
  return withFog(withReflection.rgb, pin.Fog, withReflection.a);
}
