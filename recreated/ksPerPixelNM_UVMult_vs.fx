// #define FOG_NMUV2_VERSION
#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"
// alias: ksPerPixelMultiMapSimpleRefl_vs

PS_IN_Nm main(VS_IN vin) {
  GENERIC_PIECE_NM(PS_IN_Nm);
  return vout;
}
