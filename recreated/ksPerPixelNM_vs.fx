// #define FOG_NMUV2_VERSION
#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"
// alias: ksBrokenGlass_vs
// alias: ksPerPixelAT_NM_vs
// alias: ksPerPixelMultiMap_AT_NMDetail_vs
// alias: ksPerPixelMultiMap_AT_vs
// alias: ksPerPixelMultiMap_damage_dir2_vs
// alias: ksPerPixelMultiMap_damage_dirt_vs
// alias: ksPerPixelMultiMap_damage_vs
// alias: ksPerPixelMultiMap_NMDetail_vs
// alias: ksPerPixelMultiMap_vs
// alias: ksPerPixelMultiMap_emissive_vs

PS_IN_Nm main(VS_IN vin) {
  PS_IN_Nm vout;
  float4 posW, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);
  #ifndef NO_NORMALMAPS
    vout.TangentW = normals(vin.TangentL);
    vout.BitangentW = bitangent(vin.NormalL, vin.TangentL);
  #endif
  PREPARE_AO(vout.Ao);
  return vout;
}
