#define CALCULATELIGHTING_USESPECULARMULT_AS_VALUE
#define NO_CARPAINT
#define INCLUDE_MULTILAYER3_CB
#define LIGHTING_SPECULAR_POWER intensity
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_MultilayerFresnel pin) : SV_TARGET {
  READ_VECTORS_NM

  #ifndef NO_NORMALMAPS
    float2 uv = pin.Tex * detailNMMult;
    float4 txNormalValue = txDetailNM.Sample(samLinearSimple, uv);
    float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
    float3x3 m = float3x3(tangentW, normalW, bitangentW);
    normalW = normalize(mul(transpose(m), txNormalAligned.xzy));
  #endif

  float shadow = getShadow(normalW, SHADOWS_COORDS);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMaskValue = txMask.Sample(samLinear, pin.Tex);

  #ifdef USE_MULTIMAP_TEXTURE_VARIATION
    float4 txDetailRValue;
    float4 txDetailGValue;
    float4 txDetailBValue;
    float4 txDetailAValue;

    [branch]
    if (ksAlphaRef == -193){
      txDetailRValue = textureSampleVariation(txDetailR, samLinear, pin.PosW.xz * multR);
      txDetailGValue = textureSampleVariation(txDetailG, samLinear, pin.PosW.xz * multG);
      txDetailBValue = textureSampleVariation(txDetailB, samLinear, pin.PosW.xz * multB);
      txDetailAValue = textureSampleVariation(txDetailA, samLinear, pin.PosW.xz * multA);
    } else {
      txDetailRValue = txDetailR.Sample(samLinear, pin.PosW.xz * multR);
      txDetailGValue = txDetailG.Sample(samLinear, pin.PosW.xz * multG);
      txDetailBValue = txDetailB.Sample(samLinear, pin.PosW.xz * multB);
      txDetailAValue = txDetailA.Sample(samLinear, pin.PosW.xz * multA);
    }
  #else
    float4 txDetailRValue = txDetailR.Sample(samLinear, pin.PosW.xz * multR);
    float4 txDetailGValue = txDetailG.Sample(samLinear, pin.PosW.xz * multG);
    float4 txDetailBValue = txDetailB.Sample(samLinear, pin.PosW.xz * multB);
    float4 txDetailAValue = txDetailA.Sample(samLinear, pin.PosW.xz * multA);
  #endif

  float4 combined = 
      txDetailRValue * txMaskValue.x
    + txDetailGValue * txMaskValue.y
    + txDetailBValue * txMaskValue.z
    + txDetailAValue * txMaskValue.w;
  txDiffuseValue *= combined;
  txDiffuseValue *= magicMult;

  float coefficient = saturate(1 - dot(normalW, -toCamera));
  float intensity = coefficient > 0 
    ? min(
      tarmacSpecularMultiplier * pow(
        coefficient, 
        fresnelEXP) 
        + fresnelC, 
      fresnelMaxLevel) 
    : 0;
  intensity = txDiffuseValue.a * intensity;

  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb, shadow, intensity, 1, VAO);
  LIGHTINGFX(lighting);
  return withFog(lighting, pin.Fog, 1);
}
