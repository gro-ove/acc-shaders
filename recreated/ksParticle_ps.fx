#define NO_CARPAINT
#define INCLUDE_PARTICLE_CB
#define NO_EXTAMBIENT
// #define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

Texture2D txDepth : register(t21);

float4 main(PS_IN_Particle pin) : SV_TARGET {
  // return float4(txDepth.SampleLevel(samLinear, pin.PosH.xy / float2(1920, 1080), 0).xyw, 1);

  #ifdef OFF_NOT_WORKING____ALLOW_LIGHTINGFX
    float3 normalW = pin.NormalW;
    float shadow = getShadow(normalW, SHADOWS_COORDS);

    float3 toCamera = normalize(pin.PosC);
    float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex * pin.Color.a) * pin.Color;
    float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb, shadow);
    return withFog(pin.PosC * 0.1 + 0.001 * lighting, pin.Fog, 1.0);
  #else
    float4 lighting;

    if (shaded) {
      float3 normalW = pin.NormalW;
      float shadow = getShadow(normalW, SHADOWS_COORDS);

      float3 toCamera = normalize(pin.PosC);
      float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
      lighting = float4(calculateLighting(toCamera, normalW, txDiffuseValue.rgb, shadow, 1, 1, VAO), txDiffuseValue.a);
    } else {
      lighting = txDiffuse.Sample(samLinearSimple, pin.Tex); // * float4(calculateAmbient(float3(0, 1, 0)) * 0.5, 1);
    }

    float3 r0 = ksLightColor.rgb * pin.Tex.y + ksAmbientColor.rgb;
    r0 *= lighting.rgb;
    r0 *= pin.Color.rgb;

    r0 = lerp(r0, lighting.rgb * pin.Color.rgb, emissiveBlend);

    float r0w = pin.Extra - minDistance;
    r0w = saturate(r0w / minDistance);
    r0w = lighting.a * r0w;

    // r0.r = 1.0;
    return withFog(r0, pin.Fog, r0w * pin.Color.a);
  #endif
}
