#define CALCULATELIGHTING_USESPECULARMULT_AS_VALUE
#define CARPAINT_SIMPLE
#define INCLUDE_MULTILAYER2_CB
#define LIGHTING_SPECULAR_POWER_MULT txDiffuseValue.a
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_Multilayer pin) : SV_TARGET {  
  READ_VECTORS
  float shadow = getShadow(normalW, SHADOWS_COORDS);

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMaskValue = txMask.Sample(samLinear, pin.Tex);
  float4 txDetailRValue = txDetailR.Sample(samLinear, pin.Tex * multR);
  float4 txDetailGValue = txDetailG.Sample(samLinear, pin.Tex * multG);
  float4 txDetailBValue = txDetailB.Sample(samLinear, pin.Tex * multB);
  float4 txDetailAValue = txDetailA.Sample(samLinear, pin.Tex * multA);

  float4 combined = 
      txDetailRValue * txMaskValue.x
    + txDetailGValue * txMaskValue.y
    + txDetailBValue * txMaskValue.z
    + txDetailAValue * txMaskValue.w;
  txDiffuseValue *= combined;
  txDiffuseValue *= magicMult;

  float3 lighting = calculateLighting(toCamera, normalW, txDiffuseValue.rgb, shadow, 
    ksSpecular * txDiffuseValue.a, 1, VAO);
  LIGHTINGFX(lighting);
  return withFog(lighting, pin.Fog, 1);
}
