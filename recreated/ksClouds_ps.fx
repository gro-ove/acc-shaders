#define NO_CARPAINT
#define NO_SHADOWS
#include "include_new/base/_include_ps.fx"

// #define OPTIMIZE_SKY

float4 main(PS_IN_Sky pin) : SV_TARGET {
#ifdef OPTIMIZE_SKY
  float fog = saturate(900 / ksFogLinear);
#else
  // why ???
  float fog = (ksFogLinear * ksFogLinear) / 900;
  fog = saturate(ksFogLinear / fog);
#endif

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float v = dot(pin.NormalL.xyz, ksLightDirection.xyz) * (1 - txDiffuseValue.r) * ksCloudColor;
  float3 col = 0.5 * (ksLightColor.xyz + ksAmbientColor.rgb) * (1 - ksCloudCutoff);
  col = lerp(v, ksAmbientColor.r * 0.75, fog) * ksCloudCutoff + col;
  return float4(col, saturate(ksCloudCover * txDiffuseValue.a));
}
