#define NO_CARPAINT
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_PerPixel pin) : SV_TARGET {
  float3 normalW = normalize(pin.NormalW);
  // float shadow = 1.0;
  float shadow = getShadow(normalW, SHADOWS_COORDS);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float lightValue = saturate(dot(normalize(pin.PosC), -ksLightDirection.xyz) * shadow);
  float3 diffuse = ksLightColor.rgb * ksDiffuse * lightValue;
  float3 resultLight = calculateAmbient(normalW, (float3)1, (float3)1) + diffuse;
  float resultAlpha = txDiffuseValue.a < 0.5 ? txDiffuseValue.a * lightValue : txDiffuseValue.a;
  float3 resultColor = txDiffuseValue.rgb * resultLight;
  return float4(saturate(pin.Fog) * (ksFogColor - resultColor) + resultColor, 
      resultAlpha);
}
