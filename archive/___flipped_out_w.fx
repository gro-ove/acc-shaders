float4 getReflection_c(float4 baseResultColor, float3 toCamera, float3 normalW, float3 txMapsValue, 
    float extraMultiplier, float pinFog, bool isMetallic, bool isCarPaint = false){
  float fresnelMaxLevelAdj = fresnelMaxLevel * extraMultiplier;
  float blurness = saturate(1 - txMapsValue.y * ksSpecularEXP / (isCarPaint ? 8 : 255)) * 6;
  float3 reflected = normalize(reflect(toCamera, normalW));

  float coefficient = saturate(1 - dot(normalW, -toCamera));
  float intensity = coefficient > 0 
    ? min(
      pow(
        coefficient, 
        isMetallic && !isCarPaint 
          ? max(fresnelEXP, 1) 
          : fresnelEXP) 
        + fresnelC * extraMultiplier, 
      fresnelMaxLevelAdj) 
    : 0;

  reflected.x *= -1;

  float4 color;
  if (blurness <= 3){
    color = txCube.SampleBias(samLinear, reflected, blurness);
  } else {      
    color = txCube.SampleLevel(samLinear, reflected, blurness);
  }

  if (isMetallic){    
    float4 r2 = color * txMapsValue.z - baseResultColor;
    r2 *= intensity;
    r2.a = 1.0;
    r2 += baseResultColor;
    return withFog(r2.rgb, pinFog, r2.a);
  } else {
    float4 r2 = color * intensity * txMapsValue.z;
    r2.a = 1.0;
    r2 += baseResultColor;
    return withFog(r2.rgb, pinFog, r2.a);
  }
}