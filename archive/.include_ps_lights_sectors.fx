// Params

// Slightly different from the one used by Kunos’ algorithm, saves 18 instructions
// #define OPTIMIZED_SHADOWS

// Small changes here and there to improve shaders and resolve some warnings
// #define MICROOPTIMIZATIONS

// Debug mode
// #define DEBUG_EXTRAS

// Inputs

SamplerState samLinear : register(s0) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerState samPoint : register(s2) {
  Filter = POINT;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerComparisonState samShadow : register(s1) {
  Filter = COMPARISON_MIN_MAG_MIP_LINEAR;
  AddressU = BORDER;
  AddressV = BORDER;
  AddressW = BORDER;
  BorderColor = 1;
  ComparisonFunc = LESS;
};

SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txDiffuse : register(t0);
Texture2D txVariation : register(t1);
Texture2D txNormal : register(t1);
Texture2D txDirty : register(t2);
Texture2D txGlow : register(t2);
Texture2D txBlur : register(t3);
Texture2D txNormalBlur : register(t4);
Texture2D txMask : register(t1);
Texture2D txDetailR : register(t2);
Texture2D txDetailG : register(t3);
Texture2D txDetailB : register(t4);
Texture2D txDetailA : register(t5);
Texture2D txMaps : register(t2);
Texture2D txDetail : register(t3);
Texture2D txDetailNM : register(t21);
Texture2D txNormalDetail : register(t4);
Texture2D txDamage : register(t4);
Texture2D txDust : register(t5);
Texture2D<float> txShadow0 : register(t6);
Texture2D<float> txShadow1 : register(t7);
Texture2D<float> txShadow2 : register(t8);
TextureCube txCube : register(t10);
Texture2D txDamageMask : register(t21);

cbuffer cbPerObject : register(b1) {
  float4x4 ksWorld;                  // Offset:    0 Size:    64
}

cbuffer cbLighting : register(b2) {
  float4 ksLightDirection;           // Offset:    0 Size:    16
  float4 ksAmbientColor;             // Offset:   16 Size:    16
  float4 ksLightColor;               // Offset:   32 Size:    16
  float4 ksHorizonColor;             // Offset:   48 Size:    16 [unused]
  float4 ksZenithColor;              // Offset:   64 Size:    16 [unused]
  float ksExposure;                  // Offset:   80 Size:     4 [unused]
  float ksScreenWidth;               // Offset:   84 Size:     4 [unused]
  float ksScreenHeight;              // Offset:   88 Size:     4 [unused]
  float ksFogLinear;                 // Offset:   92 Size:     4 [unused]
  float ksFogBlend;                  // Offset:   96 Size:     4 [unused]
  float3 ksFogColor;                 // Offset:  100 Size:    12
  float ksCloudCover;                // Offset:  112 Size:     4 [unused]
  float ksCloudCutoff;               // Offset:  116 Size:     4 [unused]
  float ksCloudColor;                // Offset:  120 Size:     4 [unused]
  float ksCloudOffset;               // Offset:  124 Size:     4 [unused]
  float ksMinExposure;               // Offset:  128 Size:     4 [unused]
  float ksMaxExposure;               // Offset:  132 Size:     4 [unused]
  float ksDofFocus;                  // Offset:  136 Size:     4 [unused]
  float ksDofRange;                  // Offset:  140 Size:     4 [unused]
  float ksSaturation;                // Offset:  144 Size:     4 [unused]
  float ksGameTime;                  // Offset:  148 Size:     4 [unused]
  float2 boh44;                      // Offset:  152 Size:     8 [unused]
}

cbuffer cbMaterial : register(b4) {
  float ksAmbient;                   // Offset:    0 Size:     4
  float ksDiffuse;                   // Offset:    4 Size:     4
  float ksSpecular;                  // Offset:    8 Size:     4
  float ksSpecularEXP;               // Offset:   12 Size:     4
  float3 ksEmissive;                 // Offset:   16 Size:    12
  float ksAlphaRef;                  // Offset:   28 Size:     4 [unused]
}

#ifndef NO_SHADOWS
cbuffer cbShadowMaps : register(b3) {
  float4x4 ksShadowMatrix0;          // Offset:    0 Size:    64 [unused]
  float4x4 ksShadowMatrix1;          // Offset:   64 Size:    64 [unused]
  float4x4 ksShadowMatrix2;          // Offset:  128 Size:    64 [unused]
  float3 bias;                       // Offset:  192 Size:    12
  float textureSize;                 // Offset:  204 Size:     4
}
#endif

#ifdef INCLUDE_MULTILAYER_CB
cbuffer cbMultiLayer : register(b5) {
  float multR;                       // Offset:    0 Size:     4
  float multG;                       // Offset:    4 Size:     4
  float multB;                       // Offset:    8 Size:     4
  float2 multA;                      // Offset:   16 Size:     8
  float magicMult;                   // Offset:   24 Size:     4
  float2 boh;                        // Offset:   32 Size:     8 [unused]
}
#endif

#ifdef INCLUDE_MULTILAYER2_CB
cbuffer cbMultiLayer : register(b5) {
  float2 multR;                      // Offset:    0 Size:     8
  float2 multG;                      // Offset:    8 Size:     8
  float2 multB;                      // Offset:   16 Size:     8
  float2 multA;                      // Offset:   24 Size:     8
  float magicMult;                   // Offset:   32 Size:     4
}
#endif

#ifdef INCLUDE_MULTILAYER3_CB
cbuffer cbMultiLayer : register(b5) {
  float multR;                       // Offset:    0 Size:     4
  float multG;                       // Offset:    4 Size:     4
  float multB;                       // Offset:    8 Size:     4
  float2 multA;                      // Offset:   16 Size:     8
  float magicMult;                   // Offset:   24 Size:     4
  float fresnelC;                    // Offset:   28 Size:     4
  float fresnelEXP;                  // Offset:   32 Size:     4
  float fresnelMaxLevel;             // Offset:   36 Size:     4
  float tarmacSpecularMultiplier;    // Offset:   40 Size:     4
  float2 detailNMMult;               // Offset:   48 Size:     8
}
#endif

#ifdef INCLUDE_GRASS_CB
cbuffer GrassBuffer : register(b10) {  
  float2 scale;                      // Offset:    0 Size:     8
  float gain;                        // Offset:    8 Size:     4
  float boh;                         // Offset:   12 Size:     4 [unused]
}
#endif

#ifdef INCLUDE_ALPHA_CB
cbuffer cbAlphaLevel : register(b12) {  
  float alpha;                       // Offset:    0 Size:     4
}
#endif

#ifdef CARPAINT_NM_UVMULT
cbuffer cbTextureMult : register(b11) {  
  float diffuseMult;                 // Offset:    0 Size:     4
  float normalMult;                  // Offset:    4 Size:     4
  float2 bo;                         // Offset:    8 Size:     8 [unused]
}
#endif

#ifdef INCLUDE_TYRE_CB

cbuffer cbTyre : register(b10) {
  float blurLevel;                   // Offset:    0 Size:     4
  #ifdef USE_BRAKE_DISC_MODE
  float glowLevel;                  // Offset:    4 Size:     4
  #else
  float dirtyLevel;                  // Offset:    4 Size:     4
  #endif
  float fresnelC;                    // Offset:    8 Size:     4
  float fresnelEXP;                  // Offset:   12 Size:     4
  float isAdditive;                  // Offset:   16 Size:     4
  float fresnelMaxLevel;             // Offset:   20 Size:     4
}

static bool nmObjectSpace = false;

#elif !defined NO_CARPAINT
cbuffer cbCarPaint : register(b5) {
#ifdef CARPAINT_SIMPLE
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float isAdditive;                  // Offset:    8 Size:     4
  float fresnelMaxLevel;             // Offset:   12 Size:     4
#elif defined CARPAINT_NM
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float nmObjectSpace;               // Offset:    8 Size:     4
  float isAdditive;                  // Offset:   12 Size:     4
  float fresnelMaxLevel;             // Offset:   16 Size:     4
  float3 boh;                        // Offset:   20 Size:    12 [unused]
#elif defined CARPAINT_NM_UVMULT
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float boh;                         // Offset:   12 Size:     4 [unused]
#elif defined CARPAINT_SKINNED
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float nmObjectSpace;               // Offset:   12 Size:     4
  float isAdditive;                  // Offset:   16 Size:     4 [unused]
  float useDetail;                   // Offset:   20 Size:     4
  float detailUVMultiplier;          // Offset:   24 Size:     4
  float boh;                         // Offset:   28 Size:     4 [unused]
#elif defined CARPAINT_SIMPLE_REFL
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float nmObjectSpace;               // Offset:   12 Size:     4
  float isAdditive;                  // Offset:   16 Size:     4 [unused]
  float useDetail;                   // Offset:   20 Size:     4
  float detailUVMultiplier;          // Offset:   24 Size:     4
  float shadowBiasMult;              // Offset:   28 Size:     4
#elif defined CARPAINT_NMDETAILS
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float isAdditive;                  // Offset:   12 Size:     4
  float useDetail;                   // Offset:   16 Size:     4
  float detailUVMultiplier;          // Offset:   20 Size:     4
  float shadowBiasMult;              // Offset:   24 Size:     4
  float detailNormalBlend;           // Offset:   28 Size:     4
#elif defined CARPAINT_AT
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float nmObjectSpace;               // Offset:   12 Size:     4
  float isAdditive;                  // Offset:   16 Size:     4
  float useDetail;                   // Offset:   20 Size:     4
  float detailUVMultiplier;          // Offset:   24 Size:     4
  float shadowBiasMult;              // Offset:   28 Size:     4
  float normalUVMultiplier;          // Offset:   32 Size:     4
  float3 boh;                        // Offset:   36 Size:    12 [unused]
#else
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float nmObjectSpace;               // Offset:   12 Size:     4
  float isAdditive;                  // Offset:   16 Size:     4
  float useDetail;                   // Offset:   20 Size:     4
  float detailUVMultiplier;          // Offset:   24 Size:     4
  float shadowBiasMult;              // Offset:   28 Size:     4
  float4 damageZones;                // Offset:   32 Size:    16 [unused]
  float dirt;                        // Offset:   48 Size:     4 [unused]
  float sunSpecular;                 // Offset:   52 Size:     4
  float sunSpecularEXP;              // Offset:   56 Size:     4
#endif
}
#endif

// Custom stuff

#ifdef DEBUG_EXTRAS

struct Light {
  float3 PosW;
  float Range;

  float3 DirectionW;
  float SpotlightCosMin;

  float3 Color;
  float SpotlightCosMax;

  // 36 bytes here

  float3 UpW;
  uint Type;

  float3 SpotEdgeOffset; // float3(0.079, 0.077, 0.075)
  float SpotEdgeSharpness; // 30.0

  // +12 = 48 bytes
};

#define REGIONS_SIZE 25
#define REGIONS_COUNT 40
#define MAX_LIGHTS_AMOUNT 100
#define MAX_LIGHTS_PER_REGION 24

struct VisibleIndex {
  int Indices[MAX_LIGHTS_PER_REGION];
  // 4*12 = 48 bytes
};

#define LIGHT_OFF 0
#define LIGHT_POINT 1
#define LIGHT_SPOT 2

StructuredBuffer<VisibleIndex> gIndices : register(t12);

/*StructuredBuffer<Light> gLights : register(t12);
StructuredBuffer<VisibleIndex> gIndices : register(t21);*/

cbuffer cbExtLighting : register(b8) {
  Light gLights[MAX_LIGHTS_AMOUNT];
}

float getDiffuseMultiplier(float3 normal, float3 lightDir) {
  return saturate(dot(normal, lightDir) + 0.2);
  // return saturate(dot(normal, lightDir) + 0.16);
}

float getAttenuation(float range, float d) {
  return 1.0f - smoothstep(range * 0.12f, range, d);
}

float GetNDotH(float3 normal, float3 position, float3 lightDir) {
  float3 toEye = normalize(-position);
  float3 halfway = normalize(toEye + lightDir);
  return saturate(dot(halfway, normal));
}

float CalculateSpecularLight(float nDotH, float exp, float level) {
  return pow(nDotH, max(exp, 0.1)) * level;
}

float CalculateSpecularLight_ByValues(float3 normal, float3 position, float3 lightDir, float exp, float level) {
  float nDotH = GetNDotH(normal, position, lightDir);
  return CalculateSpecularLight(nDotH, exp, level);
}

void getLight_ByType(Light light, float3 normal, float3 position, const float specularExp, const float specularValue, const bool sunSpeculars,
    inout float3 diffuse, inout float3 specular) {
  float3 toLight = light.PosW.xyz - position;
  float distance = length(toLight);
  toLight /= distance;
  float3 attenuation = (float3)getAttenuation(light.Range, distance);

  if (light.Type == LIGHT_SPOT) {
    float cosAngle = dot(light.DirectionW.xyz, -toLight);
    float spotCone = smoothstep(light.SpotlightCosMin, light.SpotlightCosMax, cosAngle);

    float up = dot(light.UpW, -toLight);
    attenuation *= spotCone * saturate((light.SpotEdgeOffset - up) * light.SpotEdgeSharpness);
  }

  // diffuse += light.Color.xyz * (0.1 + attenuation * getDiffuseMultiplier(normal, toLight) * ksDiffuse);
  diffuse += light.Color.xyz * attenuation * getDiffuseMultiplier(normal, toLight) * ksDiffuse;
}

float3 getExtraLights(int region[MAX_LIGHTS_PER_REGION], float3 normalW, float3 posW){
  float3 diffuse = 0.0;

  [loop]
  for (int i = 0; i < MAX_LIGHTS_PER_REGION && region[i] != 0; i++) {
    float3 specular = 0.0;
    getLight_ByType(gLights[region[i]], normalW, posW, 0, 0, false, diffuse, specular);
  }

  return diffuse;
}

float3 getExtraLights(float2 posS, float3 normalW, float3 posW, float3 txDiffuseValue){
  int x = floor(posW.x / REGIONS_SIZE) + (REGIONS_COUNT / 2);
  int y = floor(posW.z / REGIONS_SIZE) + (REGIONS_COUNT / 2);
  return 0;
  if (x < 0 || x >= REGIONS_COUNT || y < 0 || y >= REGIONS_COUNT) return 0;

  return txDiffuseValue.rgb * getExtraLights(
    gIndices[y * REGIONS_COUNT + x].Indices, 
    normalW, posW);
}

// #define DEBUG_EXTRAS_HERE(x) x.rgb = ((float3)1 - x.rgb) + extraBindedValue.rgb * 3;
// #define DEBUG_EXTRAS_HERE(x) x.rgb = (x.rgb - (float3)1000) + extraBindedValue.rgb * 3;
// #define DEBUG_EXTRAS_HERE(x) x.rgb = x.bgr + extraBindedValue.rgb * 3;
// #define DEBUG_EXTRAS_HERE(x) x.rgb += extraBindedValue.rgb * 10;
#define DEBUG_EXTRAS_HERE(x) x.rgb += getExtraLights(pin.PosH, normalW, pin.PosC, txDiffuseValue.rgb);

#else

#define DEBUG_EXTRAS_HERE(x) ;

#endif

// Input structures

struct PS_IN_GL {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD;
  float3 TangentH : TANGENT;
};

struct PS_IN_PerPixel {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  float Fog : TEXCOORD6;
};

struct PS_IN_Multilayer {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  float3 MultilayerThing : TEXCOORD6;
  float Fog : TEXCOORD7;
};

struct PS_IN_MultilayerFresnel {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  float3 MultilayerThing : TEXCOORD6;
  float Fog : TEXCOORD7;
  float3 MultilayerThing1 : TEXCOORD8;
  float3 MultilayerThing2 : TEXCOORD9;
};

struct PS_IN_SkidMark {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  centroid float SkidMarkThing : TEXCOORD6;
  float Fog : TEXCOORD7;
};

struct PS_IN_Tree {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float Fog : TEXCOORD6;
};

struct PS_IN_Grass {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  float Fog : TEXCOORD6;
  float2 GrassThing : TEXCOORD7;
};

struct PS_IN_AtNs {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float Fog : TEXCOORD6;
};

struct PS_IN_Nm {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  float3 TangentW : TEXCOORD6;
  float3 BitangentW : TEXCOORD7;
  float Fog : TEXCOORD8;
};

struct PS_IN_Uv2 {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  float3 TangentW : TEXCOORD6;
  float3 BitangentW : TEXCOORD7;
  float2 Tex2 : TEXCOORD8;
  float Fog : TEXCOORD9;
};

// Shadows

#ifndef NO_SHADOWS

bool checkCascade(float4 uv){
  return -1 < uv.x && uv.x < 1 && -1 < uv.y && uv.y < 1;
}

float cascadeShadowStep(float deltaZ, float4 uv, Texture2D<float> tx){
  float result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  [unroll]
  for (int x = -1; x <= 1; x++){
    [unroll]
    for (int y = -1; y <= 1; y++){
      #ifdef OPTIMIZED_SHADOWS
      result += tx.SampleCmpLevelZero(samShadow, uv.xy + textureSize * float2(x, y), comparisonValue);
      #else
      result += tx.SampleCmpLevelZero(samShadow, uv.xy + float2(y * textureSize, x * textureSize), comparisonValue);
      #endif
    }
  }

  return result;
}

float getShadow(float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2){
  #ifdef USE_SHADOW_BIAS_MULT
    float biasMultiplier = shadowBiasMult + 1;
    float deltaZBase = max(1 - abs(dot(normalW, ksLightDirection.xyz)), 0.1) * biasMultiplier;
  #elif defined GETSHADOW_BIAS_MULTIPLIER
    float deltaZBase = max(1 - abs(dot(normalW, ksLightDirection.xyz)), 0.1) * GETSHADOW_BIAS_MULTIPLIER;
  #else
    float deltaZBase = max(1 - abs(dot(normalW, ksLightDirection.xyz)), 0.1);
  #endif

  if (checkCascade(shadowTex0)){
    return cascadeShadowStep(deltaZBase * bias.x, shadowTex0, txShadow0) / 9;
  }

  if (checkCascade(shadowTex1)){
    return cascadeShadowStep(deltaZBase * bias.y, shadowTex1, txShadow1) / 9;
  }

  if (checkCascade(shadowTex2)){
    return min(cascadeShadowStep(deltaZBase * bias.z, shadowTex2, txShadow2) / 9 
        + max(-(shadowTex2.y + 0.5), 0) * 2, 1);
  }

  return 1;
}

#endif

// Normals

#if !defined NO_CARPAINT || defined USE_GETNORMALW

#ifdef USE_GETNORMALW
#define nmObjectSpace false
#endif

#ifndef GETNORMALW_SAMPLER
#define GETNORMALW_SAMPLER samLinear
#endif

#ifdef CARPAINT_DAMAGES

float3 getDamageNormalW(float2 uv, float3 normalW, float3 targentW, float3 bitangentW, float damage, 
    out float normalAlpha){
  #ifndef GETNORMALW_NORMALIZED_INPUT
    normalW = normalize(normalW);
  #endif

  float4 txNormalValue = txNormal.Sample(samLinearSimple, uv);
  normalAlpha = txNormalValue.a;

  [flatten]
  if (nmObjectSpace){
    float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
    txNormalAligned.z *= -1;
    float3x3 m = (float3x3)ksWorld;
    return normalize(mul(txNormalAligned, m));
  } else {
    float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
    float3x3 m = float3x3(targentW, normalW, bitangentW);
    return lerp(normalW, normalize(mul(transpose(m), txNormalAligned)), damage);
  }
}

#endif

#if defined CARPAINT_NM || defined USE_GETNORMALW || defined CARPAINT_AT

float3 getNormalW(float2 uv, float3 normalW, float3 targentW, float3 bitangentW, out float alpha){
#ifdef USE_BLURRED_NM
  alpha = 1;

  float3 T = normalize(targentW);
  float3 B = normalize(bitangentW);
  float4 txNormalValue = txNormal.Sample(samLinearSimple, uv);
  float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
  float4 txNormalBlurValue = txNormalBlur.Sample(samLinearSimple, uv);
  float3 txNormalBlurAligned = txNormalBlurValue.xyz * 2 - (float3)1;
  float3x3 m = float3x3(T, normalW, B);
  float3 normalStatic = normalize(mul(transpose(m), txNormalAligned.xzy));
  float3 normalBlur = normalize(mul(transpose(m), txNormalBlurAligned.xzy));
  return lerp(normalStatic, normalBlur, blurLevel);
#else 
  if (nmObjectSpace){
    #ifdef GETNORMALW_UV_MULT
      float4 txNormalValue = txNormal.Sample(samLinearSimple, uv * GETNORMALW_UV_MULT);
    #else
      float4 txNormalValue = txNormal.Sample(samLinearSimple, uv);
    #endif
    alpha = txNormalValue.a;
    float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
    txNormalAligned.z *= -1;
    float3x3 m = (float3x3)ksWorld;
    return normalize(mul(txNormalAligned, m));
  } else {
    float3 T = normalize(targentW);
    float3 B = normalize(bitangentW);
    float4 txNormalValue = txNormal.Sample(GETNORMALW_SAMPLER, uv);
    alpha = txNormalValue.a;
    float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
    float3x3 m = float3x3(T, normalW, B);
    return normalize(mul(transpose(m), txNormalAligned.xzy));
  }
#endif
}

#elif defined CARPAINT_NM_UVMULT
#else

float3 getNormalW(float2 uv, float3 normalW, float3 targentW, float3 bitangentW, bool objectSpace){
  #ifndef GETNORMALW_NORMALIZED_INPUT
    normalW = normalize(normalW);
  #endif

  if (objectSpace){
    #ifdef GETNORMALW_XYZ_TX
      float4 txNormalValue = txNormal.Sample(GETNORMALW_SAMPLER, uv);
      float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
      txNormalAligned.z *= -1;
    #else
      float4 txNormalValue = txNormal.Sample(GETNORMALW_SAMPLER, uv);
      float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
      txNormalAligned.z *= -1;
    #endif
    float3x3 m = (float3x3)ksWorld;
    return normalize(mul(txNormalAligned, m));
  } else {
    #ifdef GETNORMALW_NORMALIZED_TB
      float3 T = targentW;
      float3 B = bitangentW;
    #else
      float3 T = normalize(targentW);
      float3 B = normalize(bitangentW);
    #endif

    #ifdef GETNORMALW_XYZ_TX
      float3 txNormalValue = txNormal.Sample(samLinearSimple, uv).xyz;
    #else
      float3 txNormalValue = txNormal.Sample(samLinearSimple, uv).xzy;
    #endif

    float3 txNormalAligned = txNormalValue * 2 - (float3)1;
    float3x3 m = float3x3(T, normalW, B);
    return normalize(mul(transpose(m), txNormalAligned.xzy));
  }
}

float3 getNormalW(float2 uv, float3 normalW, float3 targentW, float3 bitangentW){
  #if defined CARPAINT_NMDETAILS || defined CARPAINT_SIMPLE
    return getNormalW(uv, normalW, targentW, bitangentW, false);
  #else
    return getNormalW(uv, normalW, targentW, bitangentW, nmObjectSpace);
  #endif
}

#endif

#endif

// Lighting

float3 calculateDiffuse(float3 normalW){
  float lightValue = saturate(dot(normalW, -ksLightDirection.xyz));
  return ksLightColor.rgb * ksDiffuse * lightValue;
}

float3 calculateAmbient(float3 normalW){
  #ifdef MICROOPTIMIZATIONS
    float ambientMultiplier = saturate(normalW.y * 0.25 + 0.75);
  #else
    float upMultiplier = normalW.y * 0.5 + 0.5;
    float ambientMultiplier = saturate(upMultiplier * 0.5 + 0.5);
  #endif
  return ksAmbientColor.rgb * ksAmbient * ambientMultiplier + ksEmissive;
}

float3 calculateDiffuseAmbient(float3 normalW, float shadow){
  return calculateDiffuse(normalW) * shadow + calculateAmbient(normalW);
}

float3 calculateSpecular(float3 normalW, float3 toCameraW, float shadow){
  float specularBase = saturate(dot(normalize(toCameraW - ksLightDirection.xyz), normalW));
  return pow(specularBase, max(ksSpecularEXP, 1)) * ksSpecular * shadow * ksLightColor.rgb;
}

// Basic shaders

#if defined NO_CARPAINT || defined CARPAINT_SIMPLE || defined CARPAINT_NM || defined CARPAINT_NM_UVMULT

float3 calculateLighting_base(float3 normalW, float shadow){
  float dotValue = saturate(dot(normalW, -ksLightDirection.xyz));

  #ifdef CALCULATELIGHTING_SPECIAL_ALPHA
    float3 light = ksLightColor.rgb * dotValue;
    float3 base = (light * ksDiffuse + calculateAmbient(normalW));
  #else
    float3 light = ksLightColor.rgb * ksDiffuse * dotValue;
    float3 base = (light * shadow + calculateAmbient(normalW));
  #endif

  return base;
}

float3 calculateLighting_base(float3 normalW, float3 txDiffuseValue, float shadow){
  return calculateLighting_base(normalW, shadow) * txDiffuseValue.rgb;
}

float3 calculateLighting_spec(float3 toCamera, float3 normalW, float shadow,
    float ksSpecularMultiplier = 1.0){
  #ifdef CALCULATELIGHTING_USESPECULARMULT_AS_VALUE
    float ksSpecularAdj = ksSpecularMultiplier;
  #else
    float ksSpecularAdj = ksSpecular * ksSpecularMultiplier;
  #endif

  float specularBase = saturate(dot(normalize(-toCamera - ksLightDirection.xyz), normalW));

  #ifndef CALCULATELIGHTING_SPECIAL_MULTILAYER
    float3 specular = pow(specularBase, max(ksSpecularEXP, 1)) 
      #ifdef CALCULATELIGHTING_SPECIAL_TYRES
        * shadow * ksSpecularAdj 
      #else
        * ksSpecularAdj * shadow 
      #endif
      * ksLightColor.rgb;
  #else
    float3 specular = pow(specularBase, max(ksSpecularEXP, 1)) * shadow * ksSpecularAdj * ksLightColor.rgb;
  #endif

  return specular;
}

float3 calculateLighting(float3 toCamera, float3 normalW, float3 txDiffuseValue, float shadow,
    float ksSpecularMultiplier = 1.0){
  return calculateLighting_base(normalW, txDiffuseValue, shadow) 
    + calculateLighting_spec(toCamera, normalW, shadow, ksSpecularMultiplier);
}

// MultiMap shaders

#else

void considerDetails(float2 uv, inout float4 txDiffuseValue, inout float3 txMapsValue){
  if (useDetail){
    float4 txDetailValue = txDetail.Sample(samLinear, uv * detailUVMultiplier);
    txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, 1 - txDiffuseValue.a);
    txMapsValue.x = lerp(txMapsValue.x, txMapsValue.x * txDetailValue.a, 1 - txDiffuseValue.a);
  }
}

#ifdef CARPAINT_NMDETAILS
void considerNmDetails(float2 uv, float3 targentW, float3 bitangentW,
    inout float4 txDiffuseValue, inout float3 txMapsValue, inout float3 normalW){
  if (useDetail){
    uv *= detailUVMultiplier;
    float3 txNormalDetailValue = txNormalDetail.Sample(samLinearSimple, uv).xyz;
    float3 txNormalDetailAligned = txNormalDetailValue * 2 - (float3)1;

    #ifdef CONSIDERNMDETAILS_NORMALIZED_TB
    float3 T = targentW;
    float3 B = bitangentW;
    #else
    float3 T = normalize(targentW);
    float3 B = normalize(bitangentW);
    #endif

    // float3 T = normalize(t - dot(t, N)*N);
    B = cross(normalW, T);
    T = cross(normalW, B);

    float3x3 m = float3x3(B, normalW, T);
    float3 normalW2 = normalize(mul(transpose(m), txNormalDetailAligned.xzy));

    float vv = 1 - txDiffuseValue.a;
    float blend = vv * detailNormalBlend;
    normalW = lerp(normalW, normalW2, blend);


    float4 txDetailValue = txDetail.Sample(samLinear, uv);
    txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, vv);
    txMapsValue.x = lerp(txMapsValue.x, txMapsValue.x * txDetailValue.a, 1 - txDiffuseValue.a);
  }
}
#endif

#define NO_SUNSPEC_MAT (defined CARPAINT_NMDETAILS || defined CARPAINT_SIMPLE_REFL || defined CARPAINT_AT || defined CARPAINT_SKINNED)

#if !NO_SUNSPEC_MAT
float3 calculateMapsLighting_wSun(float3 toCamera, float3 normalW, 
    float3 txDiffuseValue, float3 txMapsValue, float shadow){
  float r1w = txMapsValue.x * ksSpecular;
  float r3w = txMapsValue.y * ksSpecularEXP + 1;
  float r4x = (txMapsValue.z * txMapsValue.y) * sunSpecular;
  float r4y = txMapsValue.y * sunSpecularEXP + 1;
  float r4z = saturate(dot(normalW, -ksLightDirection.xyz));
  float3 diffuse = ksLightColor.rgb * ksDiffuse * r4z;
  float3 diffuseAmbient = (diffuse * shadow + calculateAmbient(normalW));

  float specularBase = saturate(dot(normalize(-toCamera - ksLightDirection.xyz), normalW));

  float baseSpecularResult = pow(specularBase, max(r3w, 1)) * shadow;
  float sunSpecularResult = pow(specularBase, max(r4y, 1)) * shadow;
  float3 specularPart = (baseSpecularResult * r1w + sunSpecularResult * r4x) * ksLightColor.rgb;

  return diffuseAmbient * txDiffuseValue + specularPart;
}
#endif

float3 calculateMapsLighting_woSun(float3 toCamera, float3 normalW, 
    float3 txDiffuseValue, float3 txMapsValue, float shadow){
  float r1w = txMapsValue.x * ksSpecular;
  float r3w = txMapsValue.y * ksSpecularEXP + 1;

  float lightValue = saturate(dot(normalW, -ksLightDirection.xyz));

  float3 diffuse = ksLightColor.rgb * ksDiffuse * lightValue;
  float3 diffuseAmbient = (diffuse * shadow + calculateAmbient(normalW)) * txDiffuseValue;

  float specularBase = saturate(dot(normalize(-toCamera - ksLightDirection.xyz), normalW));
  float3 specularPart = pow(specularBase, max(r3w, 1)) * shadow * r1w * ksLightColor.rgb;

  return diffuseAmbient + specularPart;
}

float3 calculateMapsLighting(float3 toCamera, float3 normalW, 
    float3 txDiffuseValue, float3 txMapsValue, float shadow){

  #if NO_SUNSPEC_MAT
    return calculateMapsLighting_woSun(toCamera, normalW, txDiffuseValue, txMapsValue, shadow);
  #else
    if (sunSpecular > 0){
      return calculateMapsLighting_wSun(toCamera, normalW, txDiffuseValue, txMapsValue, shadow);
    } else {
      return calculateMapsLighting_woSun(toCamera, normalW, txDiffuseValue, txMapsValue, shadow);
    }
  #endif
}

float3 getReflection(float3 baseResultColor, float3 toCamera, float3 normalW, float3 txMapsValue, 
    float extraMultiplier, bool isMetallic, bool isCarPaint = false){
  float fresnelMaxLevelAdj = fresnelMaxLevel * extraMultiplier;
  float blurness = saturate(1 - txMapsValue.y * ksSpecularEXP / (isCarPaint ? 8 : 255)) * 6;
  float3 reflected = normalize(reflect(toCamera, normalW));

  float coefficient = saturate(1 - dot(normalW, -toCamera));
  float intensity = coefficient > 0 
    ? min(
      pow(
        coefficient, 
        isMetallic && !isCarPaint 
          ? max(fresnelEXP, 1) 
          : fresnelEXP) 
        + fresnelC * extraMultiplier, 
      fresnelMaxLevelAdj) 
    : 0;

  reflected.x *= -1;

  float3 color;
  if (blurness <= 3){
    color = txCube.SampleBias(samLinear, reflected, blurness).rgb;
  } else {      
    color = txCube.SampleLevel(samLinear, reflected, blurness).rgb;
  }

  if (isMetallic){    
    float3 r2 = color * txMapsValue.z - baseResultColor;
    return intensity * r2 + baseResultColor;
  } else {
    return color * intensity * txMapsValue.z + baseResultColor;
  }
}

float3 getReflection2(float3 baseResultColor, float3 toCamera, float3 normalW, float3 txMapsValue, 
    float extraMultiplier, bool isMetallic, float fresnelMaxLevelAdj, bool isCarPaint = false){
  float blurness = saturate(1 - txMapsValue.y * ksSpecularEXP / (isCarPaint ? 8 : 255)) * 6;
  float3 reflected = normalize(reflect(toCamera, normalW));

  float coefficient = saturate(1 - dot(normalW, -toCamera));
  float intensity = coefficient > 0 
    ? min(
      pow(
        coefficient, 
        isMetallic && !isCarPaint 
          ? max(fresnelEXP, 1) 
          : fresnelEXP) 
        + fresnelC * extraMultiplier, 
      fresnelMaxLevelAdj) 
    : 0;

  reflected.x *= -1;

  float3 color;
  if (blurness <= 3){
    color = txCube.SampleBias(samLinear, reflected, blurness).rgb;
  } else {      
    color = txCube.SampleLevel(samLinear, reflected, blurness).rgb;
  }

  if (isMetallic){    
    float3 r2 = color * txMapsValue.z - baseResultColor;
    return intensity * r2 + baseResultColor;
  } else {
    return color * intensity * txMapsValue.z + baseResultColor;
  }
}

#endif

float4 withFog(float3 baseResult, float fog, float alphaValue){
  return float4(lerp(baseResult.rgb, ksFogColor, saturate(fog)), alphaValue);
}

void withFog(out float4 output, float3 baseResult, float fog, float alphaValue){
  output.w = alphaValue;
  output.xyz = lerp(baseResult.rgb, ksFogColor, saturate(fog));
}