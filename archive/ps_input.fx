SamplerState samLinear : register(s0) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerState samPoint : register(s2) {
  Filter = POINT;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerComparisonState samShadow : register(s1) {
  Filter = COMPARISON_MIN_MAG_MIP_LINEAR;
  AddressU = BORDER;
  AddressV = BORDER;
  AddressW = BORDER;
  BorderColor = 1;
  ComparisonFunc = LESS;
};

SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txDiffuse : register(t0);
Texture2D txVariation : register(t1);
Texture2D txNormal : register(t1);
Texture2D txDirty : register(t2);
Texture2D txGlow : register(t2);
Texture2D txBlur : register(t3);
Texture2D txNormalBlur : register(t4);
Texture2D txMask : register(t1);
Texture2D txDetailR : register(t2);
Texture2D txDetailG : register(t3);
Texture2D txDetailB : register(t4);
Texture2D txDetailA : register(t5);
Texture2D txMaps : register(t2);
Texture2D txDetail : register(t3);
Texture2D txDetailNM : register(t21);
Texture2D txNormalDetail : register(t4);
Texture2D txDamage : register(t4);
Texture2D txDust : register(t5);
Texture2D<float> txShadow0 : register(t6);
Texture2D<float> txShadow1 : register(t7);
Texture2D<float> txShadow2 : register(t8);
TextureCube txCube : register(t10);
Texture2D txDamageMask : register(t21);

cbuffer cbPerObject : register(b1) {
  float4x4 ksWorld;                  // Offset:    0 Size:    64
}

cbuffer cbLighting : register(b2) {
  float4 ksLightDirection;           // Offset:    0 Size:    16
  float4 ksAmbientColor;             // Offset:   16 Size:    16
  float4 ksLightColor;               // Offset:   32 Size:    16
  float4 ksHorizonColor;             // Offset:   48 Size:    16 [unused]
  float4 ksZenithColor;              // Offset:   64 Size:    16 [unused]
  float ksExposure;                  // Offset:   80 Size:     4 [unused]
  float ksScreenWidth;               // Offset:   84 Size:     4 [unused]
  float ksScreenHeight;              // Offset:   88 Size:     4 [unused]
  float ksFogLinear;                 // Offset:   92 Size:     4 [unused]
  float ksFogBlend;                  // Offset:   96 Size:     4 [unused]
  float3 ksFogColor;                 // Offset:  100 Size:    12
  float ksCloudCover;                // Offset:  112 Size:     4 [unused]
  float ksCloudCutoff;               // Offset:  116 Size:     4 [unused]
  float ksCloudColor;                // Offset:  120 Size:     4 [unused]
  float ksCloudOffset;               // Offset:  124 Size:     4 [unused]
  float ksMinExposure;               // Offset:  128 Size:     4 [unused]
  float ksMaxExposure;               // Offset:  132 Size:     4 [unused]
  float ksDofFocus;                  // Offset:  136 Size:     4 [unused]
  float ksDofRange;                  // Offset:  140 Size:     4 [unused]
  float ksSaturation;                // Offset:  144 Size:     4 [unused]
  float ksGameTime;                  // Offset:  148 Size:     4 [unused]
  float2 boh44;                      // Offset:  152 Size:     8 [unused]
}

cbuffer cbMaterial : register(b4) {
  float ksAmbient;                   // Offset:    0 Size:     4
  float ksDiffuse;                   // Offset:    4 Size:     4
  float ksSpecular;                  // Offset:    8 Size:     4
  float ksSpecularEXP;               // Offset:   12 Size:     4
  float3 ksEmissive;                 // Offset:   16 Size:    12
  float ksAlphaRef;                  // Offset:   28 Size:     4 [unused]
}

#ifndef NO_SHADOWS
cbuffer cbShadowMaps : register(b3) {
  float4x4 ksShadowMatrix0;          // Offset:    0 Size:    64 [unused]
  float4x4 ksShadowMatrix1;          // Offset:   64 Size:    64 [unused]
  float4x4 ksShadowMatrix2;          // Offset:  128 Size:    64 [unused]
  float3 bias;                       // Offset:  192 Size:    12
  float textureSize;                 // Offset:  204 Size:     4
}
#endif

#ifdef INCLUDE_MULTILAYER_CB
cbuffer cbMultiLayer : register(b5) {
  float multR;                       // Offset:    0 Size:     4
  float multG;                       // Offset:    4 Size:     4
  float multB;                       // Offset:    8 Size:     4
  float2 multA;                      // Offset:   16 Size:     8
  float magicMult;                   // Offset:   24 Size:     4
  float2 boh;                        // Offset:   32 Size:     8 [unused]
}
#endif

#ifdef INCLUDE_MULTILAYER2_CB
cbuffer cbMultiLayer : register(b5) {
  float2 multR;                      // Offset:    0 Size:     8
  float2 multG;                      // Offset:    8 Size:     8
  float2 multB;                      // Offset:   16 Size:     8
  float2 multA;                      // Offset:   24 Size:     8
  float magicMult;                   // Offset:   32 Size:     4
}
#endif

#ifdef INCLUDE_MULTILAYER3_CB
cbuffer cbMultiLayer : register(b5) {
  float multR;                       // Offset:    0 Size:     4
  float multG;                       // Offset:    4 Size:     4
  float multB;                       // Offset:    8 Size:     4
  float2 multA;                      // Offset:   16 Size:     8
  float magicMult;                   // Offset:   24 Size:     4
  float fresnelC;                    // Offset:   28 Size:     4
  float fresnelEXP;                  // Offset:   32 Size:     4
  float fresnelMaxLevel;             // Offset:   36 Size:     4
  float tarmacSpecularMultiplier;    // Offset:   40 Size:     4
  float2 detailNMMult;               // Offset:   48 Size:     8
}
#endif

#ifdef INCLUDE_GRASS_CB
cbuffer GrassBuffer : register(b10) {  
  float2 scale;                      // Offset:    0 Size:     8
  float gain;                        // Offset:    8 Size:     4
  float boh;                         // Offset:   12 Size:     4 [unused]
}
#endif

#ifdef INCLUDE_ALPHA_CB
cbuffer cbAlphaLevel : register(b12) {  
  float alpha;                       // Offset:    0 Size:     4
}
#endif

#ifdef CARPAINT_NM_UVMULT
cbuffer cbTextureMult : register(b11) {  
  float diffuseMult;                 // Offset:    0 Size:     4
  float normalMult;                  // Offset:    4 Size:     4
  float2 bo;                         // Offset:    8 Size:     8 [unused]
}
#endif

#ifdef INCLUDE_TYRE_CB

cbuffer cbTyre : register(b10) {
  float blurLevel;                   // Offset:    0 Size:     4
  #ifdef USE_BRAKE_DISC_MODE
  float glowLevel;                  // Offset:    4 Size:     4
  #else
  float dirtyLevel;                  // Offset:    4 Size:     4
  #endif
  float fresnelC;                    // Offset:    8 Size:     4
  float fresnelEXP;                  // Offset:   12 Size:     4
  float isAdditive;                  // Offset:   16 Size:     4
  float fresnelMaxLevel;             // Offset:   20 Size:     4
}

static bool nmObjectSpace = false;

#elif !defined NO_CARPAINT
cbuffer cbCarPaint : register(b5) {
#ifdef CARPAINT_SIMPLE
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float isAdditive;                  // Offset:    8 Size:     4
  float fresnelMaxLevel;             // Offset:   12 Size:     4
#elif defined CARPAINT_NM
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float nmObjectSpace;               // Offset:    8 Size:     4
  float isAdditive;                  // Offset:   12 Size:     4
  float fresnelMaxLevel;             // Offset:   16 Size:     4
  float3 boh;                        // Offset:   20 Size:    12 [unused]
#elif defined CARPAINT_NM_UVMULT
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float boh;                         // Offset:   12 Size:     4 [unused]
#elif defined CARPAINT_SIMPLE_REFL
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float nmObjectSpace;               // Offset:   12 Size:     4
  float isAdditive;                  // Offset:   16 Size:     4 [unused]
  float useDetail;                   // Offset:   20 Size:     4
  float detailUVMultiplier;          // Offset:   24 Size:     4
  float shadowBiasMult;              // Offset:   28 Size:     4
#elif defined CARPAINT_NMDETAILS
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float isAdditive;                  // Offset:   12 Size:     4
  float useDetail;                   // Offset:   16 Size:     4
  float detailUVMultiplier;          // Offset:   20 Size:     4
  float shadowBiasMult;              // Offset:   24 Size:     4
  float detailNormalBlend;           // Offset:   28 Size:     4
#else
  float fresnelC;                    // Offset:    0 Size:     4
  float fresnelEXP;                  // Offset:    4 Size:     4
  float fresnelMaxLevel;             // Offset:    8 Size:     4
  float nmObjectSpace;               // Offset:   12 Size:     4
  float isAdditive;                  // Offset:   16 Size:     4
  float useDetail;                   // Offset:   20 Size:     4
  float detailUVMultiplier;          // Offset:   24 Size:     4
  float shadowBiasMult;              // Offset:   28 Size:     4
  float4 damageZones;                // Offset:   32 Size:    16 [unused]
  float dirt;                        // Offset:   48 Size:     4 [unused]
  float sunSpecular;                 // Offset:   52 Size:     4
  float sunSpecularEXP;              // Offset:   56 Size:     4
#endif
}
#endif

struct PS_IN_GL {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD;
  float3 TangentH : TANGENT;
};

struct PS_IN_PerPixel {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  float Fog : TEXCOORD6;
};

struct PS_IN_Multilayer {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  float3 MultilayerThing : TEXCOORD6;
  float Fog : TEXCOORD7;
};

struct PS_IN_MultilayerFresnel {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  float3 MultilayerThing : TEXCOORD6;
  float Fog : TEXCOORD7;
  float3 MultilayerThing1 : TEXCOORD8;
  float3 MultilayerThing2 : TEXCOORD9;
};

struct PS_IN_SkidMark {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  centroid float SkidMarkThing : TEXCOORD6;
  float Fog : TEXCOORD7;
};

struct PS_IN_Tree {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float Fog : TEXCOORD6;
};

struct PS_IN_Grass {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  float Fog : TEXCOORD6;
  float2 GrassThing : TEXCOORD7;
};

struct PS_IN_AtNs {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float Fog : TEXCOORD6;
};

struct PS_IN_Nm {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  float3 TangentW : TEXCOORD6;
  float3 BitangentW : TEXCOORD7;
  float Fog : TEXCOORD8;
};

struct PS_IN_Uv2 {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  float3 TangentW : TEXCOORD6;
  float3 BitangentW : TEXCOORD7;
  float2 Tex2 : TEXCOORD8;
  float Fog : TEXCOORD9;
};