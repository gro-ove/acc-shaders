#define FilterSize_ 3

static const float W[3][3] =
{
    { 0.5,1.0,0.5, },
    { 1.0,1.0,1.0, },
    { 0.5,1.0,0.5, }
};

// from https://github.com/TheRealMJP/Shadows/blob/master/Shadows/Mesh.hlsl
float cascadeShadowStep(float deltaZ, float4 uv, uint cascadeIdx){
  float result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  const int FS_2 = FilterSize_ / 2;

  float2 tc = uv.xy;

  float shadowMapSize = 1.0f / textureSize.x;
  float shadowMapSizeInv = textureSize.x;

  float4 s = 0.0f;
  float2 stc = (shadowMapSize * tc.xy) + float2(0.5f, 0.5f);
  float2 tcs = floor(stc);
  float2 fc;
  int row;
  int col;
  float w = 0.0f;
  float4 v1[FS_2 + 1];
  float2 v0[FS_2 + 1];

  fc.xy = stc - tcs;
  tc.xy = tcs * shadowMapSizeInv;

  for(row = 0; row < FilterSize_; ++row)
    for(col = 0; col < FilterSize_; ++col)
      w += W[row][col];

  // -- loop over the rows
  [unroll]
  for(row = -FS_2; row <= FS_2; row += 2)
  {
    [unroll]
    for(col = -FS_2; col <= FS_2; col += 2)
    {
      float value = W[row + FS_2][col + FS_2];

      if(col > -FS_2)
        value += W[row + FS_2][col + FS_2 - 1];

      if(col < FS_2)
        value += W[row + FS_2][col + FS_2 + 1];

      if(row > -FS_2) {
        value += W[row + FS_2 - 1][col + FS_2];

        if(col < FS_2)
          value += W[row + FS_2 - 1][col + FS_2 + 1];

        if(col > -FS_2)
          value += W[row + FS_2 - 1][col + FS_2 - 1];
      }

      if(value != 0.0f)
      {
        float sampleDepth = comparisonValue;

        // #if UsePlaneDepthBias_
        //   // Compute offset and apply planar depth bias
        //   float2 offset = float2(col, row) * texelSize;
        //   sampleDepth += dot(offset, receiverPlaneDepthBias);
        // #endif

        v1[(col + FS_2) / 2] = txShadow.GatherCmp(samShadow, float3(tc.xy, cascadeIdx),
                               sampleDepth, int2(col, row));
      }
      else
        v1[(col + FS_2) / 2] = 0.0f;

      if(col == -FS_2)
      {
        s.x += (1.0f - fc.y) * (v1[0].w * (W[row + FS_2][col + FS_2]
                   - W[row + FS_2][col + FS_2] * fc.x)
                   + v1[0].z * (fc.x * (W[row + FS_2][col + FS_2]
                   - W[row + FS_2][col + FS_2 + 1.0f])
                   + W[row + FS_2][col + FS_2 + 1]));
        s.y += fc.y * (v1[0].x * (W[row + FS_2][col + FS_2]
                   - W[row + FS_2][col + FS_2] * fc.x)
                   + v1[0].y * (fc.x * (W[row + FS_2][col + FS_2]
                   - W[row + FS_2][col + FS_2 + 1])
                   +  W[row + FS_2][col + FS_2 + 1]));
        if(row > -FS_2)
        {
          s.z += (1.0f - fc.y) * (v0[0].x * (W[row + FS_2 - 1][col + FS_2]
                       - W[row + FS_2 - 1][col + FS_2] * fc.x)
                       + v0[0].y * (fc.x * (W[row + FS_2 - 1][col + FS_2]
                       - W[row + FS_2 - 1][col + FS_2 + 1])
                       + W[row + FS_2 - 1][col + FS_2 + 1]));
          s.w += fc.y * (v1[0].w * (W[row + FS_2 - 1][col + FS_2]
                    - W[row + FS_2 - 1][col + FS_2] * fc.x)
                    + v1[0].z * (fc.x * (W[row + FS_2 - 1][col + FS_2]
                    - W[row + FS_2 - 1][col + FS_2 + 1])
                    + W[row + FS_2 - 1][col + FS_2 + 1]));
        }
      }
      else if(col == FS_2)
      {
        s.x += (1 - fc.y) * (v1[FS_2].w * (fc.x * (W[row + FS_2][col + FS_2 - 1]
                   - W[row + FS_2][col + FS_2]) + W[row + FS_2][col + FS_2])
                   + v1[FS_2].z * fc.x * W[row + FS_2][col + FS_2]);
        s.y += fc.y * (v1[FS_2].x * (fc.x * (W[row + FS_2][col + FS_2 - 1]
                   - W[row + FS_2][col + FS_2] ) + W[row + FS_2][col + FS_2])
                   + v1[FS_2].y * fc.x * W[row + FS_2][col + FS_2]);
        if(row > -FS_2) {
          s.z += (1 - fc.y) * (v0[FS_2].x * (fc.x * (W[row + FS_2 - 1][col + FS_2 - 1]
                    - W[row + FS_2 - 1][col + FS_2])
                    + W[row + FS_2 - 1][col + FS_2])
                    + v0[FS_2].y * fc.x * W[row + FS_2 - 1][col + FS_2]);
          s.w += fc.y * (v1[FS_2].w * (fc.x * (W[row + FS_2 - 1][col + FS_2 - 1]
                    - W[row + FS_2 - 1][col + FS_2])
                    + W[row + FS_2 - 1][col + FS_2])
                    + v1[FS_2].z * fc.x * W[row + FS_2 - 1][col + FS_2]);
        }
      }
      else
      {
        s.x += (1 - fc.y) * (v1[(col + FS_2) / 2].w * (fc.x * (W[row + FS_2][col + FS_2 - 1]
                  - W[row + FS_2][col + FS_2 + 0] ) + W[row + FS_2][col + FS_2 + 0])
                  + v1[(col + FS_2) / 2].z * (fc.x * (W[row + FS_2][col + FS_2 - 0]
                  - W[row + FS_2][col + FS_2 + 1]) + W[row + FS_2][col + FS_2 + 1]));
        s.y += fc.y * (v1[(col + FS_2) / 2].x * (fc.x * (W[row + FS_2][col + FS_2-1]
                  - W[row + FS_2][col + FS_2 + 0]) + W[row + FS_2][col + FS_2 + 0])
                  + v1[(col + FS_2) / 2].y * (fc.x * (W[row + FS_2][col + FS_2 - 0]
                  - W[row + FS_2][col + FS_2 + 1]) + W[row + FS_2][col + FS_2 + 1]));
        if(row > -FS_2) {
          s.z += (1 - fc.y) * (v0[(col + FS_2) / 2].x * (fc.x * (W[row + FS_2 - 1][col + FS_2 - 1]
                      - W[row + FS_2 - 1][col + FS_2 + 0]) + W[row + FS_2 - 1][col + FS_2 + 0])
                      + v0[(col + FS_2) / 2].y * (fc.x * (W[row + FS_2 - 1][col + FS_2 - 0]
                      - W[row + FS_2 - 1][col + FS_2 + 1]) + W[row + FS_2 - 1][col + FS_2 + 1]));
          s.w += fc.y * (v1[(col + FS_2) / 2].w * (fc.x * (W[row + FS_2 - 1][col + FS_2 - 1]
                      - W[row + FS_2 - 1][col + FS_2 + 0]) + W[row + FS_2 - 1][col + FS_2 + 0])
                      + v1[(col + FS_2) / 2].z * (fc.x * (W[row + FS_2 - 1][col + FS_2 - 0]
                      - W[row + FS_2 - 1][col + FS_2 + 1]) + W[row + FS_2 - 1][col + FS_2 + 1]));
        }
      }

      if(row != FS_2)
        v0[(col + FS_2) / 2] = v1[(col + FS_2) / 2].xy;
    }
  }

  return dot(s, 1.0f) / w;
}

bool checkCascade(float4 uv){
  return -1 < uv.x && uv.x < 1 && -1 < uv.y && uv.y < 1;
}

/*float4 bilinearFactors( float2 _vCoord ) {
  float4 vFrac;
  vFrac.xy = frac(_vCoord);
  vFrac.zw = 1-vFrac.xy;
  return vFrac.zxzx*vFrac.wwyy;
}

float cascadeShadowStep_samplecmp(float deltaZ, float4 uv, Texture2D<float> tx){
  float result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);
  // return tx.SampleCmpLevelZero(samShadow, uv.xy, comparisonValue);

  [unroll]
  for (int x = -1; x <= 1; x += 2){
    [unroll]
    for (int y = -1; y <= 1; y += 2){
      result += tx.SampleCmpLevelZero( samShadow, uv.xy, comparisonValue, int2(x, y) );
    }
  }

  return result * 0.25;
}

float cascadeShadowStep_sharper(float deltaZ, float4 uv, Texture2D<float> tx){
  float4 result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  [unroll]
  for (int x = -1; x <= 0; x += 1){
    [unroll]
    for (int y = -1; y <= 0; y += 1){
      result += tx.GatherCmp( samShadow, uv.xy, comparisonValue, int2(x, y) );
    }
  }

  float4 interpolation = bilinearFactors(uv.xy / textureSize.x - 0.5);
  return dot(result.wzxy, interpolation / 4);
}

float cascadeShadowStep_single(float deltaZ, float4 uv, Texture2D<float> tx){
  float4 result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);
  result += tx.GatherCmp( samShadow, uv.xy, comparisonValue );
  float4 interpolation = bilinearFactors(uv.xy / textureSize.x - 0.5);
  return dot(result.wzxy, interpolation);
}

float cascadeShadowStep_orig(float deltaZ, float4 uv, Texture2D<float> tx){
  float result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  float2 rotate_at = float2(frac(10000 * uv.x), frac(11000 * uv.y));

  [unroll]
  for (int x = -1; x <= 0; x++){
    [unroll]
    for (int y = -1; y <= 0; y++){
      result += tx.SampleCmpLevelZero(samShadow, uv.xy + textureSize * reflect(float2(x, y), rotate_at), comparisonValue);
    }
  }

  return result;
}*/

float getShadow(float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2){
  #ifdef USE_SHADOW_BIAS_MULT
    float biasMultiplier = shadowBiasMult + 1;
    float deltaZBase = max(1 - abs(dot(normalW, ksLightDirection.xyz)), 0.1) * biasMultiplier;
  #elif defined GETSHADOW_BIAS_MULTIPLIER
    float deltaZBase = max(1 - abs(dot(normalW, ksLightDirection.xyz)), 0.1) * GETSHADOW_BIAS_MULTIPLIER;
  #else
    float deltaZBase = max(1 - abs(dot(normalW, ksLightDirection.xyz)), 0.1);
  #endif

  float4 tex;
  uint cascadeIdx;
  float deltaMult;

  if (checkCascade(shadowTex0)){
    tex = shadowTex0;
    cascadeIdx = 0;
    deltaMult = bias.x;
  } else if (checkCascade(shadowTex1)){
    tex = shadowTex1;
    cascadeIdx = 1;
    deltaMult = bias.y;
  } else if (checkCascade(shadowTex2)){
    tex = shadowTex2;
    cascadeIdx = 2;
    deltaMult = bias.z;
  } else {
    return 1;
  }

  /*if (checkCascade(shadowTex2)){
    return min(cascadeShadowStep(deltaZBase * bias.z, shadowTex2, 2) 
        + max(-(shadowTex2.y + 0.5), 0) * 2, 1);
  }*/

  return cascadeShadowStep(deltaZBase * deltaMult, tex, cascadeIdx);
}