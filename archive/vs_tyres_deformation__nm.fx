#include "ext_data_tyres.fx"

#define M_PI 3.141592653589793

float3 calculateNewPosition(float3 inputPosition){
  float3 posD = inputPosition - contactPos;
  float squashedBase = 1 - dot(posD, contactNormal) * profileSizeInv;
  float squashed = saturate(squashedBase);

  // if (squashed > 0.0) posW += contactNormal;

  // float squashedUp = 1 - sqrt(1 - squashed * squashed);
  float squashedUp = squashed * squashed;
  float squashedHalf = sin(squashed * M_PI);

  float squashedSkew = (squashedBase + 1) * 0.5;
  squashedSkew = saturate(squashedSkew);
  squashedSkew = pow(squashedSkew, 1.5);

  float3 posE = inputPosition - (wheelPos + ksCameraPosition.xyz);
  float lenE = length(posE);
  squashedSkew *= saturate((lenE - rimRadius) * profileSizeInv * 2.0);

  // squashed *= squashed;
  float squashedFlex = squashed * squashed * squashed;
  float sideDot = dot(posD, dirSide) * tyreWidthInv;
  float3 dirRoadForward = -normalize(cross(contactNormal, dirSide));

  inputPosition.xyz += contactNormal * squashedUp * tyreSquashDistance;
  inputPosition.xyz += dirSide * (sideDot * squashedHalf * tyreSquashDistance + squashedSkew * tyreSkewX);
  inputPosition.xyz += dirRoadForward * squashedSkew * tyreSkewY;
  return inputPosition;
}

void squashTyre(inout float3 posW, inout float3 normalW){
  float3 dirRoadForward = -normalize(cross(contactNormal, dirSide));
  float f = calculateNewPosition(posW + dirRoadForward * 0.001);
  float u = calculateNewPosition(posW + contactNormal * 0.001);
  posW = calculateNewPosition(posW);
  
  posW = calculateNewPosition(posW);
  posW.xyz += dirSide * (sideDot * squashedHalf * tyreSquashDistance + squashedSkew * tyreSkewX);
  posW.xyz += dirRoadForward * squashedSkew * tyreSkewY;

  // float3 normal = dirSide + float3() cos();
  float deriv = cos(squashed * M_PI);
  // float3 n = float3(1 - );
  // float3 normalLocal = -cross(dirSide * deriv - contactNormal * squashedHalf, dirRoadForward);
  // float3 normalLocal = float3(0, deriv, squashedHalf);
  float3 normalLocal = float3(0, squashedHalf, deriv);

  float3 T = cross(sideDot > 0 ? dirRoadForward : -dirRoadForward, normalW);
  float3 B = cross(T, normalW);
  float3x3 m = float3x3(B, normalW, T);
  float3 normalDmg = normalize(mul(transpose(m), normalLocal));
  normalW = lerp(normalW, normalDmg, saturate((abs(sideDot) - 0.8) * 5.0));

  // posW.xyz += normalW * 0.01;
}

#define ADJUST_WORLD_POS(x, y) squashTyre(x.xyz, y.xyz);
