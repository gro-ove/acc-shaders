gary.cpp:

  struct cbuffer_extflags_data
  {
    float3 flagFrom;
    float windSpeed;

    float3 flagTo;
    float windDirection;
  };

  static int update_flags_next = -1;
  static bool init = false;
  static cbuffer_perobject<cbuffer_extflags_data> tyres_cbuffer_(6);

  void gary::on_draw(hook_events::draw_args& args) const
  {
    if (update_flags_next != -1)
    {
      if (!init)
      {
        tyres_cbuffer_.initialize(ac_hooks::get_device());
        init = true;
      }

      auto data = tyres_cbuffer_.get_data();
      auto& params = flag_widths_[update_flags_next];
      data->windSpeed = settings_.dev_wind_speed;
      data->windDirection = settings_.dev_wind_direction;
      data->flagFrom = params.from;
      data->flagTo = params.to;
      LOG(INFO) << "Flag drawing: " << settings_.dev_wind_speed;
      tyres_cbuffer_.set_dirty();
      tyres_cbuffer_.set(ac_hooks::get_immediate_context());
      update_flags_next = -1;
    }
  }

  void gary::on_flags(hook_events::flags_args& args)
  {
    update_flags_next = (int)args.flag_id;
  }

  void gary::on_kn5_load_finished(hook_events::kn5_load_finished_args& args)
  {
    if (ext_track_settings_ != nullptr && ext_track_settings_->track.isnull())
    {
      std::vector<acptr_mesh> flags;
      args.root.find_by_shader(L"ksFlags", flags);
      for (auto flag : flags)
      {
        flag.set_mesh_type(acptr_mesh::type_flag, (uint)flag_widths_.size());

        auto params = flag_params();
        auto flag_start_value = -1.0f;
        auto flag_end_value = -1.0f;

        for (auto& v : flag.vertices())
        {
          if (flag_start_value == -1.0f || flag_start_value > v.tex_coord.x)
          {
            params.from = v.pos;
            flag_start_value = v.tex_coord.x;
          }

          if (flag_end_value == -1.0f || flag_end_value < v.tex_coord.x)
          {
            params.to = v.pos;
            flag_end_value = v.tex_coord.x;
          }
        }

        LOG(INFO) << "Flag: " << flag.name();
        flag_widths_.push_back(params);
      }
    }
  }

gary.h:

  hooks_->events()->flags.subscribe(std::bind(&gary::on_flags, this, _1));
  void on_flags(hook_events::flags_args& args);

  struct flag_params
  {
    float3 from;
    float3 to;
  };
  std::vector<flag_params> flag_widths_;

events:

  DEFEVENT(flags, { uint flag_id; })

CameraMeshFilter.isVisible hook:

  else if (mesh_type == acptr_mesh::type_flag)
  {
    _flag_being_drawn = mesh_type_id;
    events->flags.raise(hook_events::flags_args{_flag_being_drawn});
  }