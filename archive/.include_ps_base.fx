// Params

// Slightly different from the one used by Kunos’ algorithm, saves 18 instructions
// #define OPTIMIZED_SHADOWS

// Small changes here and there to improve shaders and resolve some warnings
// #define MICROOPTIMIZATIONS

// Inputs

SamplerState samLinear : register(s0) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerComparisonState samShadow : register(s1) {
  Filter = COMPARISON_MIN_MAG_MIP_LINEAR;
  AddressU = BORDER;
  AddressV = BORDER;
  AddressW = BORDER;
  BorderColor = 1;
  ComparisonFunc = LESS;
};

SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txDiffuse : register(t0);
Texture2D txNormal : register(t1);
Texture2D txMaps : register(t2);
Texture2D txDetail : register(t3);
Texture2D txNormalDetail : register(t4);
Texture2D txDamage : register(t4);
Texture2D txDust : register(t5);
Texture2D<float> txShadow0 : register(t6);
Texture2D<float> txShadow1 : register(t7);
Texture2D<float> txShadow2 : register(t8);
TextureCube txCube : register(t10);
Texture2D txDamageMask : register(t21);

cbuffer cbPerObject : register(b1) {
  float4x4 ksWorld;                  // Offset:    0 Size:    64
}

cbuffer cbLighting : register(b2) {
  float4 ksLightDirection;           // Offset:    0 Size:    16
  float4 ksAmbientColor;             // Offset:   16 Size:    16
  float4 ksLightColor;               // Offset:   32 Size:    16
  float4 ksHorizonColor;             // Offset:   48 Size:    16 [unused]
  float4 ksZenithColor;              // Offset:   64 Size:    16 [unused]
  float ksExposure;                  // Offset:   80 Size:     4 [unused]
  float ksScreenWidth;               // Offset:   84 Size:     4 [unused]
  float ksScreenHeight;              // Offset:   88 Size:     4 [unused]
  float ksFogLinear;                 // Offset:   92 Size:     4 [unused]
  float ksFogBlend;                  // Offset:   96 Size:     4 [unused]
  float3 ksFogColor;                 // Offset:  100 Size:    12
  float ksCloudCover;                // Offset:  112 Size:     4 [unused]
  float ksCloudCutoff;               // Offset:  116 Size:     4 [unused]
  float ksCloudColor;                // Offset:  120 Size:     4 [unused]
  float ksCloudOffset;               // Offset:  124 Size:     4 [unused]
  float ksMinExposure;               // Offset:  128 Size:     4 [unused]
  float ksMaxExposure;               // Offset:  132 Size:     4 [unused]
  float ksDofFocus;                  // Offset:  136 Size:     4 [unused]
  float ksDofRange;                  // Offset:  140 Size:     4 [unused]
  float ksSaturation;                // Offset:  144 Size:     4 [unused]
  float ksGameTime;                  // Offset:  148 Size:     4 [unused]
  float2 boh44;                      // Offset:  152 Size:     8 [unused]
}

cbuffer cbMaterial : register(b4) {
  float ksAmbient;                   // Offset:    0 Size:     4
  float ksDiffuse;                   // Offset:    4 Size:     4
  float ksSpecular;                  // Offset:    8 Size:     4
  float ksSpecularEXP;               // Offset:   12 Size:     4
  float3 ksEmissive;                 // Offset:   16 Size:    12
  float ksAlphaRef;                  // Offset:   28 Size:     4 [unused]
}

cbuffer cbShadowMaps : register(b3) {
  float4x4 ksShadowMatrix0;          // Offset:    0 Size:    64 [unused]
  float4x4 ksShadowMatrix1;          // Offset:   64 Size:    64 [unused]
  float4x4 ksShadowMatrix2;          // Offset:  128 Size:    64 [unused]
  float3 bias;                       // Offset:  192 Size:    12
  float textureSize;                 // Offset:  204 Size:     4
}

struct PS_IN_PerPixel {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  float4 ShadowTex0 : TEXCOORD3;
  float4 ShadowTex1 : TEXCOORD4;
  float4 ShadowTex2 : TEXCOORD5;
  float Fog : TEXCOORD6;
};

// Shadows

bool checkCascade(float4 uv){
  return -1 < uv.x && uv.x < 1 && -1 < uv.y && uv.y < 1;
}

float cascadeShadowStep(float deltaZ, float4 uv, Texture2D<float> tx){
  float result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  [unroll]
  for (int x = -1; x <= 1; x++){
    [unroll]
    for (int y = -1; y <= 1; y++){
      #ifdef OPTIMIZED_SHADOWS
      result += tx.SampleCmpLevelZero(samShadow, uv.xy + textureSize * float2(x, y), comparisonValue);
      #else
      result += tx.SampleCmpLevelZero(samShadow, uv.xy + float2(y * textureSize, x * textureSize), comparisonValue);
      #endif
    }
  }

  return result;
}

float getShadow(float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2){
  #ifdef USE_SHADOW_BIAS_MULT
    float biasMultiplier = shadowBiasMult + 1;
    float deltaZBase = max(1 - abs(dot(normalW, ksLightDirection.xyz)), 0.1) * biasMultiplier;
  #else
    float deltaZBase = max(1 - abs(dot(normalW, ksLightDirection.xyz)), 0.1);
  #endif

  if (checkCascade(shadowTex0)){
    return cascadeShadowStep(deltaZBase * bias.x, shadowTex0, txShadow0) / 9;
  }

  if (checkCascade(shadowTex1)){
    return cascadeShadowStep(deltaZBase * bias.y, shadowTex1, txShadow1) / 9;
  }

  if (checkCascade(shadowTex2)){
    return min(cascadeShadowStep(deltaZBase * bias.z, shadowTex2, txShadow2) / 9 
        + max(-(shadowTex2.y + 0.5), 0) * 2, 1);
  }

  return 1;
}

float3 calculateAmbient(float3 normalW){
  #ifdef MICROOPTIMIZATIONS
    float ambientMultiplier = saturate(normalW.y * 0.25 + 0.75);
  #else
    float upMultiplier = normalW.y * 0.5 + 0.5;
    float ambientMultiplier = saturate(upMultiplier * 0.5 + 0.5);
  #endif
  return ksAmbientColor.rgb * ksAmbient * ambientMultiplier + ksEmissive;
}